package manager

import (
	"crypto/md5"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/topfreegames/pitaya"
	"testing"
)

func testGMAccountManagerCommonInit(t *testing.T) (*model.PlayerObject) {
	base.BaseInit()
	pitaya.RegisterTemplate(template.GMAccountKey)
	pitaya.LoadAllTemplateWithPath("../template/txt")

	dao.NewPlayerDataDao().Init()
	dao.NewAccountDataDao().Init()
	dao.NewGuidDataDao().Init()
	dao.NewGuidGenertorDao().Init()

	NewGMAccountManager().Init()
	NewGMAccountManager().Init()

	v,err := dao.PlayerDataDao.Get(&pojo.PlayerData{Guid: 281194601245573138})
	assert.Nil(t, err)
	assert.NotNil(t, v)
	playerData,ok := v.(*pojo.PlayerData)
	assert.True(t, ok)
	return model.NewPlayerObject(playerData)
}

func TestGMAccountManager_Init(t *testing.T) {
	testGMAccountManagerCommonInit(t)
	assert.EqualValues(t, 4, len(GMAccountManager.gMAccountInfoMap))
}

func TestGMAccountManager_IsGMAccount(t *testing.T) {
	testGMAccountManagerCommonInit(t)
	assert.True(t, GMAccountManager.IsGMAccount("nsh001"))
}

func TestGMAccountManager_GetGMAccountBase(t *testing.T) {
	testGMAccountManagerCommonInit(t)
	v := GMAccountManager.GetGMAccountBase("nsh001")
	assert.NotNil(t, v)
	assert.EqualValues(t, "nsh001",v.AccountName)
}

func TestGMAccountManager_CheckInsertGMAccount(t *testing.T) {
	testGMAccountManagerCommonInit(t)
	err := GMAccountManager.CheckInsertGMAccount("nsh001","1","10000")
	assert.Nil(t, err)
	data,err1 := PlayerManager.GetAccountDataByUsernameAndChannelIdAndAppId("nsh001","1","10000")
	assert.Nil(t, err1)
	assert.EqualValues(t, "nsh001",data.Username)
	assert.EqualValues(t, "1",data.ChannelId)
	assert.EqualValues(t, "10000",data.AppId)
	assert.EqualValues(t, fmt.Sprintf("%x",md5.Sum([]byte(GMAccountManager.GetGMAccountBase("nsh001").Password))),data.Password)
}
