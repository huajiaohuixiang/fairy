package manager

import (
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/dao"
	"testing"
)

func testGangServerManagerCommonInit()  {
	base.BaseInit()
	dao.NewGangDataDao().Init()
	dao.NewGangMemberDataDao().Init()
	NewGangServerManager().Init()
}

func TestGangServerManager_Init(t *testing.T) {
	testGangServerManagerCommonInit()
	assert.NotEmpty(t, GangServerManager.allGangs)
}
