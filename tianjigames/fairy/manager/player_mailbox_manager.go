package manager

import (
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/pojo"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	PlayerMailBoxManager *playerMailBoxManager
	playerMailBoxOnce sync.Once
)

type playerMailBoxManager struct {
	BaseManager
}

func NewPlayerMailBoxManager() *playerMailBoxManager {
	playerMailBoxOnce.Do(func() {
		PlayerMailBoxManager = &playerMailBoxManager{}
	})

	return PlayerMailBoxManager
}

/**
根据guid获取邮箱
 */
func (p *playerMailBoxManager) GetPlayerMailBox(guid int64) (*pojo.PlayerMailBox,error) {
	r,err := dao.PlayerMailBoxDao.GetPlayerMailBoxByPlayerGuid(guid)
	if err != nil {
		logger.Log.Errorf("GetPlayerMailBox failed,err:%s",err.Error())
		return nil,err
	}

	return r,nil
}
