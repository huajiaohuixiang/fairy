package manager

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/template"
	"github.com/topfreegames/pitaya"
	"strconv"
	"sync"
)

var (
	FashionManger *fashionManager
	fashionManagerOnce sync.Once
)

type fashionManager struct {
	BaseManager
}

/**
时装管理器
 */
func NewFashionManager() *fashionManager {
	fashionManagerOnce.Do(func() {
		FashionManger = &fashionManager{

		}
	})
	return FashionManger
}

/**
计算时装加成的属性值
 */
func (p *fashionManager) CalFashionAttr(player *model.PlayerObject,attrId *constants.AttrFlag) int {
	if player == nil || attrId == nil {
		return 0
	}

	nRet := 0
	listData := player.MFashionListData.MFashionList
	for i := 0;i< len(listData);i++ {
		fashionId := listData[i].Id
		v := pitaya.GetTemplateById(template.FashionKey,strconv.FormatInt(int64(fashionId),10))
		if v == nil {
			continue
		}

		nRet += p.CalFashionAttrValue(player,v.(*template.Fashion),attrId)
	}
	return nRet
}

/**
计算时装加成的基本属性值
 */
func (p *fashionManager) CalFashionAttrValue(player *model.PlayerObject,fashion *template.Fashion,attrId *constants.AttrFlag) int {
	if player == nil || fashion == nil || attrId == nil {
		return 0
	}

	nRet := 0
	nBaseValue := player.GetBaseAttr(attrId)
	myCareer := player.GetCareerId()
	var attr1 []int
	var attr2 []int
	switch myCareer {
	case 1:
		attr1 = fashion.JiankeAttr1
		attr2 = fashion.JiankeAttr2
	case 2:
		attr1 = fashion.GongshouAttr1
		attr2 = fashion.GongshouAttr2
	case 3:
		attr1 = fashion.QiangkeAttr1
		attr2 = fashion.QiangkeAttr2
	case 4:
		attr1 = fashion.FashiAttr1
		attr2 = fashion.FashiAttr2
	}

	if attr1 != nil && len(attr1) >= constants.FashionIdxAttrLen {
		if attr1[0] == int(*attrId) {
			nRet += LoadPlayerInfoManager.GetAttrResult(model.GetRefixTypeByValue(attr1[1]),attr1[2],nBaseValue)
		}else {
			nRet += LoadPlayerInfoManager.CheckSpecialAttrByFlag(attrId,constants.GetAttrFlagByValue(attr1[0]),
				model.GetRefixTypeByValue(attr1[1]),attr1[2],nBaseValue)
		}
	}
	if attr2 != nil && len(attr2) >= constants.FashionIdxAttrLen {
		if attr2[0] == int(*attrId) {
			nRet += LoadPlayerInfoManager.GetAttrResult(model.GetRefixTypeByValue(attr2[1]),attr2[2],nBaseValue)
		}else {
			nRet += LoadPlayerInfoManager.CheckSpecialAttrByFlag(attrId,constants.GetAttrFlagByValue(attr2[0]),
				model.GetRefixTypeByValue(attr2[1]),attr2[2],nBaseValue)
		}
	}
	return nRet
}


