package manager

import (
	"github.com/tianjigames/fairy/dao"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	GuidGeneratorManager *guidGeneratorManager
	guidGeneratorOnce sync.Once
)


type guidGeneratorManager struct {
	BaseManager
}

func NewGuiGeneratorManager() *guidGeneratorManager {
	guidGeneratorOnce.Do(func() {
		GuidGeneratorManager = &guidGeneratorManager{}
	})
	return GuidGeneratorManager
}

func (p *guidGeneratorManager) GenGuid(typ int) (int64,error)   {
	rs,err := dao.GuidGeneratorDao.GenGuid(typ)
	if err != nil {
		logger.Log.Errorf("[GuidGeneratorManager] GenGuid failed,error:%s",err.Error())
		return 0,err
	}

	return rs,nil
}






