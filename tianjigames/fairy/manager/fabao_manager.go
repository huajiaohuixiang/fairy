package manager

import (
	"fmt"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/tianjigames/fairy/template"
	"github.com/tianjigames/fairy/util"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/logger"
	"strconv"
	"sync"
)

var (
	FabaoManager     *fabaoManager
	faBaoManagerOnce sync.Once
)

type fabaoManager struct {
	BaseManager
}

func NewFabaoManager() *fabaoManager {
	fashionManagerOnce.Do(func() {
		FabaoManager = &fabaoManager{}
	})
	return FabaoManager
}

/**
初始化玩家法宝
 */
func (p *fabaoManager) InitPlayerFabao(player *model.PlayerObject) (bool,error) {
	v,err := dao.FabaoDataDao.Get(&pojo.FabaoData{PlayerGuid: player.GetGuid()})
	if err != nil {
		logger.Log.Errorf("[FabaoManager] InitPlayerFabao failed,err:%s",err.Error())
		return false,err
	}

	if v == nil {
		return true,nil
	}

	fabao := v.(*pojo.FabaoData)

	v1 := pitaya.GetTemplateById(template.FabaoBaseKey,fmt.Sprintf("%d",fabao.TempateId))
	if v1 == nil {
		return false,nil
	}
	cfg := v1.(*template.FabaoBase)

	newFabao := model.NewFabao()
	newFabao.MDBData = fabao
	newFabao.UnserializeFabaoSkillData()
	newFabao.MFabaoBase = cfg

	//todo:检查事件

	player.MFabao = newFabao
	return true,nil
}

/**
激活玩家法宝
 */
func (p *fabaoManager) ActiveFabaoByGM(player *model.PlayerObject) error {
	if player == nil || player.MFabao == nil {
		return nil
	}

	v := pitaya.GetTemplateById(template.FabaoBaseKey,constants.FaBaoUnlockId)
	if v == nil {
		return nil
	}

	idInfo := v.(*template.Constant)
	idList := idInfo.Constant7
	if len(idList) <= 0 {
		return nil
	}

	v = pitaya.GetTemplateById(template.FabaoBaseKey,strconv.FormatInt(int64(idList[0]),10))
	if v == nil {
		return nil
	}

	faBaoCfg := v.(*template.FabaoBase)
	_,err := p.ActivePlayerFabao(player,idList[0],faBaoCfg)
	if err != nil {
		return err
	}

	fabaoMsg := &protos2.GCActiveFabaoRet{
		FabaoInfo: player.GetFabaoInfoMsg(),
	}

	err = PlayerManager.SendPacket(player,&model.MsgData{
		Route: constants.GCActiveFabaoRetRoute,
		Msg: fabaoMsg,
	})

	if err != nil {
		logger.Log.Errorf("[FabaoManager] ActiveFabaoByGM failed,error:%s",err.Error())
		return err
	}

	return nil
}

/**
激活玩家法宝
*/
func (p *fabaoManager) ActivePlayerFabao(player *model.PlayerObject,nId int,faBaoCfg *template.FabaoBase) (bool,error) {
	if player == nil || player.MFabao != nil || faBaoCfg == nil {
		return false,nil
	}
	data := model.NewFabao()
	data.MIsFirstCreate = true
	data.MFabaoBase = faBaoCfg

	err := p.UpdatePlayerFabaoDBData(player,data,util.Now(),true)
	if err != nil {
		logger.Log.Errorf("[FabaoManager] ActivePlayerFabao failed,error:%s",err.Error())
		return false,nil
	}

	data.SetId(nId)
	data.SetShowId(nId)
	data.InitSkill()
	player.MFabao = data
	player.PlayerData.FabaoId = nId
	player.MFabao.MarkAllAttr(player)

	//todo：更新玩家排行榜数据

	return true,nil
}

/**
更新玩家法宝数据库数据
*/
func (p *fabaoManager) UpdatePlayerFabaoDBData(player *model.PlayerObject,fabao *model.Fabao,nNowTime int64,isFirst bool) error {
	if player == nil || fabao == nil || !isFirst || fabao.MDBData != nil{
		return nil
	}

	fabao.MDBData = &pojo.FabaoData{}
	if fabao.MDBData.GetGuid() > 0 {
		return nil
	}

	guid,err := GuidGeneratorManager.GenGuid(int(constants.GuidTypeFabao))
	if err != nil {
		logger.Log.Errorf("[LoadPlayerInfoManager] UpdatePlayerFabaoDBData failed,error:%s",err.Error())
		return err
	}
	fabao.MDBData.FabaoGuid = guid
	fabao.MDBData.PlayerGuid = player.GetGuid()
	fabao.MDBData.ProTime = nNowTime
	return nil
}


















