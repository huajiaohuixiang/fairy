package manager

import (
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	PlayerNameManager *playerNameManager
	playerNameOnce sync.Once
)

type playerNameManager struct {
	BaseManager
	roleNameData *model.RoleNameData
}

func NewPlayerNameManager() *playerNameManager {
	playerNameOnce.Do(func() {
		PlayerNameManager = &playerNameManager{
			roleNameData: model.NewRoleNameData(),
		}
	})
	return PlayerNameManager
}

func (p *playerNameManager) Init()  {
	p.loadAllPlayers()
}

func (p *playerNameManager) loadAllPlayers() error {
	playerDatas := make([]*pojo.PlayerData,0)
	err := dao.PlayerDataDao.GetListByDb(&pojo.PlayerData{},&playerDatas)
	if err != nil {
		logger.Log.Errorf("loadAllPlayers failed,error:%s",err.Error())
		return err
	}

	p.initRoleNameData(playerDatas)
	return nil
}

func (p *playerNameManager) initRoleNameData(playerDatas []*pojo.PlayerData) {
	if playerDatas == nil || len(playerDatas) == 0{
		return
	}

	for _,playerData := range playerDatas {
		p.roleNameData.InitRoleName(playerData.RoleName,playerData.AccountId,playerData.Guid,
			playerData.CareerId,playerData.Rolelevel,int(playerData.SexId),playerData.AppId)
	}
}

func (p *playerNameManager) CheckForAddNewName(roleName string) bool {
	return p.roleNameData.IsExistRoleName(roleName)
}

func (p *playerNameManager) UpdateNewRoleName(roleName string,accountId,playerGuid int64,sexId int,appId string)  {
	p.roleNameData.AddRoleName(roleName,accountId,playerGuid,sexId,appId)
}

func (p *playerNameManager) DeleteTmpRoleName(roleName string)  {
	
}