package manager

import "sync"

var (
	MissionManager *missionManager
	missionManagerOnce sync.Once
)


type missionManager struct {
	BaseManager
}

/**
实例化任务管理器
 */
func NewMissionManager() *missionManager {
	missionManagerOnce.Do(func() {
		MissionManager = &missionManager{}
	})

	return MissionManager
}
