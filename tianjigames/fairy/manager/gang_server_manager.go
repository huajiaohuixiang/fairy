package manager

import (
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/pojo"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	GangServerManager *gangServerManager
	gangServerManagerOnce sync.Once
)

type gangServerManager struct {
	BaseManager
	allGangs map[int64]*pojo.GangData
}

func (p *gangServerManager) Init()  {
	//todo:初始化其他数据
	allGang := make([]*pojo.GangData,0)
	err := dao.GangDataDao.GetListByDb(&pojo.GangData{},&allGang)
	if err != nil {
		logger.Log.Errorf("[GangServerManager] Init failed,error:%s",err.Error())
		return
	}

	for _,gangData := range allGang {
		gangData.ParseBlobData()
		members := make([]*pojo.GangMemberData,0)
		err = dao.GangMemberDataDao.GetListByDb(&pojo.GangMemberData{
			GangId: gangData.GangId,
		},&members)
		if err != nil {
			logger.Log.Errorf("[GangServerManager] Init failed,error:%s",err.Error())
		}else{
			gangData.Members = members
			guidList := make([]int64,0)
			for _,member := range members {
				guidList = append(guidList,member.PlayerId)
			}
			gangData.MemberGuids = guidList
		}
		p.allGangs[gangData.GangId] = gangData
	}
}

func NewGangServerManager() *gangServerManager {
	gangServerManagerOnce.Do(func() {
		GangServerManager = &gangServerManager{
			allGangs: map[int64]*pojo.GangData{},
		}
	})
	return GangServerManager
}

func (p *gangServerManager) GetGangById(gangId int64) *pojo.GangData {
	if v,ok := p.allGangs[gangId];ok {
		return v
	}

	return nil
}

