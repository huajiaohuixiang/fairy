package manager

import (
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/topfreegames/pitaya"
	"testing"
)

func testRideManagerCommonInit(t *testing.T) *model.PlayerObject  {
	base.BaseInit()
	pitaya.RegisterTemplate(template.RideBaseKey)
	pitaya.LoadAllTemplateWithPath("../template/txt")

	dao.NewPlayerDataDao().Init()
	dao.NewAccountDataDao().Init()
	dao.NewGuidDataDao().Init()
	dao.NewGuidGenertorDao().Init()
	dao.NewRideDataDao().Init()

	NewGMAccountManager().Init()
	NewGMAccountManager().Init()
	NewFabaoManager().Init()
	NewRideManager().Init()

	v,err := dao.PlayerDataDao.Get(&pojo.PlayerData{Guid: 281194601245573138})
	assert.Nil(t, err)
	assert.NotNil(t, v)
	playerData,ok := v.(*pojo.PlayerData)
	assert.True(t, ok)
	return model.NewPlayerObject(playerData)
}

func TestRideManager_InitPlayerRide(t *testing.T) {
	player := testRideManagerCommonInit(t)
	assert.NotNil(t, player)
	b,err := RideManager.InitPlayerRide(player)
	assert.Nil(t, err)
	assert.True(t, b)
	assert.EqualValues(t, 4,player.RideList.MNRideCount)
	assert.EqualValues(t, 1622507024,player.RideList.GetRideData(player.RideList.MNDefaultIndex).MDBData.EquipTime)
}
