package manager

import (
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/tianjigames/fairy/util"
	"github.com/topfreegames/pitaya"
	"testing"
)

func testBaseStandLogicCommonInit(t *testing.T) *model.PlayerObject {
	base.BaseInit()
	pitaya.LoadAllTemplateWithPath("../template/txt")

	dao.NewPlayerDataDao().Init()
	dao.NewStandDataDao().Init()
	dao.NewGangMemberDataDao().Init()
	dao.NewGangDataDao().Init()

	NewGangServerManager().Init()
	NewStandManager().Init()

	v,err := dao.PlayerDataDao.Get(&pojo.PlayerData{Guid: 281194601245573138})
	assert.Nil(t, err)
	assert.NotNil(t, v)
	playerData,ok := v.(*pojo.PlayerData)
	assert.True(t, ok)
	return model.NewPlayerObject(playerData)
}

func TestBaseStandLogic_Init(t *testing.T) {
	testBaseStandLogicCommonInit(t)
}

func TestBaseStandLogic_InsertStand(t *testing.T) {
	player := testBaseStandLogicCommonInit(t)


	player.CombatPower = 21444
	//插入战力榜数据
	logic := StandManager.GetStandLogic(StandLogicPower)
	data,err := logic.InsertStand(logic,&protos2.MsgStandData{
		Guid: player.GetGuid(),
		Name: player.GetRoleName(),
		Value: int32(player.CombatPower+1),
		Level: int32(player.GetLv()),
		CampId: int32(player.PlayerData.CampId),
		CareerId: int32(player.GetCareerId()),
		SexId: int32(player.GetSexId()),
		GangId: player.GetGangId(),
		GangName: player.GangName,
		VipLv: int32(player.GetVipLevel()),
		CombatPower: int32(player.CombatPower+1),
	})

	assert.Nil(t, err)
	assert.NotNil(t, data)
	assert.EqualValues(t, player.CombatPower+1,data.SValue)
	assert.EqualValues(t, pojo.StandDataStateUpdate,data.State)
}

func TestBaseStandLogic_UpdateStand(t *testing.T) {
	player := testBaseStandLogicCommonInit(t)

	player.CombatPower = 21444
	//插入战力榜数据
	logic := StandManager.GetStandLogic(StandLogicPower).(*PowerStandLogic)
	param := &protos2.MsgStandData{
		Guid: player.GetGuid(),
		Name: "张三",
		Value: int32(player.CombatPower+1),
		Level: 444,
		CampId: 111,
		CareerId: 1,
		SexId: 2,
		GangId: 4442,
		GangName: "菜刀门",
		VipLv: 11,
		CombatPower: 11111,
	}
	err := logic.UpdateStand(param)

	assert.Nil(t,err)
	for _,data := range logic.SortDatas {
		if param.Guid != data.SGuid {
			continue
		}

		assert.EqualValues(t, pojo.StandDataStateUpdate,data.State)
		assert.EqualValues(t,param.Name,data.SName)
		assert.EqualValues(t,param.Level,data.SLevel)
		assert.EqualValues(t,param.CampId,data.CampId)
		assert.EqualValues(t,param.CareerId,data.CareerId)
		assert.EqualValues(t,param.SexId,data.SexId)
		assert.EqualValues(t,param.GangId,data.GangId)
		assert.EqualValues(t,param.GangName,data.GangName)
		assert.EqualValues(t,param.VipLv,data.VipLevel)
	}
}

func TestBaseStandLogic_UpdateMsg(t *testing.T) {
	testBaseStandLogicCommonInit(t)

	logic := StandManager.GetStandLogic(StandLogicPower).(*PowerStandLogic)
	logic.UpdateMsg()
	assert.EqualValues(t,len(logic.SortDatas), len(logic.ListMsgDatas))
	assert.EqualValues(t, logic.SortDatas[0].SLevel,logic.ListMsgDatas[0].Level)
}

func TestPowerStandLogic_UpdateMsg(t *testing.T) {
	testBaseStandLogicCommonInit(t)

	logic := StandManager.GetStandLogic(StandLogicPower).(*PowerStandLogic)
	logic.UpdateMsg()
	assert.EqualValues(t,len(logic.SortDatas), len(logic.ListMsgDatas))
	assert.EqualValues(t, logic.SortDatas[0].SLevel,logic.ListMsgDatas[0].Level)
}

func TestPowerStandLogic_CheckDiffDay(t *testing.T) {
	testBaseStandLogicCommonInit(t)

	logic := StandManager.GetStandLogic(StandLogicPower).(*PowerStandLogic)
	now := util.Now()
	logic.CheckDiffDay(now)
	for _,data := range logic.SortDatas {
		assert.EqualValues(t,util.DateUtil.GetDayFirstMills(now),data.ExtLong)
	}
}



























