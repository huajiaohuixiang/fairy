package manager

import (
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/topfreegames/pitaya"
	"testing"
)

func testFabaoManagerCommonInit(t *testing.T) *model.PlayerObject {
	base.BaseInit()
	pitaya.RegisterTemplate(template.FabaoBaseKey)
	pitaya.LoadAllTemplateWithPath("../template/txt")

	dao.NewPlayerDataDao().Init()
	dao.NewAccountDataDao().Init()
	dao.NewGuidDataDao().Init()
	dao.NewGuidGenertorDao().Init()
	dao.NewFabaoDataDao().Init()

	NewGMAccountManager().Init()
	NewGMAccountManager().Init()
	NewFabaoManager().Init()

	v,err := dao.PlayerDataDao.Get(&pojo.PlayerData{Guid: 281194601245573138})
	assert.Nil(t, err)
	assert.NotNil(t, v)
	playerData,ok := v.(*pojo.PlayerData)
	assert.True(t, ok)
	return model.NewPlayerObject(playerData)
}

func TestFabaoManager_InitPlayerFabao(t *testing.T) {
	player := testFabaoManagerCommonInit(t)
	assert.NotNil(t, player)
	b,err := FabaoManager.InitPlayerFabao(player)
	assert.Nil(t, err)
	assert.True(t, b)
	assert.EqualValues(t, constants.MaxFabaoSkillNum,len(player.MFabao.SkillList))
}
