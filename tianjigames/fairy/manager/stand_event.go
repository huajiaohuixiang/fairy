package manager

import (
	"errors"
	"fmt"
	"github.com/topfreegames/pitaya/logger"
)

type (
	IStandEvent interface {
		process() error
	}

	StandUpdateEvent struct {
		NLogicId int
		ParamData interface{}
	}
)

func (p *StandUpdateEvent) process() error {
	if p.NLogicId != 0 {
		logic := StandManager.GetStandLogic(p.NLogicId)
		if logic == nil {
			err := errors.New(fmt.Sprintf("[StandUpdateEvent] process failed,logicId:%d standlogic not exist",p.NLogicId))
			logger.Log.Errorf(err.Error())
			return err
		}

		_,err := logic.InsertStand(logic,p.ParamData)
		if err != nil {
			logger.Log.Errorf("[StandUpdateEvent] process failed,error:%s",err.Error())
			return err
		}
	}

	for i := 1;i<StandLogicNumber+1;i++ {
		if p.NLogicId == i {
			continue
		}

		logic := StandManager.GetStandLogic(p.NLogicId)
		if logic == nil {
			continue
		}

		err := logic.UpdateStand(p.ParamData)
		if err != nil {
			logger.Log.Errorf("[StandUpdateEvent] process failed,error:%s",err.Error())
			return err
		}
	}


	return nil
}
