package manager

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/util"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	MailManager *mailManager
	mailManagerOnce sync.Once
)

/**
邮件管理器
 */
type mailManager struct {
	BaseManager
}

func NewMailManager() *mailManager {
	mailManagerOnce.Do(func() {
		MailManager = &mailManager{}
	})
	return MailManager
}

/**
给某个玩家发送可执行邮件
 */
func (p *mailManager) SendExecuteMail(playerGuid int64,moduleType,cmdType int,commonds []string) (bool,error) {
	guid,err := GuidGeneratorManager.GenGuid(int(constants.GuidTypeMail))
	if err != nil {
		logger.Log.Errorf("[MailManager] SendExecuteMail failed,error:%s",err.Error())
		return false,err
	}

	exeMail := &pojo.ExecuteMail{
		Guid:        guid,
		PlayerGuId:  playerGuid,
		ModuleType:  moduleType,
		CommandType: cmdType,
		CreateTime:  util.Now(),
	}
	exeMail.SetCommands(commonds)

	err = dao.ExecuteMailDao.Add(exeMail)
	if err != nil {
		logger.Log.Errorf("[MailManager] SendExecuteMail failed,error:%s",err.Error())
		return false,err
	}

	return false,nil
}

























