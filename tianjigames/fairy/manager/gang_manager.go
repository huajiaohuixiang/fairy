package manager

import "sync"

var (
	GangManager *gangManager
	gangManagerOnce sync.Once
)

type gangManager struct {
	BaseManager
	gangCollectionItemId int
}

func NewGangManager() *gangManager {
	gangManagerOnce.Do(func() {
		GangManager = &gangManager{}
	})
	return GangManager
}

func (p *gangManager) GetGangCollectionItemId() int {
	return p.gangCollectionItemId
}
