package manager

type (
	MonsterInfo struct {
		existTime int64
		MonsterIndexes []int
	}

	BirthPosData struct {
		PosX int
		PosY int
		PosZ int
		Radius int
	}

	WildBossData struct {
		MonSterId int
		ChildIds []int
		ExistTime int
		IsBroadcast bool
		PosList []*BirthPosData
		RefreshTime []int64
		NextRefTime int64
	}

	WildBossManager struct {
		WildBossStatus map[int]bool
		BossDataList []*WildBossData
		ExistMonsters []*MonsterInfo
		NextTickTime int64
	}
)
