package manager

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/model"
	protos2 "github.com/tianjigames/fairy/protos"
)

var MessageError = &messageError{}

type messageError struct {
	
}

/**
获取公共的发送错误消息
 */
func (p *messageError) GetCommonPromptPacket(messageErrorId,typ int,params...string) *model.MsgData {
	return &model.MsgData{
		Route: constants.GCCommonPrompt,
		Msg: &protos2.GCCommonPromptRet{
			MessageId: int32(messageErrorId),
			Type: int32(typ),
			Params: params,
		},
	}
}
