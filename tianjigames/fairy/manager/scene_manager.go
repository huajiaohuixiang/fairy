package manager

import "sync"

var (
	SceneManager *sceneManager
	sceneManagerOnce sync.Once
)

type (
	sceneManager struct {
		BaseManager
	}

	SceneType int


)

const (
	_ SceneType = iota

	//主城
	SceneTypeStCity

	//野外
	SceneTypeStField

	//副本
	SceneTypeStCopy

	//离线竞技场
	SceneTypeStOfflineArena

	//可pvp的野外
	SceneTypeStPvpField
	//在线竞技场
	SceneTypeStOnlineArena

	//监狱场景
	SceneTypeStPrison

	//城市副本
	SceneTypeStCopyCity
)

/**
场景管理器
 */
func NewSceneManager() *sceneManager {
	sceneManagerOnce.Do(func() {
		SceneManager = &sceneManager{}
	})
	return SceneManager
}

func (p *sceneManager) Init()  {
	p.BaseManager.Init()
}

