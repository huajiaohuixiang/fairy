package manager

import "sync"

type Payment int

var (
	SpecialEventManager *specialEventManager
	specialEventOnce sync.Once
)

const (
	_ Payment = iota
	none
	collect
	post
)

/**
慝数事件管理器
 */
type specialEventManager struct {
	BaseManager
}

func NewSpecialEventManager() *specialEventManager {
	specialEventOnce.Do(func() {
		SpecialEventManager = &specialEventManager{

		}
	})

	return SpecialEventManager
}

func (p *specialEventManager) Init()  {
	//向GW请求最新的获取数据
}




























