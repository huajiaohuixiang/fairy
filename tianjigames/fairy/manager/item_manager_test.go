package manager

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/topfreegames/pitaya"
	"testing"
)


func TestItemManager_Init(t *testing.T) {
	base.BaseInit()
	pitaya.RegisterTemplate(template.ItemKey)
	pitaya.RegisterTemplate(template.ConstantKey)
	pitaya.LoadAllTemplateWithPath("../template/txt")

	NewItemManager().Init()
	ItemManager.Init()

	assert.EqualValues(t, 1,Strength10VipLimit)
	assert.EqualValues(t,17, len(ItemManager.mVirtualItemId))
	assert.EqualValues(t, constants.EquipRandAttrConst,ItemManager.equipRandAttrConst.Id)
	assert.EqualValues(t, constants.EquipWashConst,ItemManager.equipWashCons.Id)
}

func testItemManagerCommonInit(t *testing.T) *model.PlayerObject {
	base.BaseInit()
	pitaya.RegisterTemplate(template.GemKey)
	pitaya.RegisterTemplate(template.ItemKey)
	pitaya.RegisterTemplate(template.EquipKey)
	pitaya.RegisterTemplate(template.ConstantKey)
	pitaya.LoadAllTemplateWithPath("../template/txt")

	dao.NewPlayerDataDao().Init()
	dao.NewItemDao().Init()
	dao.NewGuidGenertorDao().Init()
	dao.NewGuidDataDao().Init()

	NewLoadPlayerInfoManager().Init()
	NewItemManager().Init()
	NewGuiGeneratorManager().Init()

	v,err := dao.PlayerDataDao.Get(&pojo.PlayerData{Guid: 281194601245573138})
	assert.Nil(t, err)
	assert.NotNil(t, v)
	playerData,ok := v.(*pojo.PlayerData)
	assert.True(t, ok)
	return model.NewPlayerObject(playerData)
}

func TestItemManager_CalGemBaseAttr(t *testing.T) {
	player := testItemManagerCommonInit(t)
	attrId := constants.AttrAttackPhy
	val := ItemManager.CalGemAttr(player,&model.CommonItem{TemplateId: 301001},&attrId)
	assert.EqualValues(t, 75,val)
}

/**
从数据库中加载玩家背包数据
 */
func TestItemManager_LoadItemFromDB(t *testing.T) {
	player := testItemManagerCommonInit(t)
	b,err := ItemManager.LoadItemFromDB(player)
	assert.Nil(t, err)
	assert.True(t, b)
}

func TestItemManager_CalEquipBaseAttr(t *testing.T) {
	player := testItemManagerCommonInit(t)
	b,err := ItemManager.LoadItemFromDB(player)
	assert.Nil(t, err)
	assert.True(t, b)
	assert.NotEmpty(t, player.Bag.BodyEquips)
	for _,guid := range player.Bag.BodyEquips {
		item := player.Bag.GetItemByGuid(guid)
		assert.NotNil(t, item)
		v := pitaya.GetTemplateById(template.EquipKey,fmt.Sprintf("%d",item.GetId()))
		assert.NotNil(t, v)
		equip,ok := v.(*template.Equip)
		assert.True(t, ok)

		for i:=1;i<=ItemMaxEquipAttr;i++ {
			cfg := ItemManager.GetEquipAttrConfig(equip,i)
			if cfg == nil {
				continue
			}

			rt := constants.GetAttrFlagByValue(cfg[0])
			if rt == nil {
				continue
			}

			assert.EqualValues(t, cfg[2],ItemManager.CalEquipBaseAttr(player,item,equip,rt))
		}
	}
}

func TestItemManager_InitPlayerEquip(t *testing.T) {
	player := testItemManagerCommonInit(t)
	b,err := ItemManager.LoadItemFromDB(player)
	assert.Nil(t, err)
	assert.True(t, b)
	err = ItemManager.InitPlayerEquip(player)
	assert.Nil(t, err)
	v := pitaya.GetTemplateById(template.ConstantKey,constants.ParamKeyDefaultWeapon)
	assert.NotNil(t, v)
	cfg := v.(*template.Constant)
	v1 := pitaya.GetTemplateById(template.EquipKey,fmt.Sprintf("%d",cfg.Constant7[player.GetCareerId()-1]))
	assert.NotNil(t, v1)
	equip := v1.(*template.Equip)
	guid := player.Bag.BodyEquips[equip.Postion]
	assert.NotEqualValues(t, 0,guid)
	item := player.Bag.GetItemByGuid(guid)
	assert.NotNil(t, item)
	assert.EqualValues(t, equip.Id,fmt.Sprintf("%d",item.GetId()))
	assert.True(t, item.IsBody())
	assert.False(t, item.IsBind())
	assert.EqualValues(t, 1,item.StackCount)
}


