package manager

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/logger"
	"strconv"
	"sync"
)

var (
	TreasureManager *treasureManager
	treasureManagerOnce sync.Once
)

/**
宝物管理器
 */
type treasureManager struct {
	BaseManager
}

func NewTreasureManager() *treasureManager {
	return &treasureManager{

	}
}

/**
计算宝物的加成属性值
 */
func (p *treasureManager) CalTreasureAttr(player *model.PlayerObject,item *model.CommonTreasureItem,attrId *constants.AttrFlag) int {
	if player == nil || item == nil || attrId == nil {
		return 0
	}

	v := pitaya.GetTemplateById(template.TreasureKey,strconv.FormatInt(int64(item.GetId()),10))
	if v == nil {
		return 0
	}

	return p.calTreasureAttrValue(player,v.(*template.Treasure),attrId)
}

/**
计算宝物的加成值
 */
func (p *treasureManager) calTreasureAttrValue(player *model.PlayerObject,treasure *template.Treasure,attrId *constants.AttrFlag) int {
	if player == nil || treasure == nil || attrId == nil {
		return 0
	}

	nRet := 0
	id := int(*attrId)
	nBaseValue := player.GetBaseAttr(attrId)
	for k:= 1;k< constants.TreasureAttr;k++ {
		pro := p.getTreasureAttrConfig(treasure,k)
		if pro == nil || len(pro) < constants.TreasureIdxAttrLen {
			continue
		}

		if pro[0] == id {
			nRet += LoadPlayerInfoManager.GetAttrResult(model.GetRefixTypeByValue(pro[1]),pro[2],nBaseValue)
		}else {
			nRet += LoadPlayerInfoManager.CheckSpecialAttrByFlag(attrId,constants.GetAttrFlagByValue(pro[0]),
				model.GetRefixTypeByValue(pro[1]),pro[2],nBaseValue)
		}
	}
	return nBaseValue
}

func (p *treasureManager) getTreasureAttrConfig(treasure *template.Treasure,nIdx int) []int {
	if treasure == nil || nIdx <= 0 || nIdx > constants.TreasureAttr {
		return nil
	}

	switch nIdx {
	case 1:
		return treasure.TreasureProperty1
	case 2:
		return treasure.TreasureProperty2
	case 3:
		return treasure.TreasureProperty3
	case 4:
		return treasure.TreasureProperty4
	}

	return nil
}

/**
从数据库中加载玩家宝物数据
 */
func (p *treasureManager) loadTreasureItemFromDB(player *model.PlayerObject) (bool,error) {
	playerTreasureList := player.TreasureBag
	if playerTreasureList == nil {
		playerTreasureList = model.NewPlayerTreasureBag()
	}
	treasureList := make([]*pojo.TreasureData,0)
	err := dao.TreasureDao.GetListByDb(&pojo.TreasureData{
		PlayerId: player.GetGuid(),
	},&treasureList)
	if err != nil {
		logger.Log.Errorf("[TreasureManager] loadTreasureItemFromDB failed,error:%s",err.Error())
		return false,err
	}

	if treasureList != nil && len(treasureList) > 0 {
		for _,it := range treasureList {
			cfg := pitaya.GetTemplateById(template.TreasureKey,strconv.FormatInt(int64(it.TemplateId),10))
			if cfg == nil {
				continue
			}

			item := &model.CommonTreasureItem{}
			item.MDBData = it
			playerTreasureList.AddOneItem(item)
		}
	}
	player.TreasureBag = playerTreasureList
	return true,nil
}

