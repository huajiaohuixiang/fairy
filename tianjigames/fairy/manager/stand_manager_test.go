package manager

import (
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/dao"
	"testing"
)

func testStandManagerCommonInit()  {
	base.BaseInit()
	dao.NewGangDataDao().Init()
	dao.NewGangMemberDataDao().Init()
	dao.NewStandDataDao().Init()

	NewGangServerManager().Init()
	NewStandManager().Init()
}

func TestStandManager_Init(t *testing.T) {
	testStandManagerCommonInit()
	v := StandManager.GetStandLogic(StandLogicPower)
	assert.NotNil(t, v)
	powerStdLogic,ok := v.(*PowerStandLogic)
	assert.True(t, ok)
	assert.EqualValues(t, 25,len(powerStdLogic.SortDatas))
	assert.Less(t, powerStdLogic.SortDatas[0].SRank,powerStdLogic.SortDatas[1].SRank)
}

