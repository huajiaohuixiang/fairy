package manager

import (
	"github.com/tianjigames/fairy/dao"
	"github.com/tianjigames/fairy/pojo"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	WeddingManager *weddingManager
	weddingManagerOnce sync.Once
)

/**
婚礼管理器
 */
type weddingManager struct {
	BaseManager
}

func NewWeddingManager() *weddingManager {
	weddingManagerOnce.Do(func() {
		WeddingManager = &weddingManager{

		}
	})

	return WeddingManager
}

/**
加载婚礼邀请列表
 */
func (p *weddingManager) LoadInvitationList(guid int64) ([]*pojo.WeddingInvitationData,error) {
	rs,err := dao.WeddingInvitationDataDao.GetListByDb(guid)
	if err != nil {
		logger.Log.Errorf("[WeddingManager] LoadInvitationList failed,error:%s",err.Error())
		return nil,err
	}

	return rs,nil
}
