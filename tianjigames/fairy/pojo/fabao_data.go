package pojo

import (
	"fmt"
)

type FabaoData struct {
	BaseData       `xorm:"-"`
	FabaoGuid      int64 `xorm:"'fabaoguid' pk notnull"`
	PlayerGuid     int64 `xorm:"'playerguid' default('0') notnull"`
	ProTime        int64 `xorm:"'proTime' default('0') notnull"`
	TempateId      int `xorm:"'templateid' default('0') notnull"`
	ShowTemplateId int `xorm:"'showtemplateid' default('0') notnull"`
	CurrentExp     int `xorm:"'currentexp' default('0') notnull"`
	Currentlucky   int `xorm:"'currentlucky' default('0') notnull"`
	SkillData      []uint8 `xorm:"'skillData'"`
}

func (p *FabaoData) TableName() string {
	return "fabaodata"
}

func (p *FabaoData) GetDataKey() string {
	if p.FabaoGuid <= 0 {
		return ""
	}

	return fmt.Sprintf("%d",p.FabaoGuid)
}

