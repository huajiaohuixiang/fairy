package pojo

type PlayerMailBox struct {
	BaseData                `xorm:"-"`
	PlayerGuid              int64 `xorm:"pk autoincr 'playerGuId' notnull"`
	CheckDate               int    `xorm:"'checkDate' default('0') notnull"`
	MailPrivileges          string    `xorm:"'mailprivileges' default('') notnull"`
	GroupMailIds            string    `xorm:"'groupMailIds' default('') notnull"`
	PrivateMailIds          string    `xorm:"'privateMailIds' default('') notnull"`
	SystemMailIds           string    `xorm:"'systemMailIds' default('') notnull"`
	TodaySendPrivateMailNum int    `xorm:"'todaySendPrivateMailNum' default('0') notnull"`
}

func (p *PlayerMailBox) TableName() string {
	return "playermailbox"
}