package pojo

type (
	DbData interface {
		TableName() string
		GetGuid() int64
		GetDataKey() string
		SetStatus(OptStatus)
		GetStatus() OptStatus
	}

	DbDataList []DbData

	BaseData struct {
		Status OptStatus
	}

	OptStatus int
)

const (
	OptNone   OptStatus = iota
	OptInsert
	OptUpdate
	OptDelete
)

func (p *BaseData) TableName() string {
	return ""
}


func (p *BaseData) GetGuid() int64 {
	return 0
}


func (p *BaseData) GetDataKey() string {
	return ""
}

func (p *BaseData) SetStatus(status OptStatus)  {
	p.Status = status
}

func (p *BaseData) GetStatus() OptStatus {
	return p.Status
}
