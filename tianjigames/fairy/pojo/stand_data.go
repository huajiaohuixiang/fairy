package pojo

import "fmt"

const (
	StandDataStateSaved = 0 //已存盘
	StandDataStateUpdate = 1 //待更新
	StandDataStateInsert = 2 //待插入
)

type StandData struct {
	BaseData `xorm:"-"`
	SType       int     `xorm:"pk 'stype' default('0') notnull"`
	SGuid       int64     `xorm:"pk 'sguid' default('0') notnull"`
	SRank       int     `xorm:"'srank' default('0') notnull"`
	SName		string `xorm:"'sname' default('') notnull"`
	SValue int `xorm:"'svalue' default('0') notnull"`
	SLevel int `xorm:"'slevel' default('0') notnull"`
	CampId int `xorm:"'campid' default('0') notnull"`
	CareerId int `xorm:"'careerid' default('0') notnull"`
	SexId int `xorm:"'sexid' default('-1') notnull"`
	GangId int64 `xorm:"'gangid' default('0') notnull"`
	VipLevel int `xorm:"'viplevel' default('0') notnull"`
	CombatPower int `xorm:"'combatPower' default('0') notnull"`
	ExtInt int `xorm:"'extInt' default('0') notnull"`
	ExtLong int64 `xorm:"'extLong' default('0') notnull"`
	GangName string `xorm:"-"`
	State int `xorm:"-"`
}

func NewStandData(stype int,sguid int64) *StandData {
	return &StandData{
		SType: stype,
		SGuid: sguid,
	}
}

func (p *StandData) TableName() string {
	return "standdata"
}

func (p *StandData) GetGuid() int64 {
	return p.SGuid
}

func (p *StandData) GetDataKey() string {
	return fmt.Sprintf("%d_%d",p.SType,p.SGuid)
}
