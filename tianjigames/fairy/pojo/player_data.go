package pojo

import (
	"fmt"
	"github.com/tianjigames/fairy/protos"
	"github.com/tianjigames/fairy/util"
	"github.com/topfreegames/pitaya"
	constants2 "github.com/topfreegames/pitaya/constants"
	"time"
)

const (
	RoleStateUse = 1
	RoleStateDelete = 2
	RoleStateLock = 3

	RoleStartLevel = 1//角色初始等级
	RoleStartGuideCopyId = 211//角色第一次创角时要进入的副本ID
	RoleStartSceneId = 2//角色初始场景ID
	RoleStartDirection = 45//角色初始朝向
)

type (
	PlayerData struct {
		BaseData    `xorm:"-"`
		PlayerId    int `xorm:"pk autoincr 'playerid' notnull"`
		AccountId   int64 `xorm:"'accountId' default('0') notnull"`
		ChannelId   string `xorm:"'channelId' default('') notnull"`
		BackSceneId int `xorm:"'backsceneid' default('0') notnull"`
		BackPosX    int `xorm:"'backposX' default('0') notnull"`
		BackPosY    int `xorm:"'backposY' default('0') notnull"`
		BackPosZ    int `xorm:"'backposZ' default('0') notnull"`
		BagNum      int `xorm:"'bagNum' default('72') notnull"`
		StoreNum    int `xorm:"'storeNum' default('40') notnull"`
		BankMoney   int `xorm:"'bankMoney' default('0') notnull"`
		BindBankMoney int `xorm:"'bindBankMoney' default('0') notnull"`
		CareerId int `xorm:"'careerid' default('0') notnull"`
		CloId int `xorm:"'cloId' default('0') notnull"`
		CreateTime time.Time `xorm:"'createTime' default('null')"`
		Direction int `xorm:"'direction' default('0') notnull"`
		PlayerExp int `xorm:"'playerexp' default('0') notnull"`
		GangId int64 `xorm:"'gangId' default('0') notnull"`
		Guid int64 `xorm:"'guid' default('0') notnull"`
		Hp int `xorm:"'hp' default('0') notnull"`
		LoginTime int64 `xorm:"'loginTime' default('0') notnull"`
		LogoutTime int64 `xorm:"'logoutTime' default('0') notnull"`
		MissionData []uint8 `xorm:"'missiondata' default('null')"`
		ActivityData       []uint8 `xorm:"'activityData' default('null')"`
		ChargeActivityData []uint8 `xorm:"'chargeactivityData' default('null')"`
		SevenDayActData    []uint8 `xorm:"'sevenDayActData' default('null')"`
		CardData           []uint8 `xorm:"'cardData' default('null')"`
		Money              int     `xorm:"'money' default('0') notnull"`
		Mp                 int     `xorm:"'mp' default('0') notnull"`
		PosX               int     `xorm:"'posX' default('0') notnull"`
		PosY               int     `xorm:"'posY' default('0') notnull"`
		PosZ               int     `xorm:"'posZ' default('0') notnull"`
		GmIngot            int     `xorm:"'gmIngot' default('0') notnull"`
		RoleIndex          int     `xorm:"'roleindex' default('0') notnull"`
		Rolelevel          int     `xorm:"'rolelevel' default('1') notnull"`
		RoleName           string  `xorm:"'rolename' default('') notnull"`
		AccountName        string  `xorm:"'accountname' default('') notnull"`
		SceneId            int     `xorm:"'sceneId' default('0') notnull"`
		SkillData          []uint8 `xorm:"'skillData' default('null')"`
		SkillPoint         int     `xorm:"'skillPoint' default('0') notnull"`
		EquipEssence       int     `xorm:"'equipEssence' default('0') notnull"`
		SoulEssence        int     `xorm:"'soulEssence' default('0') notnull"`
		CardEssence        int     `xorm:"'cardEssence' default('0') notnull"`
		ArenaMoney         int     `xorm:"'arenaMoney' default('0') notnull"`
		Xianyuan        int     `xorm:"'xianyuan' default('0') notnull"`
		Status          int     `xorm:"'status' default('0') notnull"`
		WeaId           int     `xorm:"'weaId' default('0') notnull"`
		WeaStarLevel    int     `xorm:"'weaStarLevel' default('0') notnull"`
		CampId          int16   `xorm:"'campId' default('0') notnull"`
		KillPoint       int16   `xorm:"'killPoint' default('0') notnull"`
		KillPointTime   int16   `xorm:"'killPointTime' default('0') notnull"`
		ChangeSceneTime int64   `xorm:"'changeSceneTime' default('0') notnull"`
		ChatTime        int64   `xorm:"'chatTime' default('0') notnull"`
		DeleteRoleTime  int64   `xorm:"'deleteRoleTime' default('0') notnull"`
		FabaoId         int     `xorm:"'fabaoId' default('0') notnull"`
		WingId          int     `xorm:"'wingId' default('0') notnull"`
		WingData []uint8 `xorm:"'wingData' default('null')"`
		TitleData []uint8 `xorm:"'titleData' default('null')"`
		TitleId int `xorm:"'titleId' default('0') notnull"`
		LastIp string `xorm:"'lastIp' default('') notnull"`
		ServerMailTime int64 `xorm:"'serverMailTime' default('0') notnull"`
		IngotShopData []uint8 `xorm:"'ingotShopData' default('null')"`
		JieYiName string `xorm:"'jieYiName' default('') notnull"`
		JieYiSubName string `xorm:"'jieYiSubName' default('') notnull"`
		JieYiTime int64 `xorm:"'jieYiTime' default('0') notnull"`
		SexId int8 `xorm:"'sexId' default('-1') notnull"`
		MarriageTime int64 `xorm:"'marriageTime' default('0') notnull"`
		MateName string `xorm:"'mateName' default('') notnull"`
		MateCareer int `xorm:"'mateCareer' default('0') notnull"`
		Prestige int `xorm:"'prestige' default('0') notnull"`
		WeddingState int16 `xorm:"'weddingState' default('0') notnull"`
		WeddingOrderTime int64 `xorm:"'weddingOrderTime' default('0') notnull"`
		WeddingGuestGuids []uint8 `xorm:"'weddingGuestGuids' default('null')"`
		BetFun      []uint8 `xorm:"'betFun' default('null')"`
		MateGuid    int64 `xorm:"'mateGuid' default('0') notnull"`
		BetIntegral int `xorm:"'betIntegral' default('0') notnull"`
		FashionId   int `xorm:"'fashionId' default('0') notnull"`
		FashionData []uint8 `xorm:"'fashionData' default('null')"`
		TimecutData []uint8 `xorm:"'timecutData' default('null')"`
		AppId       string `xorm:"'appId' default('') notnull"`
		WorldId     int `xorm:"'worldId' default('0') notnull"`
	}
	
	PlayerDataSlice []*PlayerData
)

func (*PlayerData) TableName() string {
	return "playerdata"
}

func (p *PlayerData) GetGuid() int64 {
	return p.Guid
}

func (p *PlayerData) GetDataKey() string {
	if p.Guid <= 0 {
		return ""
	}

	return fmt.Sprintf("%d",p.Guid)
}

func (p *PlayerDataSlice) Len() int {
	return len(*p)
}

/**
roleLevel越大 越靠前
 */
func (p *PlayerDataSlice) Less(i, j int) bool{
	return (*p)[i].Rolelevel < (*p)[j].Rolelevel
}
func (p *PlayerDataSlice) Swap(i, j int) {
	(*p)[i],(*p)[j] = (*p)[j],(*p)[i]
}

func (p *PlayerData) NewLcRoleInfoRet() *protos.LCRoleInfoRet {
	deleteRoleTime := int64(0)
	if p.DeleteRoleTime > 0 {
		deleteRoleTime = (pitaya.GetConfig().GetDuration(constants2.LoginServerDeleteRoleKeepTime).Milliseconds()-
			util.Now() + p.DeleteRoleTime)
	}
	createRoleTime := p.CreateTime.UnixNano() / 1e6


	return &protos.LCRoleInfoRet{
		PlayerId:        int32(p.PlayerId),
		RoleIndex:       int32(p.RoleIndex),
		RoleName:        p.RoleName,
		RoleLevel:       int32(p.Rolelevel),
		CareerId:        int32(p.CareerId),
		SexId:           int32(p.SexId),
		WeaponId:        int32(p.WeaId),
		WeaponStarLevel: int32(p.WeaStarLevel),
		ClothesId:       int32(p.CloId),
		WingId:          int32(p.WingId),
		FabaoId:         int32(p.FabaoId),
		FashionId:       int32(p.FashionId),
		LastLoginTime:   (int32)(p.LoginTime / 1000),
		DeleteRoleTime:  deleteRoleTime,
		CreateRoleTime:  createRoleTime,
	}
}

func (p *PlayerData)  GenerateMsgPlayerData() *protos.MsgPlayerData {
	msgPlayerData := &protos.MsgPlayerData{
		AccountId:p.AccountId,
		AppId:p.AppId,
		World:int32(p.WorldId),
		Accountname:p.AccountName,
		BackposX:int32(p.BackPosX),
		BackposY:int32(p.BackPosY),
		BackposZ:int32(p.BackPosZ),
		BackSceneID:int32(p.BackSceneId),
		BagNum:int32(p.BagNum),
		BankMoney:int32(p.BankMoney),
		BindBankMoney:int32(p.BindBankMoney),
		CampId:int32(p.CampId),
		CareerId:int32(p.CareerId),
		SexId:int32(p.SexId),
		ChangeSceneTime:p.ChangeSceneTime,
		ChatTime:p.ChatTime,
		CloId:int32(p.CloId),
		CreateTime:util.GetTime(p.CreateTime),
		DeleteRoleTime:p.DeleteRoleTime,
		Direction:int32(p.Direction),
		EquipEssence:int32(p.EquipEssence),
		CardEssence:int32(p.CardEssence),
		SoulEssence:int32(p.SoulEssence),
		ArenaMoney:int32(p.ArenaMoney),
		Xianyuan:int32(p.Xianyuan),
		FabaoId:int32(p.FabaoId),
		GangId:p.GangId,
		GmIngot:int32(p.GmIngot),
		Guid:p.Guid,
		Hp:int32(p.Hp),
		KillPoint:int32(p.KillPoint),
		KillPointTime:int32(p.KillPointTime),
		LastIp:p.LastIp,
		Logintime:p.LoginTime,
		LogoutTime:p.LogoutTime,
		Mp:int32(p.Mp),
		Money:int32(p.Money),
		PlayerId:int32(p.PlayerId),
		Playerexp:int32(p.PlayerExp),
		PosX:int32(p.PosX),
		PosY:int32(p.PosY),
		PosZ:int32(p.PosZ),
		RoleIndex:int32(p.RoleIndex),
		RoleLevel:int32(p.Rolelevel),
		RoleName:p.RoleName,
		SceneId:int32(p.SceneId),
		StoreNum:int32(p.StoreNum),
		SkillPoint:int32(p.SkillPoint),
		Status:int32(p.Status),
		TitleId:int32(p.TitleId),
		WeaId:int32(p.WeaId),
		WeaStarLevel:int32(p.WeaStarLevel),
		WingId:int32(p.WingId),
		ServerMailTime:p.ServerMailTime,
		ChannelId:p.ChannelId,
		JieYiName:p.JieYiName,
		JieYiSubName:p.JieYiSubName,
		JieYiTime:p.JieYiTime,
		MateName:p.MateName,
		MateCareer:int32(p.MateCareer),
		MarriageTime:p.MarriageTime,
		Prestige:int32(p.Prestige),
		WeddingState:int32(p.WeddingState),
		WeddingOrderTime:p.WeddingOrderTime,
		MateGuid:p.MateGuid,
		BetIntegral:int32(p.BetIntegral),
		FashionId:int32(p.FashionId),
	}

	if p.MissionData != nil && len(p.MissionData) > 0 {
		msgPlayerData.MissionData = make([]int32,len(p.MissionData)/8+1)
		util.ByteArrayToInt32Array(p.MissionData,0,len(p.MissionData),msgPlayerData.MissionData,0)
	}

	if p.ActivityData != nil && len(p.ActivityData) > 0 {
		msgPlayerData.ActivityData = make([]int32,len(p.ActivityData)/8+1)
		util.ByteArrayToInt32Array(p.ActivityData,0,len(p.ActivityData),msgPlayerData.ActivityData,0)
	}

	if p.ChargeActivityData != nil && len(p.ChargeActivityData) > 0 {
		msgPlayerData.ChargeActivityData = make([]int32,len(p.ChargeActivityData)/8+1)
		util.ByteArrayToInt32Array(p.ChargeActivityData,0,len(p.ChargeActivityData),msgPlayerData.ChargeActivityData,0)
	}

	if p.SevenDayActData != nil && len(p.SevenDayActData) > 0 {
		msgPlayerData.SevenDayActData = make([]int32,len(p.SevenDayActData)/8+1)
		util.ByteArrayToInt32Array(p.SevenDayActData,0,len(p.SevenDayActData),msgPlayerData.SevenDayActData,0)
	}

	if p.TimecutData != nil && len(p.TimecutData) > 0 {
		msgPlayerData.TimecutData = make([]int32,len(p.TimecutData)/8+1)
		util.ByteArrayToInt32Array(p.TimecutData,0,len(p.TimecutData),msgPlayerData.TimecutData,0)
	}

	if p.CardData != nil && len(p.CardData) > 0 {
		msgPlayerData.CardData = make([]int32,len(p.CardData)/8+1)
		util.ByteArrayToInt32Array(p.CardData,0,len(p.CardData),msgPlayerData.CardData,0)
	}

	if p.SkillData != nil && len(p.SkillData) > 0 {
		msgPlayerData.SkillData = make([]int32,len(p.SkillData)/8+1)
		util.ByteArrayToInt32Array(p.SkillData,0,len(p.SkillData),msgPlayerData.SkillData,0)
	}

	if p.WingData != nil && len(p.WingData) > 0 {
		msgPlayerData.WingData = make([]int32,len(p.WingData)/8+1)
		util.ByteArrayToInt32Array(p.WingData,0,len(p.WingData),msgPlayerData.WingData,0)
	}

	if p.TitleData != nil && len(p.TitleData) > 0 {
		msgPlayerData.TitleData = make([]int32,len(p.TitleData)/8+1)
		util.ByteArrayToInt32Array(p.TitleData,0,len(p.TitleData),msgPlayerData.TitleData,0)
	}

	if p.IngotShopData != nil && len(p.IngotShopData) > 0 {
		msgPlayerData.IngotShopData = make([]int32,len(p.IngotShopData)/8+1)
		util.ByteArrayToInt32Array(p.IngotShopData,0,len(p.IngotShopData),msgPlayerData.IngotShopData,0)
	}

	if p.WeddingGuestGuids != nil && len(p.WeddingGuestGuids) > 0 {
		msgPlayerData.WeddingInvitationData = make([]int32,len(p.WeddingGuestGuids)/8+1)
		util.ByteArrayToInt32Array(p.WeddingGuestGuids,0,len(p.WeddingGuestGuids),msgPlayerData.WeddingInvitationData,0)
	}

	if p.BetFun != nil && len(p.BetFun) > 0 {
		msgPlayerData.BetFunData = make([]int32,len(p.BetFun)/8+1)
		util.ByteArrayToInt32Array(p.BetFun,0,len(p.BetFun),msgPlayerData.BetFunData,0)
	}

	if p.FashionData != nil && len(p.FashionData) > 0 {
		msgPlayerData.FashionData = make([]int32,len(p.FashionData)/8+1)
		util.ByteArrayToInt32Array(p.FashionData,0,len(p.FashionData),msgPlayerData.FashionData,0)
	}

	return msgPlayerData
}

/**
解析playerData
 */
func MakePlayerData(data *protos.MsgPlayerData) *PlayerData {
	if data == nil {
		return nil
	}

	build := &PlayerData{
		AccountId: data.GetAccountId(),
		AppId: data.GetAppId(),
		WorldId: int(data.GetWorld()),
		AccountName: data.GetAccountname(),
		BackPosX: int(data.GetBackposX()),
		BackPosY: int(data.GetBackposY()),
		BackPosZ: int(data.GetBackposZ()),
		BackSceneId: int(data.GetBackSceneID()),
		BagNum: int(data.GetBagNum()),
		BankMoney: int(data.GetBankMoney()),
		BindBankMoney: int(data.GetBindBankMoney()),
		CampId: int16(data.GetCampId()),
		CareerId: int(data.GetCareerId()),
		SexId: int8(data.GetSexId()),
		ChangeSceneTime: data.GetChangeSceneTime(),
		ChatTime: data.GetChatTime(),
		CloId: int(data.GetCloId()),
		CreateTime: util.GetData(data.GetCreateTime()),
		DeleteRoleTime: data.GetDeleteRoleTime(),
		Direction: int(data.GetDirection()),
		EquipEssence: int(data.GetEquipEssence()),
		SoulEssence: int(data.GetSoulEssence()),
		CardEssence: int(data.GetCardEssence()),
		ArenaMoney: int(data.GetArenaMoney()),
		Xianyuan: int(data.GetXianyuan()),
		FabaoId: int(data.GetFabaoId()),
		GangId: data.GetGangId(),
		GmIngot: int(data.GetGmIngot()),
		Guid: data.GetGuid(),
		Hp: int(data.GetHp()),
		KillPoint: int16(data.GetKillPoint()),
		KillPointTime: int16(data.GetKillPointTime()),
		LastIp: data.GetLastIp(),
		LoginTime: data.GetLogintime(),
		LogoutTime: data.GetLogoutTime(),
		Mp: int(data.GetMp()),
		Money: int(data.GetMoney()),
		PlayerId: int(data.GetPlayerId()),
		PlayerExp: int(data.GetPlayerexp()),
		PosX: int(data.GetPosX()),
		PosY: int(data.GetPosY()),
		PosZ: int(data.GetPosZ()),
		RoleIndex: int(data.GetRoleIndex()),
		Rolelevel: int(data.GetRoleLevel()),
		RoleName: data.GetRoleName(),
		SceneId: int(data.GetSceneId()),
		StoreNum: int(data.GetStoreNum()),
		SkillPoint: int(data.GetSkillPoint()),
		Status: int(data.GetStatus()),
		TitleId: int(data.GetTitleId()),
		WeaId: int(data.GetWeaId()),
		WeaStarLevel: int(data.GetWeaStarLevel()),
		WingId: int(data.GetWingId()),
		ServerMailTime: data.GetServerMailTime(),
		ChannelId: data.GetChannelId(),
		JieYiName: data.GetJieYiName(),
		JieYiSubName: data.GetJieYiSubName(),
		JieYiTime: data.GetJieYiTime(),
		MateName: data.GetMateName(),
		MateCareer: int(data.GetMateCareer()),
		MarriageTime: data.GetMarriageTime(),
		Prestige: int(data.GetPrestige()),
		WeddingState: int16(data.GetWeddingState()),
		WeddingOrderTime: data.GetWeddingOrderTime(),
		MateGuid: data.GetMateGuid(),
		BetIntegral: int(data.GetBetIntegral()),
		FashionId: int(data.GetFashionId()),
	}

	if data.GetMissionData() != nil && len(data.GetMissionData()) > 0 {
		build.MissionData = util.Int32ArrayToByteArray(data.MissionData)
	}

	if data.GetActivityData() != nil && len(data.GetActivityData()) > 0 {
		build.ActivityData = util.Int32ArrayToByteArray(data.GetActivityData())
	}

	if data.GetChargeActivityData() != nil && len(data.GetChargeActivityData()) > 0 {
		build.ChargeActivityData = util.Int32ArrayToByteArray(data.GetChargeActivityData())
	}

	if data.GetSevenDayActData() != nil && len(data.GetSevenDayActData()) > 0 {
		build.SevenDayActData = util.Int32ArrayToByteArray(data.GetSevenDayActData())
	}

	if data.GetTimecutData() != nil && len(data.GetTimecutData()) > 0 {
		build.TimecutData = util.Int32ArrayToByteArray(data.GetTimecutData())
	}

	if data.GetCardData() != nil && len(data.GetCardData()) > 0 {
		build.CardData = util.Int32ArrayToByteArray(data.GetCardData())
	}

	if data.GetSkillData() != nil && len(data.GetSkillData()) > 0 {
		build.SkillData = util.Int32ArrayToByteArray(data.GetSkillData())
	}

	if data.GetWingData() != nil && len(data.GetWingData()) > 0 {
		build.WingData = util.Int32ArrayToByteArray(data.GetWingData())
	}

	if data.GetTitleData() != nil && len(data.GetTitleData()) > 0 {
		build.TitleData = util.Int32ArrayToByteArray(data.GetTitleData())
	}

	if data.GetIngotShopData() != nil && len(data.GetIngotShopData()) > 0 {
		build.IngotShopData = util.Int32ArrayToByteArray(data.GetIngotShopData())
	}

	if data.GetWeddingInvitationData() != nil && len(data.GetWeddingInvitationData()) > 0 {
		build.WeddingGuestGuids = util.Int32ArrayToByteArray(data.GetWeddingInvitationData())
	}

	if data.GetBetFunData() != nil && len(data.GetBetFunData()) > 0 {
		build.BetFun = util.Int32ArrayToByteArray(data.GetBetFunData())
	}

	if data.GetFashionData() != nil && len(data.GetFashionData()) > 0 {
		build.FashionData = util.Int32ArrayToByteArray(data.GetFashionData())
	}

	return build
}