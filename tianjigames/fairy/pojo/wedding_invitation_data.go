package pojo

import "fmt"

type WeddingInvitationData struct {
	BaseData    `xorm:"-"`
	Id          int `xorm:"'id' pk autoincr notnull"`
	PlayerGuid  int64 `xorm:"'playerguid' default('0') notnull"`
	ManGuid     int64 `xorm:"'manguid' default('0') notnull"`
	WomanGuid   int64 `xorm:"'womanguid' default('0') notnull"`
	OrderTime   int64 `xorm:"'orderTime' default('0') notnull"`
	ManName     string `xorm:"'manName' defualt('') notnull"`
	WomanName   string `xorm:"'womanName' default('') notnull"`
	ManCareer   int `xorm:"'manCareer' default('0') notnull"`
	WomanCareer int `xorm:"'womanCareer' default('0') notnulL"`
}

func (p *WeddingInvitationData) TableName() string {
	return "weddinginvitation"
}

func (p *WeddingInvitationData) GetGuid() int64 {
	return int64(p.Id)
}

func (p *WeddingInvitationData) GetDataKey() string {
	return fmt.Sprintf("wedding_invitation_%d_%d_%d",
		p.PlayerGuid,p.ManGuid,p.WomanGuid)
}
