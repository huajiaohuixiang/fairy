package pojo

import (
	"fmt"
	"strings"
)

const commandsSplitDot = ";"

type ExecuteMail struct {
	BaseData    `xorm:"-"`
	Guid        int64  `xorm:"pk 'guid'  notnull"`
	CommandType int    `xorm:"'cmmandType' default('0') notnull"`
	Commands    string `xorm:"'commands' default('') notnull"`
	CreateTime  int64  `xorm:"'createTime' default('0') notnull"`
	ExecTime    int64  `xorm:"'execTime' default('0') notnull"`
	ModuleType  int    `xorm:"'moduleType' default('0') notnull"`
	PlayerGuId  int64  `xorm:"'playerGuId' default('0') notnull"`
}

func (p *ExecuteMail) TableName() string {
	return "executemail"
}

func (p *ExecuteMail) GetGuid() int64 {
	return p.Guid
}

func (p *ExecuteMail) GetDataKey() string {
	if p.Guid <= 0 {
		return ""
	}

	return fmt.Sprintf("%d",p.Guid)
}

func (p *ExecuteMail) SetCommands(cmdList []string) string {
	if cmdList == nil || len(cmdList) <= 0 {
		return p.Commands
	}
	
	strs := strings.Split(p.Commands,commandsSplitDot)
	all := make([]string,0)
	for _,str := range strs {
		if str == "" {
			continue
		}

		all = append(all,str)
	}

	all = append(all,cmdList...)
	rs := strings.Join(all,commandsSplitDot)
	rs = fmt.Sprintf("%s%s",rs,commandsSplitDot)
	p.Commands = rs
	return rs
}
