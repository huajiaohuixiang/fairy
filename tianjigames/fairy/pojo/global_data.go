package pojo

const (
	GlobalDataWorldIdKey = 17
)

type GlobalData struct {
	BaseData `xorm:"-"`
	Id       int `xorm:"pk 'id' default('0') notnull"`
	NKey     int `xorm:"pk 'nKey' default('0') notnull"`
	Worth    string `xorm:"'worth' default('') notnull"`
	Ext      int `xorm:"'ext' default('0') notnull"`
	Ext1     int64 `xorm :"'ext1' default('0') notnull"`
}

func (p *GlobalData) TableName() string {
	return "globaldata"
}
