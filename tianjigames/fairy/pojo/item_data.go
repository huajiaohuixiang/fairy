package pojo

import "fmt"

type ItemData struct {
	BaseData    `xorm:"-"`
	PlayerGuid  int64 `xorm:"pk autoincr 'playerguid' notnull"`
	Items       []uint8 `xorm:"items"`
	DeleteItems []uint8 `xorm:"deleteitems"`
	BuyItems    []uint8 `xorm:"buyitems"`
}

func (p *ItemData) TableName() string {
	return "itemdata"
}

func (p *ItemData) GetDataKey() string {
	if p.PlayerGuid <= 0 {
		return ""
	}

	return fmt.Sprintf("item_data_%d",p.PlayerGuid)
}
