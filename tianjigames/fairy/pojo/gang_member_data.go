package pojo

import (
	"fmt"
	"time"
)

type GangMemberData struct {
	BaseData        `xorm:"-"`
	PlayerId int64	`xorm:"pk 'playerId' notnull"`
	GangId int64	`xorm:"pk 'gangid' notnull"`
	Atk int `xorm:"'atk' default('0') notnull"`
	GangContriDegree int `xorm:"'gangcontridegree' default('0') notnull"`
	GangCurWeekContri int `xorm:"'gangcurweekcontri' default('0') notnull"`
	IncContriDegreeTime int `xorm:"'inccontridegreetime' default('null')"`
	GangJob int `xorm:"'gangjob' default('0') notnull"`
	GangMaxContriDegree int `xorm:"'gangMaxContriDegree' default('0') notnull"`
	JoinTime time.Time `xorm:"'joinTime' default('null')"`
	LoginTime time.Time `xorm:"'loginTime' default('null')"`
	LogoutTime time.Time `xorm:"'logoutTime' default('null')"`
	UserLevel int `xorm:"'userLevel' default('0') notnull"`
	UserName string `xorm:"'userName' default('') notnull"`
	UserProfession int `xorm:"'userProfession' default('0') notnull"`
	GangGiftCount int `xorm:"'ganggiftcount' default('0') notnull"`
	WeekGangJob int `xorm:"'weekgangjob' default('0') notnull"`
	WeekGangContriDegree int `xorm:"'weekgangcontridegree' default('0') notnull"`
	WeekGangActivity int `xorm:"'weekgangactivity' default('0') notnull"`
	VipLevel int `xorm:"'viplevel' default('0') notnull"`
	WelfareTime int64 `xorm:"'welfaretime' default('0') notnull"`
	GangDonateTime int64 `xorm:"'gangdonatetime' default('0') notnull"`
	GangDonateCount int `xorm:"'gangdonatecount' default('0') notnull"`
	GangCircleTime int64 `xorm:"'gangcircletime' default('0') notnull"`
	GangCircleMissionId string `xorm:"'gangcirclemissionid' default('') notnull"`
	GangTreePoint int `xorm:"'gangtreepoint' default('0') notnull"`
	GangTreePray int `xorm:"'gangtreepray' default('0') notnull"`
	GangTreeTime int64 `xorm:"'gangtreetime' default('0') notnull"`
	GangDonateMoney int `xorm:"'gangdonatemoney' default('0') notnull"`
	GangMemberNotice int `xorm:"'gangmembernotice' default('0') notnull"`
}

func (p *GangMemberData) TableName() string {
	return "gangmemberdata"
}

func (p *GangMemberData) GetGuid() int64 {
	return p.PlayerId
}

func (p *GangMemberData) GetDataKey() string {
	if p.PlayerId == 0 || p.GangId == 0 {
		return ""
	}

	return fmt.Sprintf("%d_%d",p.GangId,p.PlayerId)
}
