package pojo

import "fmt"

type RideData struct {
	BaseData   `xorm:"-"`
	RideGuid   int64 `xorm:"pk 'rideguid' notnull""`
	EquipTime  int64 `xorm:"'equipTime' default('0') notnull"`
	PlayerGuid int64 `xorm:"'playerguid' default('0') notnull"`
	ProTime    int64 `xorm:"'proTime' default('0') notnull"`
	TemplateId int `xorm:"'templateid' default('0') notnull"`
	RideIndex  int `xorm:"'rideindex' default('0') notnull"`
	DefFlag    int `xorm:"'defflag' default('0') notnull"`
	RideFlag   int `xorm:"'rideflag' default('0') notnull"`
}

func (p *RideData) TableName() string {
	return  "ridedata"
}

func (p *RideData) GetGuid() int64 {
	return p.RideGuid
}

func (p *RideData) GetDataKey() string {
	if p.RideGuid <= 0 {
		return ""
	}

	return fmt.Sprintf("%d",p.RideGuid)
}
