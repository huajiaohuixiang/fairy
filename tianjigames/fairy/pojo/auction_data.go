package pojo

import "fmt"

type AuctionData struct {
	BaseData        `xorm:"-"`
	ItemGuid        int64 `xorm:"'itemguid' pk notnull"`
	PlayerId        int64 `xorm:"'playerId' default('0') notnull"`
	PlayerAccountId int64 `xorm:"'playeraccountId' default('0') notnull"`
	ProTime         int64 `xorm:"'protime' default('0') notnull"`
	TemplateId      int 	`xorm:"'templateid' default('0') notnull"`
	StackCount      int `xorm:"'stackCount' default('0') notnull"`
	Price           int `xorm:"'price' default('0') notnull"`
	Valid           int `xorm:"'valid' default('0') notnull"`
	PlayerName      string `xorm:"'playername' default('') notnull"`
	AppId           string `xorm:"'appId' default('') notnull"`
}

func (p *AuctionData) TableName() string  {
	return "auctiondata"
}

func (p *AuctionData) GetDataKey() string {
	if p.ItemGuid <= 0 {
		return ""
	}

	return fmt.Sprintf("auction_dataa_%d",p.ItemGuid)
}
