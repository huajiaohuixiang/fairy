package pojo

import (
	"fmt"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/util"
	"time"
)

type (
	GangData struct {
		BaseData        `xorm:"-"`
		GangId       int64    `xorm:"pk 'gangid' notnull"`
		AcceptJoin	int 	`xorm:"'acceptJoin' default('0') notnull"`
		ApplyMembers []uint8	`xorm:"'applyMembers' default('null')"`
		GangCreatetime time.Time `xorm:"'gangcreatetime' default('null')"`
		GangFlag	int 	`xorm:"'gangFlg' default('0') notnull"`
		GangGold	int 	`xorm:"'ganggold' default('0') notnull"`
		MaxGangGold	int 	`xorm:"'maxganggold' default('0') notnull"`
		GangHonor	int 	`xorm:"'ganghonor' default('0') notnull"`
		GangLevel	int 	`xorm:"'ganglevel' default('0') notnull"`
		GangBuilding	string 	`xorm:"'gangbuilding' default('') notnull"`
		GangSkillList	string 	`xorm:"'gangskilllist' default('') notnull"`
		GangName	string 	`xorm:"'gangName' default('') notnull"`
		GangNotice	string 	`xorm:"'gangNotice' default('') notnull"`
		MemberMax	int 	`xorm:"'membermax' default('0') notnull"`
		GangCamp	int 	`xorm:"'gangcamp' default('0') notnull"`
		WeekGangLevel	int 	`xorm:"'weekganglevel' default('0') notnull"`
		WeekGangActivity	int 	`xorm:"'weekgangactivity' default('0') notnull"`
		CalWeekActivitytime	int 	`xorm:"'calweekactivitytime' default('0') notnull"`
		WelfareCalTime	int 	`xorm:"'welfarecaltime' default('0') notnull"`
		GangGiftCount	int 	`xorm:"'ganggiftcount' default('0') notnull"`
		GangBuildPoint	int 	`xorm:"'gangbuildpoint' default('0') notnull"`
		GangActData []uint8	`xorm:"'gangactdata' default('null')"`
		GangTreePoint	int 	`xorm:"'gangtreepoint' default('0') notnull"`
		GangBeastId	int 	`xorm:"'gangBeastId' default('0') notnull"`
		GangBeastExp	int 	`xorm:"'gangBeastExp' default('0') notnull"`
		GangBeastName	string 	`xorm:"'gangBeastName' default('') notnull"`
		DayGangActivity	int 	`xorm:"'dayGangActivity' default('0') notnull"`
		GangTreeTime	int64 	`xorm:"'gangtreetime' default('0') notnull"`
		ApplMemberList map[int64]*ApplyMember `xorm:"-"`
		Members []*GangMemberData `xorm:"-"`
		MemberGuids []int64 `xorm:"-"`
		TotalGangAttack int `xorm:"-"`
		PreFightSum int `xorm:"-"`
		ActData *GangActData `xorm:"-"`
	}

	GangActData struct {
		MVersion byte  //活动数据版本
		MXiulianStartTime int64  //帮会打坐修炼开始时间
		MXiulianXiangId int //帮会打坐修炼使用的香
 		MDefenceStartTime int64  //帮会防御修炼开始时间
		MTruckStartTime int64 //帮会押票开启时间
		MTruckRobMoney int //今日劫镖资金
		MTruckRobTime int64 //上一次劫镖完成时间
		MBeastDevilTime int64 //上一次神兽心魔时间
	}

	ApplyMember struct {
		PlayerId int64
		CarrerId int
		Name string
		Lv int
		Power int
		VipLevel int
	}
)

func (p *GangData) TableName() string {
	return "gangdata"
}

func (p *GangData) GetDataKey() string {
	return fmt.Sprintf("%d",p.GangId)
}

func (p *GangData) GetGuid() int64 {
	return p.GangId
}

/**
读取二级制数据
 */
func (p *GangData) ParseBlobData()  {
	p.ActData = UnSerializeGangActData(p.GangActData)
	p.ApplMemberList = UUnSerializeGangApplyMemeberList(p.ApplyMembers)
}

func UnSerializeGangActData(in []byte) *GangActData {
	if in == nil || len(in) <= 1{
		return &GangActData{}
	}
	
	ret := &GangActData{}
	nBeginIdx := 0
	ret.MVersion = in[nBeginIdx]
	nBeginIdx += 1

	switch ret.MVersion {
	case 0:
		ret.MXiulianStartTime = util.ByteArrayToInt64(in,nBeginIdx)
		nBeginIdx += 8
		ret.MDefenceStartTime = util.ByteArrayToInt64(in,nBeginIdx)
		nBeginIdx += 8
		ret.MXiulianXiangId = util.ByteArrayToInt(in,nBeginIdx)
		nBeginIdx += 4
		ret.MTruckStartTime = util.ByteArrayToInt64(in,nBeginIdx)
		nBeginIdx += 8
		ret.MTruckRobTime = util.ByteArrayToInt64(in,nBeginIdx)
		nBeginIdx += 8
		ret.MTruckRobMoney = util.ByteArrayToInt(in,nBeginIdx)
		nBeginIdx += 4
		ret.MBeastDevilTime = util.ByteArrayToInt64(in,nBeginIdx)
		nBeginIdx += 8
	default:
	}
	return ret
}

func UUnSerializeGangApplyMemeberList(in []byte) map[int64]*ApplyMember {
	if in == nil || len(in) <= 1 {
		return map[int64]*ApplyMember{}
	}

	nBeginIdx := 0
	mVersion := in[nBeginIdx]
	nBeginIdx += 1
	switch mVersion {
	case 0:
		count := util.ByteArrayToInt(in,nBeginIdx)
		nBeginIdx += 4

		retMp := make(map[int64]*ApplyMember,count)
		for i :=0;i<count;i++ {
			member := &ApplyMember{}
			member.PlayerId = util.ByteArrayToInt64(in,nBeginIdx)
			nBeginIdx += 8
			member.CarrerId = util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			nameLen := in[nBeginIdx]
			nBeginIdx += 1
			if nameLen > constants.MaxMissionStringParamLen {
				nameLen = constants.MaxMissionStringParamLen
			}

			namebyte := make([]byte,nameLen)
			for j:=byte(0);j<nameLen;j++ {
				namebyte[j] = in[nBeginIdx]
				nBeginIdx += 1
			}

			member.Name = string(namebyte)
			member.Lv = util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			member.Power = util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			member.VipLevel = util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			retMp[member.PlayerId] = member
		}
		return retMp
	default:

	}

	return nil
}
