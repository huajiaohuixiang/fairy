package pojo



type GuidData struct {
	BaseData `xorm:"-"`
	Id       int `xorm:"pk autoincr 'id' notnull"`
	Perch    int `xorm:"'perch' default('0') notnull"`
	Low      int `xorm:"'low' default('0') notnull"`
	Des      string `xorm:"'des' default('') notnull"`
}

func (g *GuidData) TableName() string {
	return "guiddata"
}

func (p *GuidData) GetGuid() int64 {
	return int64(p.Id)
}