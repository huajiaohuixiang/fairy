package pojo

import "fmt"

type TreasureData struct {
	BaseData      `xorm:"-"`
	TreasureGuid  int64 `xorm:"pk 'treasureguid' notnull"`
	PlayerId      int64 `xorm:"'playerId' default('0') notnull"`
	TemplateId    int `xorm:"'templateid' default('0') notnull"`
	Valid         int `xorm:"'valid' tinyint default('0') notnull"`
	TreasureState int `xorm:"treasurestate" tinyint default('0') notnull`
	TreasurePos   int `xorm:"treasurepos" tinyint default('0') notnull`
	TreasureExp   int `xorm:"treasureexp" tinyint default('0') notnull`
}

func (p *TreasureData) TableName() string {
	return "treasuredata"
}

func (p *TreasureData) GetDataKey() string {
	if p.TreasureGuid <= 0 {
		return ""
	}

	return fmt.Sprintf("%d",p.TreasureGuid)
}

func (p *TreasureData) GetGuid() int64 {
	return p.TreasureGuid
}
