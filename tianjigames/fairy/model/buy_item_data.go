package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/util"
)

type BuyItemData struct {
	MVersion byte
	ShopIndex []int
	BuyItemTime []int
	BuyItemCount []int
}

func NewBuyItemData() *BuyItemData {
	return &BuyItemData{
		ShopIndex: make([]int,constants.LimitShopItemCount),
		BuyItemTime: make([]int,constants.LimitShopItemCount),
		BuyItemCount: make([]int,constants.LimitShopItemCount),
	}
}

/**
反序列化购买的物品数据
 */
func UnSerializeBuyItemData(data []byte) *BuyItemData {
	newMData := NewBuyItemData()
	if data != nil && len(data) > 0 {
		newMData.ReadObject(data)
	}
	return newMData
}

/**
读取数据
 */
func (p *BuyItemData) ReadObject(in []byte)  {
	nBeginIdx := 0
	p.MVersion = in[nBeginIdx]
	nBeginIdx += 1

	switch p.MVersion {
	case 0:
		nSize := constants.LimitShopItemCount*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.ShopIndex,0)
		nBeginIdx += nSize

		nSize = constants.LimitShopItemCount*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.BuyItemTime,0)
		nBeginIdx += nSize

		nSize = constants.LimitShopItemCount*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.BuyItemCount,0)
		nBeginIdx += nSize
	}
}
