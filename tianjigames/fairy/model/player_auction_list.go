package model

type PlayerAuctionList struct {
	LastAskAuctionListTime int64
	AskAuctionDataTimeInterval int
	LastPutOnAuctionItemTime int64
	PutOnAuctionItemTimeInterval int64
	AuctionList map[int64]*CommonAuctionItem
}
