package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/util"
)

/**
商城元宝数据
 */
type IngotShopData struct {
	MVersion byte //数据版本
	ShopIndex []int //限购商品的标id
	BuyItemTime []int //限购商品购买时间
	BugItemCount []int //限购商品购买数量

}

func NewIngotShopData() *IngotShopData {
	return &IngotShopData{
		ShopIndex: make([]int,constants.LimitIngotItemCount),
		BuyItemTime: make([]int,constants.LimitIngotItemCount),
		BugItemCount: make([]int,constants.LimitIngotItemCount),
	}
}

/**
限购商品的购买数据
 */
func UnSerializeIngotShopData(data []byte) *IngotShopData {
	newMData := NewIngotShopData()
	if data != nil && len(data) > 0 {
		newMData.ReadObject(data)
	}

	return newMData
}

/**
读取数据
 */
func (p *IngotShopData) ReadObject(in []byte)  {
	nBeginIdx := 0
	p.MVersion = in[nBeginIdx]
	nBeginIdx ++

	if p.MVersion == 0 {
		//限购商品下标
		nSize := constants.LimitIngotItemCount*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.ShopIndex,0)
		nBeginIdx += nSize

		//限购商品购买时间
		nSize = constants.LimitIngotItemCount*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.BuyItemTime,0)
		nBeginIdx += nSize

		//限购商品购买数量
		nSize = constants.LimitIngotItemCount*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.BugItemCount,0)
		nBeginIdx += nSize
	}
}
