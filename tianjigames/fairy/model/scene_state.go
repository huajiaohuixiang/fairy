package model

type SceneState int

const (
	_ SceneState = iota
	STNone
	STInit
	STLoad
	STSleep
	STSelect
	STRunning
	STClosed
)
