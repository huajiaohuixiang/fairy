package model

type AiStatusLogic struct {
	MPTargetCharObj *BaseCharObj
	MNTargetCharObjId int
	MNTargetCharObjType int
	MBConfirmDirection bool
	MPTargetPos *Pos
	MNTraceIndex int
}
