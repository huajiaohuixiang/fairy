package model

type AiEventLogic struct {
	MPAiSelf *AiObjectBase
	MAEventList []*AiEventData
}
