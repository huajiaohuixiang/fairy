package model

import protos2 "github.com/tianjigames/fairy/protos"

type RobotType int

const (
	_ RobotType = iota
	DefaultType
	ArenaType
	ScultureTypePlayer
	ScultureTypeNpc
	Lansquenet
	GangTrunk
	Resource
)

const (
	HpperHigh = 70
	HpperMid = 50
	HpperLow = 30
	HpperBegin = 90
	SoulActiveTime = 20*1000
	BeginActiveTime = 10*1000
)

type RobotPlayer struct {
	BaseCharObj
	ReadId        int64
	MsgRankData   *protos2.MsgRankData
	Mp            int
	Seat          int
	EnemyReadId   int64
	enemyId       int
	MIsActiveSoul bool
	MIsCheckHigh  bool
	MIsCheckMid   bool
	MIsCheckLow   bool
	MActiveTimer  *NowTimer
	ReConnectMap  map[int64]*DelayTimer
	LanInterval   int64
	HpInc         int
	MpInc         int
	GangName      string
	MyType        RobotType
	FuncNpcId     int
}
