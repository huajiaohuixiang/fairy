package model

type SceneNpcData struct {
	SceneNpcId string
	SceneId int
	NpcId int
	BrushPosArr []*Pos
}
