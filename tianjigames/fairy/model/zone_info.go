package model

type ZoneInfo struct {
	MNZoneW int
	MNZoneH int
	MNZoneWSize int
	MNZoneHSize int
	MNZoneSize int
	MNRefreshSize int
}
