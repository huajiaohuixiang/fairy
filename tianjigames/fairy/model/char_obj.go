package model

import "github.com/tianjigames/fairy/constants"

type (
	CharObj interface {
		IBaseObj
		GetLv() int
		GetCareerId() int
		GetSexId() int
		GetSkillData() *SkillData
		SetCalRefixAttr(isCalRefixAttr bool)
		SetTempCalRefixAttr(value int)
		GetTempCalRefixAttr() int
		GetBaseAttr(attrId *constants.AttrFlag) int
		SetAttrFlag(attrId *constants.AttrFlag,bValue bool) bool
		IsFollow() bool
		IsPhysicAttack() bool //是否有物理攻击
		GetAttributeList() *AttributeList
		GetZoneID() int
		GetSceneID() int
		SetInit(isInit bool)
	}

	BaseCharObj struct {
		ActionObj
		MGuid int64
		MNZoneId int
		SegmentId int
		MScene *Scene
		MModelId int
		MOwnerId int
		MOwnerType int
		MIsActive bool
		MAi *AiObjectBase
		MImpactData []*ImpactData
		MSkillData *SkillData
		MSillCDList *CoolDownList
		MMoveParam *MoveParam
		SkillParam *SkillParam
		MNDirection int
		mbHosted bool
		IsInit bool
		MAAttributeList *AttributeList
		MNTriggerId       int
		MBDodge           bool
		MUSkill           bool
		MNAiConfigId      int
		MNStatus          int
		MAStatusParam     []int
		IdleTime          int64
		SegmentTime       int64
		NextSeg           int64
		IsCalRefixAttr    bool
		NTempCalRefixAttr int
		Lv                int
		CareerId          int
		SexId             int
		MNFightCamp       int
		IsShapShift       bool
		ModleSize         float32
		NoLanOwner        *BaseCharObj
		BelongId          int
		BelongGuid        int64
		TeamLanId         int
	}
)

func NewBaseCharObj() *BaseCharObj {
	rs := &BaseCharObj{
		ActionObj:ActionObj{
			MMontionData:NewMotionData(),
		},
		MNZoneId: -1,
		MIsActive: true,
		MImpactData: make([]*ImpactData,constants.MaxImpact),
		MSillCDList: NewCoolDownList(),
		MMoveParam: NewMoveParam(),
		SkillParam: NewSkillParam(),
		MUSkill: true,
		MNAiConfigId: -1,
		MNStatus: constants.RoleStatusPosNormal,
		MAStatusParam: make([]int,constants.ObjStatusNum),
		SegmentTime: 500,
		CareerId: 1,
		ModleSize: 1.0,
	}

	rs.MAAttributeList = NewAttributeList(rs)
	return rs
}

func (p *BaseCharObj) GetScene() *Scene {
	return p.MScene
}

func (p *BaseCharObj) GetLv() int {
	return p.Lv
}

func (p *BaseCharObj) GetCareerId() int {
	return p.CareerId
}

func (p *BaseCharObj) GetSexId() int {
	return p.SexId
}

func (p *BaseCharObj) IsFollow() bool {
	return false
}

func (p *BaseCharObj) IsPhysicAttack() bool {
	return false
}

func (p *BaseCharObj) GetSceneID() int {
	return p.MScene.MnSceneId
}

func (p *BaseCharObj) SetInit(isInit bool)  {
	p.IsInit = isInit
}

/**
获取玩家所在的位置
 */
func (p *BaseCharObj) GetZoneID() int {
	return p.MNZoneId
}


/**
设置某个属性是否需要重新计算的标记
 */
func (p *BaseCharObj) SetAttrFlag(attrId *constants.AttrFlag,bValue bool) bool {
	if attrId == nil {
		return false
	}

	if int(*attrId) <= int(constants.AttrMaxIndex) || int(*attrId) >= int(constants.AttrMaxIndex) {
		return false
	}


	p.MAAttributeList.SetRefixAttrFlag(attrId,bValue)

	var tAttrId constants.AttrFlag
	switch *attrId {//力道变更 影响物攻、物防御
	case constants.AttrStr:
		tAttrId = constants.AttrAttackPhy
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrDefencePhy
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrSpi://元气变更 影响法功、法防
		tAttrId = constants.AttrAttackMagic
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrDefenceMagic
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrCon://体质改变 影响生命值、法力值
		tAttrId = constants.AttrMaxHp
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrMaxMp
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrAgi://敏捷度改变 影响名中职、闪避值
		tAttrId = constants.AttrHit
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrMiss
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrAttackAllelem://攻击类改变 影响所有攻击类属性
		tAttrId = constants.AttrAttackIce
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrAttackFire
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrAttackThunder
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrAttackPoison
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrResistAllelem://所有抗类属性
		tAttrId = constants.AttrResistIce
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrResistFire
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrResistThunder
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrResistPoison
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrReduceResistAllelem://减抗类属性改变 影响所有减抗属性
		tAttrId = constants.AttrReduceResistIce
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrReduceResistFire
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrReduceResistThunder
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrReduceResistPoison
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrAttackAll://所有攻击类属性
		tAttrId = constants.AttrAttackPhy
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrAttackMagic
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	case constants.AttrDefenceAll://所有防御类属性
		tAttrId = constants.AttrDefencePhy
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
		tAttrId = constants.AttrDefenceMagic
		p.MAAttributeList.SetRefixAttrFlag(&tAttrId,bValue)
	}
	return true
}

func (p *BaseCharObj) SetCalRefixAttr(isCalRefixAttr bool)  {
	p.IsCalRefixAttr = isCalRefixAttr
}

func (p *BaseCharObj) SetTempCalRefixAttr(value int) {
	p.NTempCalRefixAttr = value
}

func (p *BaseCharObj) GetTempCalRefixAttr() int {
	return p.NTempCalRefixAttr
}

func (p *BaseCharObj) GetSkillData() *SkillData {
	return p.MSkillData
}

func (p *BaseCharObj) GetAttributeList() *AttributeList {
	return p.MAAttributeList
}



/**
获取基本属性值
 */
func (p *BaseCharObj) GetBaseAttr(attrId *constants.AttrFlag) int {
	nRet := p.MAAttributeList.GetBaseAttr(attrId)
	if p.IsCalRefixAttr && p.NTempCalRefixAttr > 0 {
		nRet += p.NTempCalRefixAttr
	}

	return nRet
}

