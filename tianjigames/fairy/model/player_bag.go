package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/tianjigames/fairy/util"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/logger"
	"strconv"
)

type PlayerBag struct {
	DeleteSize int
	Flag int
	MDBData *pojo.ItemData
	BodyEquips map[int]int64 //穿在身上的装备
	BagItems []int64 //背包中所有的物品
	Items map[int64]*CommonItem //背包中所有wup
	DeleteItems []*CommonItem //已经删除的物品
	BuyItems []*CommonItem //商店中回购的物品列表
	GemItems map[int]int64 //宝石列表
	StoreItems []int64 //仓库中所有的物品
	OverDueItems []int64 //过期的物品
	EquipWashRet *CommonItem //临时保存的洗练结果
	BuyItemData *BuyItemData //购买的物品数据
}

/**
获取背包物品条目对象
 */
func (p *PlayerBag) GetItemByGuid(guid int64) *CommonItem {
	return p.Items[guid]
}

/**
实例化玩家背包
 */
func NewPlayerBag() *PlayerBag {
	return &PlayerBag{
		BodyEquips: map[int]int64{},
		BagItems: make([]int64,0),
		Items: map[int64]*CommonItem{},
		BuyItems: make([]*CommonItem,0),
		DeleteItems: make([]*CommonItem,0),
		GemItems: map[int]int64{},
		StoreItems: make([]int64,0),
		OverDueItems: make([]int64,0),
	}
}

/**
反序列化背包数据
 */
func (p *PlayerBag) UnSerializeData(dbData *pojo.ItemData)  {
	p.MDBData = dbData
	in := dbData.Items
	if in != nil && len(in) > 0 {
		nBeginIdx := 0
		version := in[nBeginIdx]
		nBeginIdx ++

		count := util.ByteArrayToInt(in,nBeginIdx)
		nBeginIdx += 4

		for i:=0;i<count;i++ {
			item := NewCommonItem()
			item.MVersion = version
			nBeginIdx += item.ReadObject(in,nBeginIdx)
			if !item.IsValid() {
				continue
			}

			v := pitaya.GetTemplateById(template.ItemKey,strconv.FormatInt(int64(item.GetId()),10))
			if v == nil {
				logger.Log.Errorf("PlayerBag:UnSerializData config error, player:%d, %d",
					dbData.PlayerGuid,item.GetId())
				continue
			}

			item.InitData(v.(*template.Item))
			item.RepairStackCount()
			p.AddOneItem(item)
		}
	}
}

/**
反序列化背包中被删除的物品
 */
func (p *PlayerBag) UnSerializeDeleteData(dbData *pojo.ItemData)  {
	 in  := dbData.DeleteItems
	 if in == nil || len(in) == 0 {
	 	return
	 }

	 nBeginIdx := 0
	 version := in[nBeginIdx]
	 nBeginIdx += 1

	 count := util.ByteArrayToInt(in,nBeginIdx)
	 nBeginIdx += 4

	 for i:=0;i<count;i++ {
	 	item := NewCommonItem()
	 	item.MVersion = version
	 	nBeginIdx += item.ReadObject(in,nBeginIdx)
	 	v := pitaya.GetTemplateById(template.ItemKey,strconv.FormatInt(int64(item.GetId()),10))
	 	if v == nil {
			logger.Log.Errorf("PlayerBag::UnSerializDeleteData config error, player:%d, %d",
				dbData.PlayerGuid,item.GetId())
	 		continue
		}

		item.InitData(v.(*template.Item))
	 	p.DeleteItems = append(p.DeleteItems,item)
	 }
}

/**
向背包中添加一个条目
 */
func (p *PlayerBag) AddOneItem(item *CommonItem)  {
	if item == nil || !item.IsValid() {
		return
	}

	p.Items[item.ItemGuid] = item
	if item.IsBody() {
		if item.MItemCfg.Type == int(constants.ItemTypeEquip) { //穿在身上的装备
			p.BodyEquips[item.MItemCfg.SubType] = item.ItemGuid
		}else if item.MItemCfg.Type == int(constants.ItemTypeGem) {//宝石
			p.GemItems[int(item.GemIndex)] = item.ItemGuid
		}
	}else if item.IsStore() {
		p.StoreItems = append(p.StoreItems,item.ItemGuid)
	}else {
		p.BagItems = append(p.BagItems,item.ItemGuid)
	}
}

func (p *PlayerBag) SetModify(isModify bool)  {

}






