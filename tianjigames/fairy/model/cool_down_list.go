package model

type CoolDownList struct {
	MData map[int]*CoolDownData
}

func NewCoolDownList() *CoolDownList {
	return &CoolDownList{
		MData: map[int]*CoolDownData{},
	}
}
