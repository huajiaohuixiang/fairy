package model

import "container/list"

type (
	Node struct {
		t       Triangle
		cost    int
		arrival int
		Parent  *Node
		Next    *Node
	}

	PathFinder struct {
		MapTriangle map[int]*Triangle
		MBIsInit bool
		OpenedList list.List
		ClosedList []int
	}
)
