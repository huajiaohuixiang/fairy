package model

type Rect struct {
	NStartx int
	NStartz int
	NEndx int
	NEndz int
}

func (p *Rect) ClearUp()  {
	p.NStartx = 0
	p.NStartz = 0
	p.NEndx = 0
	p.NEndz = 0
}

/**
判断某个顶点是否为矩形范围中的一个顶点
 */
func (p *Rect) IsContain(x,z int) bool {
	if x < p.NStartx || x > p.NEndx || z < p.NStartz || z > p.NEndz {
		return false
	}

	return true
}