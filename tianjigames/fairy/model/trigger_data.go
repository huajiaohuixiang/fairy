package model

type TriggerData struct {
	MIsActive bool
	MCheckProfession bool
	MNTriggerType int
	MNTriggerConditionType int
	MNLastTriggerId []int
	MNElaspsedTime int
	MNConditionNpcId int
	MNConditionDialogId int
	MAResultParam []int
	MNConditionPointIdx int
}
