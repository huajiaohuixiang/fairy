package model

type MoveParam struct {
	IsActive bool
	NCurMoveTime int
	MNCurPointIndex int
	MNCurPointDist int
	MNStopRange int
	StartPos *Pos
	EndPos *Pos
	MPathPoint []*Pos
	Direction int
}

func NewMoveParam() *MoveParam {
	rs := &MoveParam{
		StartPos: NewPos(0,0,0),
		EndPos: NewPos(0,0,0),
	}

	rs.ClearUp()
	return rs
}

func (p *MoveParam) ClearUp()  {
	p.IsActive = true
	p.NCurMoveTime = 0
	p.MNStopRange = 0
	p.StartPos.SetX(0)
	p.StartPos.SetY(0)
	p.StartPos.SetZ(0)
	p.EndPos.SetX(0)
	p.EndPos.SetY(0)
	p.EndPos.SetZ(0)
	p.MPathPoint = nil
	p.MNCurPointIndex =  0
	p.MNCurPointDist = -1
	p.Direction = 0
}
