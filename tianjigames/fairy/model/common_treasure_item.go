package model

import "github.com/tianjigames/fairy/pojo"

type CommonTreasureItem struct {
	MIsFirstCreate bool
	MDBData *pojo.TreasureData
}

func (p *CommonTreasureItem) GetId() int {
	return p.MDBData.TemplateId
}

func (p *CommonTreasureItem) IsValid() bool {
	return p.MDBData.Valid > 0
}

func (p *CommonTreasureItem) GetGuid() int64 {
	return p.MDBData.TreasureGuid
}

/**
宝物是否装备在身上
 */
func (p *CommonTreasureItem) IsBody() bool {
	return p.GetTreausrePos() > 0
}

/**
宝物装备在身上的位置
 */
func (p *CommonTreasureItem) GetTreausrePos() int {
	return p.MDBData.TreasurePos
}
