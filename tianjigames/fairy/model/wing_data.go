package model

/**
翅膀数据
 */
type WingData struct {
	TemplateId int
	ActiveTime int64
}

/**
实例化翅膀数据对象
 */
func NewWingData(templateId int,activeTime int64) *WingData {
	return &WingData{
		TemplateId: templateId,
		ActiveTime: activeTime,
	}
}


