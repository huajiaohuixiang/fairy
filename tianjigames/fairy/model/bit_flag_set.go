package model

type BitFlagSet struct {
	MABitFlags []byte
	MNBitSize int
	MNByteSize int
}

func NewBitFlagSet(nBitSize int) *BitFlagSet {
	return &BitFlagSet{
		MNBitSize: nBitSize,
		MNByteSize: 1 + nBitSize/8,
		MABitFlags: make([]byte,1 + nBitSize/8),
	}
}

/**
获取位状态
 */
func (p *BitFlagSet) GetFlagByIndex(nIdx int) bool {
	if nIdx < 0 || nIdx > p.MNBitSize {
		return false
	}

	return p.MABitFlags[nIdx >> 3]&(1<<(nIdx%8)) != 0
}

/**
清理位状态
 */
func (p *BitFlagSet) ClearFlagByIndex(nIdx int)  {
	if nIdx < 0 || nIdx >= p.MNBitSize {
		return
	}

	p.MABitFlags[nIdx >> 3] &= ^(0x01 << (nIdx%8))
}

/**
标记位状态
 */
func (p *BitFlagSet) MarkFlagByIndex(nIdx int)  {
	if nIdx < 0 || nIdx >= p.MNBitSize {
		return
	}

	p.MABitFlags[nIdx >> 3] |= 0x01 << (nIdx%8)
}

func (p *BitFlagSet) ReadObject(in []byte,startIdx int) int {
	rs := 0
	for i:= startIdx;i < len(p.MABitFlags) && i<len(in);i++ {
		p.MABitFlags[i] = in[i]
		rs += 1
	}

	return rs
}











