package model

type VoteData struct {
	Id int
	CurrentIndex int
	ResultArray []int
	PlayerIdArray []int
}
