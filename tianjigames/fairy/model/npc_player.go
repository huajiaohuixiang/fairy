package model

type NpcType int

const (
	_ NpcType = iota
	Default
	Collection
	Normal
	BuffDrug
	Trigger
	CollectTreasure
	CoollectGlass
	MaonrFlag
	ScoreBall
)

type NpcPlayer struct {
	MId              int
	MAdaptionId      int
	MPos             *Pos
	IsDelete         bool
	BrushType        int
	BrushTime        int64
	MBirthPos        *Pos
	Hp               int
	CharacterType    int
	MoveSpeedBackup  int
	TriggerNpc       int
	MDelayTickTime   int64
	MDiedTime        int64
	MOwnedTime       int64
	MMaxOwnedTime    int64
	GangId           int64
	OwnerList        []int
	MissionOwnerList []int
	AttackWave       int16
	Resource         ResourceData
	AttackCount      map[int]int

}
