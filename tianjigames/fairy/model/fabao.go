package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/tianjigames/fairy/util"
)

type Fabao struct {
	MIsFirstCreate bool
	MDBData *pojo.FabaoData
	MFabaoBase *template.FabaoBase
	SkillList []int
}

func NewFabao() *Fabao {
	return &Fabao{
		SkillList: make([]int,constants.MaxFabaoSkillNum),
	}
}

func (p *Fabao) GetId() int {
	if p.MDBData == nil {
		return 0
	}

	return p.MDBData.TempateId
}

func (p *Fabao) SetId(nId int)  {
	if p.MDBData == nil {
		return
	}

	p.MDBData.TempateId = nId
}

func (p *Fabao) GetShowTemplateId() int {
	if p.MDBData == nil {
		return 0
	}

	return p.MDBData.ShowTemplateId
}

func (p *Fabao) SetShowId(nId int)  {
	if p.MDBData == nil {
		return
	}

	p.MDBData.ShowTemplateId = nId
}

func (p *Fabao) GetFabaoExp() int {
	if p.MDBData == nil {
		return 0
	}

	return p.MDBData.CurrentExp
}

func (p *Fabao) GetFabaoLuck() int {
	if p.MDBData == nil {
		return 0
	}

	return p.MDBData.Currentlucky
}

/**
初始化法宝技能
 */
func (p *Fabao) InitSkill()  {
	for i := 0;i<constants.MaxFabaoSkillNum;i++ {
		p.SkillList[i] = 0
	}
}

/**
弄脏玩家法宝所有的属性
 */
func (p *Fabao) MarkAllAttr(player *PlayerObject)  {
	p.DirtyByAttrFlag(player,p.MFabaoBase.Attr1)
	if player.IsPhysicAttack() {
		p.DirtyByAttrFlag(player,p.MFabaoBase.Attr2_0)
	}else {
		p.DirtyByAttrFlag(player,p.MFabaoBase.Attr2_1)
	}
	p.DirtyByAttrFlag(player,p.MFabaoBase.Attr3)
	p.DirtyByAttrFlag(player,p.MFabaoBase.Attr4)
	p.DirtyByAttrFlag(player,p.MFabaoBase.Attr5)
	p.DirtyByAttrFlag(player,p.MFabaoBase.Attr6)
	p.DirtyByAttrFlag(player,p.MFabaoBase.Attr7)
	p.DirtyByAttrFlag(player,p.MFabaoBase.Attr8)
}

/**
弄脏法宝的某个属性
 */
func (p *Fabao) DirtyByAttrFlag(player *PlayerObject,attr []int)  {
	if attr[0] <= 0 {
		return
	}

	player.SetAttrFlag(constants.GetAttrFlagByValue(attr[0]),true)
}

/**
反序列化法宝技能数据
 */
func (p *Fabao) UnserializeFabaoSkillData() bool {
	if p.MDBData == nil {
		return false
	}

	if p.MDBData.SkillData == nil {
		p.InitSkill()
	}else {
		p.ReadObject(p.MDBData.SkillData)
	}
	return true
}

func (p *Fabao) ReadObject(in []byte)  {
	size := constants.MaxFabaoSkillNum*4
	util.ByteArrayToIntArray(in,0,size,p.SkillList,0)
}