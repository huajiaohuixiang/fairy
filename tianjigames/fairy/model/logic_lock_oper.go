package model

type LockFlag int

const (
	_ LockFlag = iota - 1
	LockBag
	LockSave
	LockCommitExp
	MaxLock
)
