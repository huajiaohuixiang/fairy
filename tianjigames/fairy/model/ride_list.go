package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/template"
	"github.com/tianjigames/fairy/util"
)

type (
	RideList struct {
		MData []*Ride
		MNRideCount int
		MNDefaultIndex int
		MNRideIndex int
	}

	Ride struct {
		MBIsFirstCreate bool
		MDBData *pojo.RideData
		MRideBase *template.RideBase
	}
)

func NewRideList() *RideList {
	return &RideList{
		MData: make([]*Ride,constants.RideMaxNumber),
	}
}

/**
获取坐骑数据
 */
func (p *RideList) GetRideData(nIndex int) *Ride {
	if nIndex < 0 || nIndex >= constants.RideMaxNumber {
		return nil
	}

	return p.MData[nIndex]
}

func (p *RideList) SetRideData(nIdx int,data *Ride) int {
	if nIdx < 0 || nIdx >= constants.RideMaxNumber {
		return -1
	}

	if p.MData[nIdx] == nil && data != nil {
		p.MNRideCount += 1
	}

	p.MData[nIdx] = data
	return data.GetIndex()
}

func (p *RideList) SetDefaultIndex(nDefaultIdx int,player *PlayerObject)  {
	if p.MNDefaultIndex >=0 && p.MNDefaultIndex < constants.RideMaxNumber {
		ride := p.MData[p.MNDefaultIndex]
		if ride != nil {
			ride.MarkAllAttr(player)
		}
	}

	p.MNDefaultIndex = nDefaultIdx
	if p.MNDefaultIndex >= 0 && p.MNDefaultIndex < constants.RideMaxNumber {
		ride := p.MData[p.MNDefaultIndex]
		if ride != nil {
			ride.MarkAllAttr(player)
		}
	}
}

func (p *Ride) SetId(nId int)  {
	if p.MDBData == nil {
		return
	}

	p.MDBData.TemplateId = nId
}

func (p *Ride) GetId() int {
	if p.MDBData == nil {
		return 0
	}

	return p.MDBData.TemplateId
}

func (p *Ride) SetIndex(nIdx int)  {
	if p.MDBData == nil {
		return
	}

	p.MDBData.RideIndex = nIdx
}

func (p *Ride) GetIndex() int {
	if p.MDBData == nil {
		return 0
	}

	return p.MDBData.RideIndex
}

func (p *Ride) SetDefFlag(nFlag int)  {
	if p.MDBData == nil {
		return
	}

	p.MDBData.DefFlag = nFlag
}

/**
坐骑是否可以被删除
*/
func (p *Ride) CanRemove() bool {
	if p.MRideBase == nil {
		return true
	}

	if p.MRideBase.ActiveTime < 0 {
		return false
	}

	equipTime := p.MDBData.EquipTime
	nowTime := util.Now()

	if nowTime - equipTime > int64(p.MRideBase.ActiveTime)*24*3600 {
		return true
	}

	return false
}

func (p *Ride) MarkAllAttr(player *PlayerObject)  {
	var careerAttr1 []int
	var careerAttr2 []int
	if player.IsPhysicAttack() {
		careerAttr1 = p.MRideBase.SpecialPhyAttr1
		careerAttr2 = p.MRideBase.SpecialPhyAttr2
	}else {
		careerAttr1 = p.MRideBase.SpecialMagicAttr1
		careerAttr2 = p.MRideBase.SpecialMagicAttr2
	}

	at := constants.AttrMoveSpeed
	player.SetAttrFlag(&at,true)
	player.SetAttrFlag(constants.GetAttrFlagByValue(careerAttr1[0]),true)
	player.SetAttrFlag(constants.GetAttrFlagByValue(careerAttr2[0]),true)
	player.SetAttrFlag(constants.GetAttrFlagByValue(p.MRideBase.CommonAttr1[0]),true)
	player.SetAttrFlag(constants.GetAttrFlagByValue(p.MRideBase.CommonAttr2[0]),true)
	player.SetAttrFlag(constants.GetAttrFlagByValue(p.MRideBase.CommonAttr3[0]),true)
	player.SetAttrFlag(constants.GetAttrFlagByValue(p.MRideBase.CommonAttr4[0]),true)

}

