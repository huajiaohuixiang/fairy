package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/template"
	"github.com/tianjigames/fairy/util"
	"github.com/topfreegames/pitaya"
	"strconv"
)

/**
任务数据
 */
type MissionDataList struct {
	MNMainMissionId int
	MVersion byte //任务版本
	MNCount byte //任务数量
	MaMission []*MissionData //玩家身上正在进行的任务数组 最多10个
	MaMissionDoneFlags []int //任务的完成标志数组
	MaMissionData []int //自定义任务数据
	MaMissionDataFlag []int //自定义任务数据标志
}

/**
实例化一个任务列表对象 
 */
func NewMisssionDataList() *MissionDataList {
	rs := &MissionDataList{
		MaMission: make([]*MissionData,constants.MaxCharMissionNum),
		MaMissionDoneFlags: make([]int,constants.MaxCharMissionFlagLen),
		MaMissionData: make([]int,constants.MaxCharMissionDataNum),
		MaMissionDataFlag: make([]int,constants.MaxCharMissionFlagDataNum),
	}

	for i,_ := range rs.MaMission {
		rs.MaMission[i] = NewMissionData()
		rs.MaMission[i].ClearUp()
	}
	return rs
}

/**
将任务数据反序列化玩家身上
 */
func UnSerializeMissionListData(data []byte) *MissionDataList {
	newMData := NewMisssionDataList()
	if data != nil && len(data) > 0 {
		newMData.ReadObject(data)
	}
	return newMData
}

/**
反序列化数据
 */
func (p *MissionDataList) ReadObject(in []byte) int {
	p.MVersion = in[0] //读出任务版本号
	nBeginIdx := 1
	p.MNCount = in[nBeginIdx]//读出任务数量
	nBeginIdx ++

	if p.MNCount > constants.MaxCharMissionNum {
		p.MNCount = constants.MaxCharMissionNum
	}

	//读出任务标志位
	nSize := constants.MaxCharMissionFlagLen * 4
	util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.MaMissionDoneFlags,0)
	nBeginIdx += nSize

	//获取自定义任务存储
	nSize = constants.MaxCharMissionDataNum * 4
	util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.MaMissionData,0)
	nBeginIdx += nSize

	//自定义数据标志
	nSize = constants.MaxCharMissionFlagDataNum * 4
	util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.MaMissionDataFlag,0)
	nBeginIdx += nSize

	bCheck := false
	for i := byte(0);i<p.MNCount;i++ {
		nBeginIdx += p.MaMission[i].ReadObject(in,nBeginIdx)
		if bCheck {
			v := pitaya.GetTemplateById(template.MissionBaseKey,strconv.FormatInt(int64(p.MaMission[i].MNMissionId),10))
			if v != nil {
				pMissionTable := v.(*template.MissionBase)
				if pMissionTable.TaskClassify == constants.McMissionMain {
					p.MNMainMissionId = p.MaMission[i].MNMissionId
					bCheck = false
				}
			}
		}
	}

	return nBeginIdx
}


