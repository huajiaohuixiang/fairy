package model

import protos2 "github.com/tianjigames/fairy/protos"

type PlayerInfo struct {
	FullData *protos2.MsgPlayerData
	PlayerId int
	AccountId int64
	AppId string
	WorldId int
	PlayerGuid int64
	Ticket string
	AccName string
	Name string
	ServerType string
	ServerId string
	SceneId int
	Level int
	Power int
	GangId int
	VipLevel int
	CareeId int
	IsOnline bool
	TeamId int
}
