package model

/**
定时器对象
 */
type NowTimer struct {
	MNTickTerm int64 //定时器周期执行的时间间隔
	MNTickOld int64 //定时器上一次开始计时的起始时间
	MBOper bool //是否设置了定时时间
	MBOver bool
}

func NewNowTimer() *NowTimer {
	return &NowTimer{}
}

/*
是否设置了时间时间
 */
func (p *NowTimer) IsSetTimer() bool {
	return p.MBOper
}

/**
开启定时器
 */
func (p *NowTimer) BeginTimer(nTerm,nNow int64)  {
	p.MBOper = true
	p.MNTickOld = nNow
	p.MNTickTerm = nTerm
}

/**
定时时间是否已到 如果计时时间已到 重新设置定时器的时间
 */
func (p *NowTimer) CountingTimer(nNow int64) bool {
	if !p.MBOper {
		return false
	}

	nNew := nNow
	if nNew < p.MNTickTerm + p.MNTickOld {
		return false
	}

	p.MNTickOld = nNew
	return true
}
