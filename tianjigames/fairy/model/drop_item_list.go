package model

import (
	"github.com/streadway/handy/atomic"
	"github.com/tianjigames/fairy/protos"
)

const ItemDropLifeTime = 120

/**
掉落物品
 */
type DropItemList struct {
	DropDatas map[int]map[int]*protos.MsgDropItem
	DropIdBase atomic.Int
}

func NewDropItemList() *DropItemList {
	return &DropItemList{
		DropDatas: map[int]map[int]*protos.MsgDropItem{},
		DropIdBase: 0,
	}
}
