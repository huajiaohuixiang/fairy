package model

import (
	"github.com/tianjigames/fairy/pojo"
	"github.com/tianjigames/fairy/util"
)

type WeddingData struct {
	Version byte
	GuestCount byte
	GuestGuidList []int64
	InvitationDataList []*pojo.WeddingInvitationData
	RemindedList []*pojo.WeddingInvitationData
	HasRemindedMe bool
}

/**
婚礼数据
 */
func NewWeddingData() *WeddingData {
	return &WeddingData{
		GuestGuidList: make([]int64,0),
		RemindedList: make([]*pojo.WeddingInvitationData,0),
	}
}

/**
反序列化婚礼数据
 */
func UnSerializeWeddingData(data []byte) *WeddingData {
	newMData := NewWeddingData()
	if data != nil && len(data) > 0 {
		newMData.ReadObject(data)
	}
	return newMData
}

/**
读取数据
 */
func (p *WeddingData) ReadObject(in []byte)  {
	nBeginIdx := 0
	p.Version = in[nBeginIdx]
	nBeginIdx += 1
	p.GuestCount = in[nBeginIdx]
	nBeginIdx += 1
	for i := byte(0);i<p.GuestCount;i++ {
		p.GuestGuidList[i] = util.ByteArrayToInt64(in,nBeginIdx)
		nBeginIdx += 8
	}
}





















