package model

import (
	"github.com/tianjigames/fairy/protos"
	"github.com/tianjigames/fairy/template"
)

const (
	SkillParamFatal = 0
	SKILLParamIgnore = 1
	SkillParamJoyo = 2
	SkillParamKnowing = 3
	SkillParamSize = 4
)

type SkillParam struct {
	BaseData     *template.SkillBase
	MNTargeId    int
	MNTargetType int
	MTargetPos   *Pos
	MNSectionNum int
	MParam       []int
	ToObjList    []*protos.MsgTargetInfo
}

func NewSkillParam() *SkillParam {
	return &SkillParam{
		MNTargetType: -1,
		MTargetPos: NewPos(0,0,0),
		MParam: make([]int,SkillParamSize),
	}
}
