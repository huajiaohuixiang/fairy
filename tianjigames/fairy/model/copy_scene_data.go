package model

const MaxParamSize = 32

type CopySceneData struct {
	MNConfigBaseId    int
	MNSceneBaseid     int
	MTimer            *NowTimer
	MDelayTimer       *NowTimer
	MReadyTimer       *NowTimer
	MNFightId         int
	MBCanComplete     bool
	IsCopyFinished    bool
	MMemberList       []int
	MWukuiRobotData   map[int64]*RobotPlayer
	Seat              int
	GameStatus        int
	MNTeamId          int
	MNOwnerId         int
	MNAvgLevel        int
	MBSingle          bool
	MNCopyDifficult   int
	ActiveTime        int64
	StartTime         int64
	TickSecond        int64
	MNStar            int
	MMemberFLop       map[int]int
	MHongBaoList      []*HongBaoData
	MVoteDataList     []*VoteData
	GangNpcMap        map[string]int
	PlayerRightMap    map[int64]*PlayerInfo
	playerLeftkMap    map[int64]*PlayerInfo
	RightGangId       int64
	LeftGangId        int64
	GangBeastId       int
	BCreateGlasss     bool
	GatherClassNum    int
	LastGatherNum     int
	LastGatherTrigger int
	GangCopyType      GangSceneType
	IntegralLock      []byte
}

func NewCopySceneData() *CopySceneData {
	return &CopySceneData{
		Seat:              -1,
		GameStatus:        -1,
		MNCopyDifficult:   -1,
		GangBeastId:       -1,
		LastGatherNum:     -1,
		LastGatherTrigger: -1,
		GangCopyType:      Invalid,
	}
}