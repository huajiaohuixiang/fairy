package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/util"
)

type ActivityDataType int
type ActivityPrizeFlag int

const (
	_ ActivityDataType = iota
	ActivityDataTypeGetIngot
	ActivityDataTypeUseIngot
	ActivityDataTypeChargeNumTotal
	ActivityDataTypeLstChargeNum
	ActivityDataTypeChargeNumDaily
	ActivityDataTypeLimit6TimeChargeNumTotal
	ActivityDataTypeMax
)

const (
	_ ActivityPrizeFlag = iota
	ActivityPrizeFlagComplete
	ActivityPrizeFlagHasDone
	ActivityPrizeFlagHasMail
	ActivityPrizeFlagMax
)

type (
	DataUnit struct {
		MDataType int
		MDayValue int
	}

	PrizeUnit struct {
		MActivityId int
		MPrizeFlag *BitFlagSet
	}

	ActivityData struct {
		MVesion byte
		MDataCount byte
		MPrizeCount byte
		MADataUnit map[int]*DataUnit
		MAPrizeUnit []*PrizeUnit
		MMinActivityId int
	}
)

/**
精彩活动数
 */
func NewActivityData() *ActivityData {
	return &ActivityData{
		MADataUnit: map[int]*DataUnit{},
		MAPrizeUnit: []*PrizeUnit{},
	}
}

/**
获取单元数据
 */
func NewDataUnit(nDataType,nDataValue int) *DataUnit {
	return &DataUnit{
		MDataType: nDataType,
		MDayValue: nDataValue,
	}
}

func NewPrizeUnit(nActivityId int) *PrizeUnit {
	return &PrizeUnit{
		MActivityId: nActivityId,
		MPrizeFlag: NewBitFlagSet(int(ActivityPrizeFlagMax)),
	}
}

/**
反序列化数据
*/
func UnSerializeActivityData(data []byte) *ActivityData {
	newMData := NewActivityData()
	if data != nil && len(data) > 0 {
		newMData.ReadObject(data)
	}

	return newMData
}

/**
读取数据
 */
func (p *ActivityData) ReadObject(in []byte) {
	nBeginIdx := 0
	p.MVesion = in[nBeginIdx]
	nBeginIdx ++

	if p.MVesion == 0 {
		p.MDataCount = in[nBeginIdx]
		nBeginIdx ++

		p.MPrizeCount = in[nBeginIdx]
		nBeginIdx ++

		if p.MPrizeCount > constants.MaxActivitySavaNum {
			p.MPrizeCount = constants.MaxActivitySavaNum
		}

		for i := byte(0);i<p.MDataCount;i ++ {
			dataType := util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4

			dataValue := util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4

			p.MADataUnit[dataType] = NewDataUnit(dataType,dataValue)
		}

		for i := byte(0);i<p.MPrizeCount;i ++ {
			nActivetyId := util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4

			if p.MMinActivityId > nActivetyId {
				p.MMinActivityId = nActivetyId
			}

			unit := NewPrizeUnit(nActivetyId)
			nBeginIdx += unit.MPrizeFlag.ReadObject(in,nBeginIdx)
			p.MAPrizeUnit = append(p.MAPrizeUnit,unit)
		}
	}
}
