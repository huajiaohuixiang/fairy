package model

const (
	BrushAtStart = 1
	BrushAfterOneTrigger = 2
	BrushAfterTime = 3
	BrushAfterMonsterDie = 4
	BrushAfterDialogId = 5
	BrushAfterTvId = 6
	BrushAfterTrunkEnd = 7

	TriggerTypeBrushMonster = 1
	TriggerTypeBigWord = 2
	TriggerTypeDialog = 3
	TriggerTypeFloatTip = 4
	TriggerTypeTimeDown = 5
	TriggerTypeTv = 6
	TriggerTypeObsAdd = 7
	TriggerTypeObsDel = 8
	TriggerTypeBrushNpc = 9
	TriggerTypeCollecitonCollider = 10
	TriggerTypeActiveCollectionNpc = 11
	TriggerTypeCreateAimPos = 12
	TriggerTypeMotifyNpcAistate = 13
	TriggerTYPeMotifyAliveTime = 14
	TriggerTypeNpcBufferTreasure = 15
)


type (
	AliveCheckData struct {
		TriggerId int
		BCheckProfession bool
		TriggerType int
		ResultParam []int
		proTime int64
	}

	MonsterTriggerManager struct {
		CopyNpcDataMap map[string]*CopySceneNpcData
		MRefreshedNpcMap map[int][]int
		MAliveTriggers map[int]*AliveCheckData
		MATrigglePool *TriggerData
		MNTriggerPoolPos int
	}


)
