package model

import (
	"github.com/tianjigames/fairy/protos"
	"time"
)

const (
	TeamStatusWait = 1 //等待状态的队伍
	TeamStatusCheck = 2 //检查中的队伍
	TeamStatusVote = 3 //投票中的队伍
	TeamStatusBattle = 4 //战斗中的队伍
	TeamStatusDelete = 5 //删除中的队伍

	VoteStatusInit = 0 //投票状态初始化
	VoteStatusYes = 1 //投票通过
	VoteStatusNo = 2 //投票未通过
)

/**
组队数据对象
 */
type (
	TeamData struct {
		TeamId int
		LeaderId int
		Status int
		AutoJoin bool

		CopySceneId int
		CopyDifficultType int
		SceneId int

		LastCallTime int64 //上一次组队召唤时间
		CallSceneId int
		CallPosX int
		CallPosY int
		CallPosZ int

		mapMembers    map[int]*TeamMember
		bufferMembers map[int]*TeamMember //缓存没有雇佣兵的队伍的队员信息
		applyMembers  map[int]*TeamMember //申请入队的玩家信息
		inviteMembers map[int]*TeamMember //要请入队的玩家信息


	}

	TeamMember struct {
		PlayerId int
		Name string
		Level int
		Career int
		SexId int
		MaxHp int
		JoinTime int64
		VoteStatus byte
		IsLanSquenet bool
	}
)

/**
实例化一个组队数据
*/
func NewTeamData(teamId,leaderId int) *TeamData {
	return &TeamData{
		TeamId: teamId,
		LeaderId: leaderId,
		Status: TeamStatusWait,
		AutoJoin: false,
	}
}

/**
实例化一个组队成员
 */
func NewTeamMember(playerId int) *TeamMember {
	return &TeamMember{
		PlayerId: playerId,
		JoinTime: time.Now().UnixNano() / 1e6,
	}
}


/**
获取所有不是雇佣兵的组队成员
 */
func (p *TeamData) GetMembers() map[int]*TeamMember {
	return p.getBufferMap()
}

/**
获取零时的bufferMap
 */
func (p *TeamData) getBufferMap() map[int]*TeamMember {
	p.bufferMembers = map[int]*TeamMember{}
	for k,v := range p.mapMembers {
		if v.IsLanSquenet {
			continue
		}

		p.bufferMembers[k] = v
	}

	return p.bufferMembers
}

/**
序列化组队数据
 */
func (p *TeamData) ToTeamInfoBuilder() *protos.MsgTeamInfo {
	autoTeam := int32(0)
	if p.AutoJoin {
		autoTeam = 1
	}

	builder := &protos.MsgTeamInfo{
		TeamId: int32(p.TeamId),
		TeamStatus: int32(p.Status),
		IsAutoTeam: autoTeam,
		TeamMemberList: []*protos.MsgTeamMemberInfo{},
	}

	for _,v := range p.mapMembers {
		builder.TeamMemberList = append(builder.TeamMemberList,p.ToTeamMemberInfo(v))
	}

	return builder
}

/**
序列化组队队员数据
 */
func (p *TeamData) ToTeamMemberInfo(member *TeamMember) *protos.MsgTeamMemberInfo {
	isLeader := int32(32)
	if p.LeaderId == member.PlayerId {
		isLeader = 1
	}
	isLansquenet := int32(0)
	if member.IsLanSquenet {
		isLansquenet = int32(1)
	}

	builder := &protos.MsgTeamMemberInfo{
		PlayerId: int32(member.PlayerId),
		RoleLevel: int32(member.Level),
		PlayerName: member.Name,
		CareerId: int32(member.Career),
		SexId: int32(member.SexId),
		MaxHp: int32(member.MaxHp),
		IsLeader: isLeader,
		IsLansquenet: isLansquenet,
	}

	return builder
}

/**
删除某个
 */
func (p *TeamData) DelMember(playerId int) {
	delete(p.mapMembers,playerId)
	if playerId == p.LeaderId {
		p.AutoChangeLeader()
	}
}

/**
更改leader
 */
func (p *TeamData) AutoChangeLeader()  {
	if len(p.mapMembers) == 0 {
		p.LeaderId = 0
	}else {
		var leader *TeamMember
		for _,v := range p.mapMembers {
			if v.IsLanSquenet {
				continue
			}

			if leader == nil || leader.JoinTime < v.JoinTime {
				leader = v
			}
		}

		if leader != nil {
			p.LeaderId = leader.PlayerId
		}else{
			p.LeaderId = 0
		}
	}
}

/**
判断是否为正常的组队
 */
func (p *TeamData) IsOkTeam() bool {
	if p.Status == TeamStatusDelete {
		return false
	}

	if p.LeaderId == 0 {
		p.AutoChangeLeader()
	}

	if p.LeaderId == 0 || len(p.mapMembers) == 0 {
		p.Status = TeamStatusDelete
		return false
	}

	return true
}

/**
添加组队成员
 */
func (p *TeamData) AddToMember(playerId int,name string,level,career,sexId,maxHp int)  {
	member,ok := p.mapMembers[playerId]
	if !ok {
		member = NewTeamMember(playerId)
		member.Name = name
		member.Level = level
		member.SexId = sexId
		member.MaxHp = maxHp
		member.Career = career

		delete(p.applyMembers,playerId)
		delete(p.inviteMembers,playerId)
	}
	p.mapMembers[playerId] = member
}



















