package model

import "github.com/tianjigames/fairy/constants"

/**
宝物背包
 */
type PlayerTreasureBag struct {
	BodyTreasures map[int]int64
	BagTreaseures []int64
	Treasures map[int64]*CommonTreasureItem
	IsModify bool
}

func NewPlayerTreasureBag() *PlayerTreasureBag {
	return &PlayerTreasureBag{
		BodyTreasures: make(map[int]int64,constants.TreasureBodyNum),
		BagTreaseures: make([]int64,0),
		Treasures: map[int64]*CommonTreasureItem{},
		IsModify: false,
	}
}

/**
向宝物背包中添加一个物品
 */
func (p *PlayerTreasureBag) AddOneItem(item *CommonTreasureItem)  {
	if item == nil || !item.IsValid() {
		return
	}

	p.Treasures[item.GetGuid()] = item
	if item.IsBody() {
		p.BodyTreasures[item.GetTreausrePos()] = item.GetGuid()
	}else{
		p.BagTreaseures = append(p.BagTreaseures,item.GetGuid())
	}

}
