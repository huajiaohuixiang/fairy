package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/template"
)

const (
	_ RefixType = iota - 1
	RefixTypeVTPercent
	RefixTypeVTValue
)

const (
	CharMaxRage = 1000
)

type (


	RefixType int

	CharAttrs struct {
		MAttrs []int
	}

	AttributeList struct {
		MBaseAttrs     *CharAttrs
		MBaseAttrFlag  *BitFlagSet
		MRefixAttrs    *CharAttrs
		MRefixAttrFlag *BitFlagSet
		AttrFactor     *template.AttrFactor
	}
)

func NewAttributeList(host CharObj) *AttributeList {
	return &AttributeList{
		MBaseAttrs: NewCharAttrs(int(constants.AttrMaxIndex)),
		MBaseAttrFlag: NewBitFlagSet(int(constants.AttrMaxIndex)),
		MRefixAttrs: NewCharAttrs(int(constants.AttrMaxIndex)),
		MRefixAttrFlag: NewBitFlagSet(int(constants.AttrMaxIndex)),
	}
}

/**
新建属性对象
 */
func NewCharAttrs(size int) *CharAttrs {
	return &CharAttrs{
		MAttrs:make([]int,size),
	}
}

func GetRefixTypeByValue(value int) *RefixType {
	if value >= int(RefixTypeVTPercent) && value <= int(RefixTypeVTValue) {
		rt := RefixType(value)
		return &rt
	}
	return nil
}

/**
设置属性值
 */
func (p *CharAttrs) SetAttr(nIdx,nValue int)  {
	if nIdx >= 0 && nIdx < len(p.MAttrs) {
		p.MAttrs[nIdx] = nValue
	}
}

/**
获取属性值
 */
func (p *CharAttrs) GetAttr(nIdx int) int {
	if nIdx >= 0 && nIdx < len(p.MAttrs) {
		return p.MAttrs[nIdx]
	}
	return 0
}

/**
获取基本属性值
 */
func (p *AttributeList) GetBaseAttr(nIdx *constants.AttrFlag) int {
	if nIdx == nil {
		return 0
	}

	return p.MBaseAttrs.GetAttr(int(*nIdx))
}

/**
获取基本属性是否需要重新计算标志
 */
func (p *AttributeList) GetBaseAttrFlag(nIdx *constants.AttrFlag) bool {
	if nIdx == nil {
		return false
	}

	return p.MBaseAttrFlag.GetFlagByIndex(int(*nIdx))
}

/**
获取修正属性需要重新计算标志
 */
func (p *AttributeList) GetRefixAttrFlag(nIdx *constants.AttrFlag) bool {
	if nIdx == nil {
		return false
	}
	return p.MRefixAttrFlag.GetFlagByIndex(int(*nIdx))
}

/**
设置修正属性标志值
 */
func (p *AttributeList) SetRefixAttrFlag(nIdx *constants.AttrFlag,bValue bool)  {
	if nIdx == nil {
		return
	}

	if bValue {
		p.MRefixAttrFlag.MarkFlagByIndex(int(*nIdx))
	}else{
		p.MRefixAttrFlag.ClearFlagByIndex(int(*nIdx))
	}
}

/**
设置某个属性值的状态
 */
func (p *AttributeList) SetBaseAttrFlag(attrId *constants.AttrFlag,bValue bool)  {
	if attrId == nil {
		return
	}

	if bValue {
		p.MBaseAttrFlag.MarkFlagByIndex(int(*attrId))
	}else {
		p.MBaseAttrFlag.ClearFlagByIndex(int(*attrId))
	}
}