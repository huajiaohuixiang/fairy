package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/util"
)

type TitleData struct {
	MVersion byte
	TitleList []int //称号列表
	TitleProList []int //获取称号的时间列表
}

/**
实例化称号
 */
func NewTitleData() *TitleData {
	return &TitleData{
		TitleList: make([]int,constants.MaxTitleNum),
		TitleProList: make([]int,constants.MaxTitleNum),
	}
}

/**
反序列化称号数据
 */
func UnSerializeTitleData(data []byte) *TitleData {
	newMData := NewTitleData()
	if data != nil && len(data) > 0 {
		newMData.ReadObject(data)
	}
	return newMData
}

/**
读取数据
 */
func (p *TitleData) ReadObject(in []byte)  {
	nBeginIdx := 0
	p.MVersion = in[nBeginIdx]
	nBeginIdx ++

	if p.MVersion == 0 {
		//解析称号
		nSize := constants.MaxTitleNum*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.TitleList,0)
		nBeginIdx += nSize

		//解析称号获取时间
		nSize = constants.MaxTitleNum*4
		util.ByteArrayToIntArray(in,nBeginIdx,nSize,p.TitleProList,0)
		nBeginIdx += nSize
	}
}