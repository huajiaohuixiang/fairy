package model

type SceneData struct {
	MnSceneSizeX int
	MnSceneSizeY int
	MnServerId int
	MnThreadIndex int
	MnSceneTypeId int
	MnSceneTemplate int
	MnSublinePlayer int
	MnPriority int
	MnStressRatio int
	MnRuleId int
	MSceneName string
}
