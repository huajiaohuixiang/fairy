package model

type SceneTickCount struct {
	MNTickCount int
}

func (p *SceneTickCount) GetTickCount() int {
	return p.MNTickCount
}
