package model

import "github.com/tianjigames/fairy/util"

const (
	BetDataSize0 = 2*8 + 3*4
)

type BetData struct {
	MatchGuid int64
	TeamId int64
	MatchType int
	ItemType int
	ItemNum int
}


/**
反序列化下注数据
 */
func UnSerializeBetData(in []byte) map[int64][]*BetData {
	if in == nil || len(in) <= 0 {
		return nil
	}


	nBeginIdx := 0
	realVersion := in[nBeginIdx]
	nBeginIdx ++
	switch realVersion {
	case 0:
		mp := map[int64][]*BetData{}
		var list []*BetData
		var ok bool

		for ;nBeginIdx <= len(in) - BetDataSize0; {
			retObj := &BetData{}
			retObj.MatchGuid = util.ByteArrayToInt64(in,nBeginIdx)
			nBeginIdx += 8
			retObj.TeamId = util.ByteArrayToInt64(in,nBeginIdx)
			nBeginIdx += 8
			retObj.MatchType = util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			retObj.ItemType = util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			retObj.ItemNum = util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4

			list,ok = mp[retObj.MatchGuid]
			if !ok {
				list = make([]*BetData,0)
			}
			list = append(list,retObj)
			mp[retObj.MatchGuid] = list
		}
		return mp
	}

	return nil
}