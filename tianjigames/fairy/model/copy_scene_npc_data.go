package model

type CopySceneNpcData struct {
	TriggerId string
	BrushPosArr []*Pos
}
