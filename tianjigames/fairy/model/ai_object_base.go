package model

type AiObjectBase struct{
	MPAiStatusLogic *AiStatusLogic
	MPCharself *BaseCharObj
	MPEnemyDataList *AiEnemyDataList
	MPSkillDataList *AiEnemyDataList
	MPEventLogic *AiEventLogic
}
