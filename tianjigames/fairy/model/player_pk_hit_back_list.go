package model

import (
	"github.com/tianjigames/fairy/constants"
)

type (
	PlayerPkHitBackList struct {
		PkHitBackList []*HitBackInfo
		RemoveList []int
	}

	HitBackInfo struct {
		PlayerId int
		TickCount int64
	}
)

/**
反击列表
 */
func NewPlayerHitBackList() *PlayerPkHitBackList {
	return &PlayerPkHitBackList{
		PkHitBackList: make([]*HitBackInfo,constants.PkHitBackCount),
		RemoveList: make([]int,constants.PkHitBackCount),
	}
}