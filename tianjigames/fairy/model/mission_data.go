package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/util"
)

type MissionData struct {
	MNMissionId int //任务配置id
	MYStatus byte //任务状态
	MYFlags byte //任务进行的状态
	MNParam []int //任务参数
	MYParamStr []byte //任务字符串参数
}

/**
实例化一个任务对象
 */
func NewMissionData() *MissionData {
	return &MissionData{
		MNParam: make([]int,constants.MaxMissionDataParamNum),
		MYParamStr: make([]byte,constants.MaxMissionStringParamLen),
	}
}

/**
清理任务对象
 */
func (p *MissionData) ClearUp()  {
	p.MNMissionId = -1
	p.MYStatus = 0
	p.MYFlags = 0
	for i:=0;i<len(p.MNParam);i++ {
		p.MNParam[i] = 0
	}

	for i:= 0;i< len(p.MYParamStr);i++ {
		p.MYParamStr[i] = 0
	}
}

/**
将字节数组数据读出，反序列化对象属性
 */
func (p *MissionData) ReadObject(in []byte,nStart int) int {
	nEnd := nStart
	p.MNMissionId = util.ByteArrayToInt(in,nEnd)
	nEnd += 4
	p.MYFlags = in[nEnd]
	nEnd ++

	for i := 0;i< len(p.MNParam);i++ {
		p.MNParam[i] = util.ByteArrayToInt(in,nEnd)
		nEnd += 4
	}

	for i := 0;i< len(p.MYParamStr);i++ {
		p.MYParamStr[i] = in[nEnd]
		nEnd ++
	}

	return nEnd - nStart
}


























