package model

import "context"

type LoginInfo struct {
	SessionId string
	Ctx context.Context
	QueueIndex int
	Mid string
	ChannelId string
	LoginType int32
	AccountId int64
	NickName string
	ChannelJson string
	DeviceModel string
	AppId string
	WorldId int32
}
