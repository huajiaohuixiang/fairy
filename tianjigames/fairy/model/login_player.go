package model

import (
	"github.com/tianjigames/fairy/pojo"
)

const (
	PlayerLoginNormal = 400
	PlayerLoginStateNeedActive = 401// 账号需要激活，等待输入激活码中
	PlayerLoginWaitACtive = 402 // 激活码已输入，等在验证结果返回中
	PlayerLoginBeginGame = 403 // 已确定进入游戏，等待world返回登陆状态中
	PlayerLoginWaitDisconnect = 404// 被踢掉了，等待客户端断线中，此时不能做任何事
	PlayLoginStateForbid = 405 // 账号被封停
)

type LoginPlayer struct {
	M_nLoginPlayerState int
	M_version string
	M_ActiveCode string
	LoginInfo *LoginInfo
	AccountData *pojo.AccountData
	RoleInfo []*pojo.PlayerData
	M_CreateCareerId int
	M_CreateSexId int
	M_RoleIndex int
	M_WaitRemTime int64
	M_LoginPlayerId int
}

func NewLoginPlayer(loginInfo *LoginInfo) *LoginPlayer {
	return &LoginPlayer{
		M_nLoginPlayerState: PlayerLoginNormal,
		M_version:           "",
		M_ActiveCode:        "",
		LoginInfo:           loginInfo,
		AccountData:         nil,
		RoleInfo:            make([]*pojo.PlayerData,0),
		M_CreateCareerId:    0,
		M_CreateSexId:       0,
		M_RoleIndex:         0,
		M_WaitRemTime:       0,
		M_LoginPlayerId:     0,
	}
}

/**
获取角色数量
 */
func (p *LoginPlayer) GetRoleNum() int {
	if p.RoleInfo != nil {
		return len(p.RoleInfo)
	}

	return 0
}

func (p *LoginPlayer) GetNewRoleIndex() int {
	if p.RoleInfo != nil && len(p.RoleInfo) > 0 {
		for _,v := range p.RoleInfo {
			if v.RoleIndex == p.M_RoleIndex {
				return -1
			}
		}
	}

	return p.M_RoleIndex
}

func (p *LoginPlayer) AddRoleInfo(playData *pojo.PlayerData)  {
	if p.RoleInfo == nil {
		p.RoleInfo = make([]*pojo.PlayerData,0)
	}

	p.RoleInfo = append(p.RoleInfo,playData)
}

func (p *LoginPlayer) GetRoleInfo(playerId int) *pojo.PlayerData {
	if p.RoleInfo == nil || len(p.RoleInfo) == 0 {
		return nil
	}

	for _,v := range p.RoleInfo {
		if v.PlayerId == playerId {
			return v
		}
	}

	return nil
}

func (p *LoginPlayer) GetAccountId() int64 {
	if p.AccountData != nil {
		return p.AccountData.AccountId
	}

	return 0
}
