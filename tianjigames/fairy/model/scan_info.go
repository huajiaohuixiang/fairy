package model

import "github.com/tianjigames/fairy/protos"

type ScanInfo struct {
	MNZoneRadius int
	MRect *Rect
	Type         protos.ObjType
}
