package model

type AiSkillDataList struct {
	MASkillList []*AiSkillData
	MNAttackCount int
}
