package model

type DelayTimer struct {
	MNTickTerm int64
	MNTickSum int64
	MBOper bool
	MBOver bool
}
