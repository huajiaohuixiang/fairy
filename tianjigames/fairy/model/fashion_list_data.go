package model

import (
	"github.com/tianjigames/fairy/util"
)

type (
	FashionData struct {
		Id        int
		GotTime   int
		LimitTime int
	}

	FashionListData struct {
		MVersion byte
		MFashionList []*FashionData
	}
)

/**
实例化时装数据
 */
func NewFashionListData() *FashionListData {
	return &FashionListData{
		MFashionList: make([]*FashionData,0),
	}
}

func NewFashionData(id,time,limitTime int) *FashionData {
	return &FashionData{
		Id: id,
		GotTime: time,
		LimitTime: limitTime,
	}
}


/**
反序列化时装数据
 */
func UnSerializeFashionListData(data []byte) *FashionListData {
	newMData := NewFashionListData()
	if data != nil && len(data) > 0 {
		newMData.ReadObject(data)
	}
	return newMData
}

/**
读取数据
 */
func (p *FashionListData) ReadObject(in []byte)  {
	nBeginIdx := 0
	p.MVersion = in[nBeginIdx]
	nBeginIdx += 1

	count := util.ByteArrayToInt(in,nBeginIdx)
	nBeginIdx += 4

	switch p.MVersion {
	case 0:
		for i:=0;i<count;i++ {
			id := util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			time := util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			persistTime := util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4
			p.MFashionList = append(p.MFashionList,NewFashionData(id,time,persistTime))
		}
	}
}
















