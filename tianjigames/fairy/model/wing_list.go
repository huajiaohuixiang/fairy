package model

import (
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/util"
)

type WingList struct {
	MNWingCount int16
	MNWingLevel int
	MNWingExp int
	MNWingShowId int
	MWingData []*WingData
}

func NewWingList(wingId int,wingData []byte) *WingList {
	rs := &WingList{
		MWingData: make([]*WingData,constants.MaxWingSize),
		MNWingShowId: -1,
	}
	
	if wingData != nil && len(wingData) > 0 {
		rs.UnSerializeDbData(wingData)
	}else{
		for i:=0;i<len(rs.MWingData);i++ {
			rs.MWingData[i] = NewWingData(-1,0)
		}
	}

	return rs
}

/**
反序列化数据
 */
func (p *WingList) UnSerializeDbData(in []byte) {
	nBeginIdx := 0
	nVersion := util.ByteArrayToInt(in,nBeginIdx)
	nBeginIdx += 4

	if nVersion == constants.WingDataVersion1 {
		p.MNWingLevel = util.ByteArrayToInt(in,nBeginIdx)
		nBeginIdx += 4
		p.MNWingExp = util.ByteArrayToInt(in,nBeginIdx)
		nBeginIdx += 4
		p.MNWingCount = int16(util.ByteArrayToInt32(in,nBeginIdx))
		nBeginIdx += 4

		for i :=0;i<len(p.MWingData);i++ {
			templateId := util.ByteArrayToInt(in,nBeginIdx)
			nBeginIdx += 4

			activeTime := util.ByteArrayToInt64(in,nBeginIdx)
			nBeginIdx += 8

			p.MWingData[i] = NewWingData(int(templateId),activeTime)
		}
	}
}
