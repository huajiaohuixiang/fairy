package model

type TickTimeData struct {
	mnAvgTick int
	mnMaxTick int
	mnTickCount int
}
