package model

/**
场景角色管理器
 */
type ObjManager struct {
	MAddObjList map[string]*NpcPlayer //准备加入的npc
	MPlayerList map[string]*PlayerObject
	MNpcList map[string]*NpcPlayer
}

/**
获取角色列表
 */
func (p *ObjManager) GetPlayerList() []*PlayerObject {
	if p.MPlayerList == nil || len(p.MPlayerList) == 0 {
		return make([]*PlayerObject,0)
	}

	rs := make([]*PlayerObject,0)
	for _,v := range p.MPlayerList {
		rs = append(rs,v)
	}
	return rs
}
