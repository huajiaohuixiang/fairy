package model

import "github.com/tianjigames/fairy/template"

type AiEventData struct {
	MPConfig *template.MonsterEvent
	MNTriggerCount int
}
