package model

import (
	protos2 "github.com/tianjigames/fairy/protos"
)

type IBaseObj interface {
	Init()
	GetGuid() int64
	GetId() int
	GetPos() *Pos
	SetPos(pos *Pos)
	Tick(nTime int64)
	GetType() protos2.ObjType
	IsActive() bool
	SetActive(bActive bool)
	GetScene() *Scene
	SetScene(scene *Scene)
	SetGuid(guid int)
	SetId(id int)
}
