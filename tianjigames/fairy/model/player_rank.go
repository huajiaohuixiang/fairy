package model

type PlayerRank struct {
	Rank int
	WinStreak int
	WinCount int
	BattleTotal int
	RewardData []int
}

func NewPlayerRank() *PlayerRank {
	return &PlayerRank{}
}
