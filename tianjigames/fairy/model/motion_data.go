package model

import (
	"github.com/tianjigames/fairy/protos"
)

const MotionIntParamSize = 4

type MotionData struct {
	MIsActive     bool
	MNLogicId     int
	MNLoopTime    int
	MNElapsedTime int64
	MNSectionNum  byte
	MNUnitObjType protos.ObjType
	MNUnitObjId   int
	MAIntParams   []int
	MAPosParam    *Pos
	MType         int
}

func NewMotionData() *MotionData {
	return &MotionData{
		MAIntParams: make([]int,MotionIntParamSize),
	}
}
