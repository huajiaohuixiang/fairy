package model

type ImpactData struct {
	MNIndex int
	MNLogicId int
	MNElapssedTime int
	MNCount int
	MBOnce bool
	IsActiveSkill bool
	MNIntervalTime int
	IsActive bool
}
