package model



type (
	BaseGatherLogic struct {
		TypeId int
		ItemIdIndex []int
	}

	GatherLogic interface {
		GetTypeId() int
		GetItemIdIndex() []int
		GetGatherPos() *Pos
		GetDistance() int
		GetSceneId() int
		GetLevel() int
		GetRound() int
		GetInterval() int
		GetMaxTime() int64
		GetMaxNum() int
		Init()
		OnGo(player *PlayerObject)
		OnCheck(player *PlayerObject)
		OnBegin(player *PlayerObject,itemId int)
		OnSync(player *PlayerObject,itemId int)
		OnInterrupt(player *PlayerObject,itemId int)
		IsOkSync(player *PlayerObject)
		OnDoing(player *PlayerObject)
		OnEnd(player *PlayerObject)
		OnReset(player PlayerObject)
		OnLevelUp(player *PlayerObject,newLv int)
	}
)

func (p *BaseGatherLogic) GetTypeId() int {
	return p.TypeId
}

func (p *BaseGatherLogic) GetItemIdIndex() []int {
	return p.ItemIdIndex
}

func (p *BaseGatherLogic) GetGatherPos() *Pos {
	return nil
}

func (p *BaseGatherLogic) GetDistance() int {
	return 0
}

func (p *BaseGatherLogic) GetSceneId() int {
	return 0
}
func (p *BaseGatherLogic) GetLevel() int {
	return 0
}
func (p *BaseGatherLogic) GetRound() int {
	return 0
}

func (p *BaseGatherLogic) GetInterval() int {
	return 0
}
func (p *BaseGatherLogic) GetMaxTime() int64 {
	return 0
}
func (p *BaseGatherLogic) GetMaxNum() int {
	return 0
}
func (p *BaseGatherLogic) Init(){

}
func (p *BaseGatherLogic) OnGo(player *PlayerObject){

}

func (p *BaseGatherLogic) OnCheck(player *PlayerObject){
	//todo:处理check逻辑
}

func (p *BaseGatherLogic) OnBegin(player *PlayerObject,itemId int){

}

func (p *BaseGatherLogic) OnSync(player *PlayerObject,itemId int) {

}
func (p *BaseGatherLogic) OnInterrupt(player *PlayerObject,itemId int){

}
func (p *BaseGatherLogic) IsOkSync(player *PlayerObject) {

}
func (p *BaseGatherLogic) OnDoing(player *PlayerObject) {

}
func (p *BaseGatherLogic) OnEnd(player *PlayerObject) {

}
func (p *BaseGatherLogic) OnReset(player PlayerObject) {

}
func (p *BaseGatherLogic) OnLevelUp(player *PlayerObject,newLv int){

}
