package model

type (
	BaseGatherInfo struct {
		Logic     GatherLogic
		Player    *PlayerObject
		Doing     bool
		BeginTime int64
		Change    bool
		MaxNum    int
		MaxTime   int64
		Temp      int64
	}

	GatherInfo interface {
		GetIdIndex() byte
		SetIdByIndex(index byte)
		GetDoingPhase() byte
		SetDoingPhase(doingPhase byte)
		GetDoingNum() int
		SetDoingNum(doingNum int)
		GetDongId() int
		SetDongId()
		GetPlayer() *PlayerObject
		SetPlayer(player *PlayerObject)
		IsDoing() bool
		SetDoing(isDoing bool)
		GetBeginTime() int64
		SetBeginTime(beginTime int64)
		IsChange() bool
		SetChange(isChange bool)
		GetMaxNum() int
		SetMaxNum(maxNum int)
		GetMaxTime() int64
		SetMaxTime(maxTime int64)
		GetTemp() int64
		SetTemp(temp int64)
	}
)

func (p *BaseGatherInfo) GetIdIndex() byte {
	return 0
}
func (p *BaseGatherInfo) SetIdByIndex(index byte){

}
func (p *BaseGatherInfo) GetDoingPhase() byte {
	return 0
}

func (p *BaseGatherInfo) SetDoingPhase(doingPhase byte){

}
func (p *BaseGatherInfo) GetDoingNum() int {
	return 0
}
func (p *BaseGatherInfo) SetDoingNum(doingNum int) {

}
func (p *BaseGatherInfo) GetDongId() int {
	return 0
}
func (p *BaseGatherInfo) SetDongId() {

}

func (p *BaseGatherInfo) GetPlayer() *PlayerObject {
	return p.Player
}

func (p *BaseGatherInfo) SetPlayer(player *PlayerObject) {
	p.Player = player
}
func (p *BaseGatherInfo) IsDoing() bool {
	return p.Doing
}

func (p *BaseGatherInfo) SetDoing(isDoing bool) {

}

func (p *BaseGatherInfo) GetBeginTime() int64 {
	return p.BeginTime
}
func (p *BaseGatherInfo) SetBeginTime(beginTime int64) {
	p.BeginTime = beginTime
}
func (p *BaseGatherInfo) IsChange() bool {
	return p.Change
}
func (p *BaseGatherInfo) SetChange(isChange bool){
	p.Change = isChange
}

func (p *BaseGatherInfo) GetMaxNum() int {
	return p.MaxNum
}

func (p *BaseGatherInfo) SetMaxNum(maxNum int) {
	p.MaxNum = maxNum
}

func (p *BaseGatherInfo) GetMaxTime() int64 {
	return p.MaxTime
}
func (p *BaseGatherInfo) SetMaxTime(maxTime int64) {
	p.MaxTime = maxTime
}
func (p *BaseGatherInfo) GetTemp() int64 {
	return p.Temp
}
func (p *BaseGatherInfo) SetTemp(temp int64) {
	p.Temp = temp
}

