package model

import "github.com/tianjigames/fairy/constants"

type Pos struct {
	x int
	y int
	z int
}

func NewPos(x,y,z int) *Pos {
	return &Pos{
		x:x,
		y:y,
		z:z,
	}
}

func (p *Pos) GetX() float32 {
	return float32(p.x)/ constants.PlayerPosBase
}

func (p *Pos) GetY() float32 {
	return float32(p.y)/ constants.PlayerPosBase
}

func (p *Pos) GetZ() float32 {
	return float32(p.z)/ constants.PlayerPosBase
}

func (p *Pos) SetX(x int)  {
	p.x = x
}

func (p *Pos) SetY(y int)  {
	p.y = y
}

func (p *Pos) SetZ(z int)  {
	p.z = z
}


