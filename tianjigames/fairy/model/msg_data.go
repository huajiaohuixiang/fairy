package model

type MsgData struct {
	Route string
	Stamp uint
	Msg interface{}
}

/**
实例化一个发送消息体
 */
func NewMsgData(route string,msg interface{}) *MsgData {
	return &MsgData{
		Route: route,
		Msg: msg,
	}
}
