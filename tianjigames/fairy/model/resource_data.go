package model

type ResourceData struct {
	Gold int
	BuildStuff int
	HpRatio float32
}
