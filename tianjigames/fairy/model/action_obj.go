package model

import (
	protos2 "github.com/tianjigames/fairy/protos"
)

type ActionObj struct {
	MMontionData *MotionData
}

func (p *ActionObj) Init(){

}

func (p *ActionObj)GetGuid() int64 {
	return 0
}

func (p *ActionObj)GetId() int{
	return 0
}

func (p *ActionObj)GetPos() *Pos {
	return nil
}

func (p *ActionObj) SetPos(pos *Pos){

}
func (p *ActionObj) Tick(nTime int64) {

}
func (p *ActionObj) GetType() protos2.ObjType {
	return protos2.ObjType_ObtNone
}

func (p *ActionObj) IsActive() bool{
	return false
}
func (p *ActionObj) SetActive(bActive bool){

}
func (p *ActionObj) GetScene() *Scene {
	return nil
}
func (p *ActionObj) SetScene(scene *Scene){

}

func (p *ActionObj) SetGuid(guid int){

}
func (p *ActionObj) SetId(id int){

}

