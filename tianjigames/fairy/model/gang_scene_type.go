package model

type GangSceneType int

const (
	Invalid       GangSceneType = -1
	Manor         GangSceneType = 1
	PreFight      GangSceneType = 2
	RobFight      GangSceneType = 3
	ContendFight  GangSceneType = 4
	ManorActivity GangSceneType = 5
)
