package hall

import (
	"context"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/manager"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/tianjigames/fairy/util"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/component"
	constants2 "github.com/topfreegames/pitaya/constants"
	"github.com/topfreegames/pitaya/logger"
)

type RoleHandler struct {
	component.Base
}

func NewRoleHandler() *RoleHandler {
	return &RoleHandler{}
}

/**
创建角色
 */
func (r *RoleHandler) Create(ctx context.Context,request *protos2.CLCreateRole) (*protos2.LCCreateRoleRet,error) {
	s := pitaya.GetSessionFromCtx(ctx)
	if len(s.UID()) == 0{
		return &protos2.LCCreateRoleRet{RetCode: constants.LoginUsernameError},nil
	}

	accountId := s.Int64(constants.UIdKey)
	player := manager.LoginManager.GetLoginPlayer(s.UID())

	if player == nil {
		logger.Log.Errorf("RoleHandler Create fail,account=%d not login",accountId)
		return &protos2.LCCreateRoleRet{RetCode: constants.LoginUsernameError},nil
	}

	if player.AccountData == nil {
		logger.Log.Infof("RoleHandler Create fail,account=%d,AccountData not exist",accountId)
		return &protos2.LCCreateRoleRet{RetCode: constants.RoleNotRoundError},nil
	}

	if player.GetRoleNum() > constants.MaxRoleNum {
		logger.Log.Infof("RoleHandler Create fail,account=%d,player RoleHandler num too many",accountId)
		return &protos2.LCCreateRoleRet{RetCode: constants.RoleCreateFailTooMany},nil
	}

	if request.RoleIndex < 0 || request.RoleIndex > constants.MaxRoleNum {
		return &protos2.LCCreateRoleRet{RetCode: constants.RoleCreateFailIndex},nil
	}

	if len(request.RoleName) < 0 || len(request.RoleName) > 18 {
		return &protos2.LCCreateRoleRet{RetCode: constants.NicknameLengthError},nil
	}

	b ,err := util.StringFilter.CheckString(request.RoleName)
	if err != nil {
		logger.Log.Errorf("RoleHandler Create failed,account=%d,error:%s",accountId,err.Error())
		return nil,err
	}

	if b {
		return &protos2.LCCreateRoleRet{RetCode: constants.NicknameContentError},nil
	}

	player.M_RoleIndex = int(request.RoleIndex)
	if player.GetNewRoleIndex() <= 0 {
		player.M_RoleIndex = 0
		return &protos2.LCCreateRoleRet{RetCode: constants.RoleCreateFailIndex},nil
	}

	player.M_CreateCareerId = int(request.CareerId)
	player.M_CreateSexId = int(request.SexId)

	reply := &protos2.WLCheckPlayerNameRet{}
	err = pitaya.RPC(ctx, constants.CheckPlayerNameRoute,reply,&protos2.LWCheckPlayerName{
		Name:      request.RoleName,
	})

	if err != nil {
		logger.Log.Errorf("RoleHandler create failed,error:%s",err.Error())
		return nil,err
	}

	existName := reply.IsExist == 1
	rs,err1 :=  manager.LoginManager.CheckPlayerNameResoult(reply.Name,ctx,existName)
	if err1 != nil {
		logger.Log.Errorf("RoleHandler create failed,error:%s",err1.Error())
		return nil,err1
	}

	if rs {
		deleteRoleNameReq := &protos2.GWDeleteTmpPlayerName{Name: reply.Name}
		reply := &protos2.RpcResponse{}
		err = pitaya.RPC(ctx, constants.GwDeleteTmpPlayerNameRoute,reply,deleteRoleNameReq)
		if err != nil {
			logger.Log.Errorf("RoleHandler create failed,error:%s",err.Error())
			return nil,err
		}
	}

	return nil,constants2.ErrReturnNil
}
