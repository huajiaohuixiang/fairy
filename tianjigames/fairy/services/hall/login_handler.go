package hall

import (
	"context"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/manager"
	"github.com/tianjigames/fairy/model"
	"github.com/tianjigames/fairy/pojo"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/component"
	constants2 "github.com/topfreegames/pitaya/constants"
	"github.com/topfreegames/pitaya/logger"
	"strconv"
)

type (
	LoginHandler struct {
		component.Base
	}

	LoginResponse struct {
		status string
	}
)


func NewLoginHandler() *LoginHandler {
	return &LoginHandler{}
}


func (p *LoginHandler) Login(ctx context.Context,reqLogin *protos2.CLLogin) (*protos2.LCLoginRet,error) {
	if reqLogin.SwitchRole == 0 {
		err := p.doStartLogin(ctx,reqLogin)
		if err != nil {
			logger.Log.Errorf("[LoginHandler] login failed,error=%s",err.Error())
		}
	}else {
		err := p.doQuickStartLogin(ctx,reqLogin)
		if err != nil {
			logger.Log.Errorf("[LoginHandler] login failed,error=%s",err.Error())
		}
	}

	sessionId := pitaya.GetSessionFromCtx(ctx).UID()
	//玩家的登录消息添加到了登录队列 说明玩家正排队登录 需要通知worldServer 玩家所在的loginServer
	err := pitaya.RPC(ctx, constants.LPPlayerOnLoginServerRoute,&protos2.PLSessionOnLoginServerRet{},&protos2.LPSessionOnLoginServer{
		SessionId: sessionId,
		ServerId: pitaya.GetServer().ID,
	})
	if err != nil {
		logger.Log.Errorf("[LoginHandler] login failed,error=%s",err.Error())
	}


	return nil,constants2.ErrReturnNil
}

func (p *LoginHandler) doStartLogin(ctx context.Context,reqLogin *protos2.CLLogin) error {
	s := pitaya.GetSessionFromCtx(ctx)


	info := &model.LoginInfo{
		SessionId:s.UID(),
		Ctx:ctx,
		Mid:reqLogin.Mid,
		ChannelId: strconv.FormatInt(int64(reqLogin.ChannelId),10),
		LoginType:reqLogin.LoginType,
		AccountId:reqLogin.AccountId,
		ChannelJson:reqLogin.ChannelJson,
		DeviceModel:reqLogin.DeviceId,
		AppId:reqLogin.AppId,
		WorldId:reqLogin.WorldId,
	}

	err := manager.LoginManager.StartDoLogin(info)
	if err != nil {
		logger.Log.Errorf("[LoginHandler] doStartLogin failed,error=%s",err.Error())
		return err
	}

	return nil
}

func (p *LoginHandler) doQuickStartLogin(ctx context.Context,reqLogin *protos2.CLLogin) error {
	s := pitaya.GetSessionFromCtx(ctx)



	info := &model.LoginInfo{
		SessionId:s.UID(),
		Ctx:ctx,
		Mid:reqLogin.Mid,
		ChannelId: strconv.FormatInt(int64(reqLogin.ChannelId),10),
		LoginType:reqLogin.LoginType,
		AccountId:reqLogin.AccountId,
		ChannelJson:reqLogin.ChannelJson,
		DeviceModel:reqLogin.DeviceId,
		AppId:reqLogin.AppId,
		WorldId:reqLogin.WorldId,
	}


	bLoginSuc := false
	var accountData *pojo.AccountData
	var err error


	if info.AccountId != 0 {
		bLoginSuc = true
		accountData,err = manager.PlayerManager.GetAccountDataByAccountId(info.AccountId,info.AppId)
		if err != nil {
			logger.Log.Errorf("[LoginHandler] doQuickStartLogin failed,err:%s",err.Error())
			return err
		}

		if accountData == nil {
			bLoginSuc = false
		}
	}

	err = manager.LoginManager.DoLoginResult(ctx,bLoginSuc,info,accountData,false,true)
	if err != nil {
		logger.Log.Errorf("[LoginHandler] doQuickStartLogin failed,err:%s",err.Error())
		return err
	}

	return nil
}
