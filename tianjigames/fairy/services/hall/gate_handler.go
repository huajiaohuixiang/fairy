package hall

import (
	"context"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/manager"
	"github.com/tianjigames/fairy/model"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/component"
)

type GateHandler struct {
	component.Base
}

func NewGateHandler() *GateHandler {
	return &GateHandler{}
}

/**
处理客户端连接关闭
*/
func (p *GateHandler) OnSessionClosed(ctx context.Context,req *protos2.GatePOnSessionClosed) (*protos2.GatePOnSessionClosedRet,error) {
	//处理session关闭相关逻辑

	if req.SessionIds != nil && len(req.SessionIds) > 0 {
		loginThread := pitaya.ThreadPool.GetThread(constants.LoginThread).(*manager.LoginThread)
		loginThread.RemoveLoginMap(req.SessionIds...)

		for _,sessionId := range req.SessionIds {
			//玩家可能已经登录到登录服，需要移除loginPlayer
			loginPlayer := manager.LoginManager.GetLoginPlayer(sessionId)
			if loginPlayer != nil {
				loginPlayer.M_nLoginPlayerState = model.PlayerLoginWaitDisconnect
			}
		}

	}

	return &protos2.GatePOnSessionClosedRet{},nil
}