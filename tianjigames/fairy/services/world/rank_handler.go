package world

import (
	"context"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/topfreegames/pitaya/component"
)

type RankHandler struct {
	component.Base
}

func NewRankHandler() *RankHandler {
	return &RankHandler{}
}

func (p *RankHandler) AddOrUpdateStandData(ctx context.Context,req *protos2.GWAddOrUpdateStandData) (*protos2.WGAddOrUpdateStandDataRet,error) {
	return nil,nil
}
