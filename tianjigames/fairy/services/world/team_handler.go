package world

import (
	"context"
	"github.com/tianjigames/fairy/manager"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/topfreegames/pitaya/component"
	constants2 "github.com/topfreegames/pitaya/constants"
	"github.com/topfreegames/pitaya/logger"
)

type TeamHandler struct {
	component.Base
}

func NewTeamHandler() *TeamHandler {
	return &TeamHandler{}
}

/**
创建组队
 */
func (p *TeamHandler) Create(ctx context.Context,req *protos2.GWCreateTeam) (*protos2.WGCreateTeamRet,error) {
	err := manager.TeamManager.CreateTeam(req)
	if err != nil {
		logger.Log.Errorf("[TeamHandler] Create failed,error:%s",err.Error())
		return nil,err
	}

	return nil,constants2.ErrReturnNil
}
