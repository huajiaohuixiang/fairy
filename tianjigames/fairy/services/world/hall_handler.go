package world

import (
	"context"
	"github.com/google/uuid"
	"github.com/tianjigames/fairy/constants"
	"github.com/tianjigames/fairy/manager"
	"github.com/tianjigames/fairy/model"
	protos2 "github.com/tianjigames/fairy/protos"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/cluster"
	"github.com/topfreegames/pitaya/component"
	constants2 "github.com/topfreegames/pitaya/constants"
	"github.com/topfreegames/pitaya/logger"
	"math/rand"
	"time"
)

type HallHandler struct {
	component.Base
}

func NewHallHandler() *HallHandler {
	return &HallHandler{}
}

func (r *HallHandler) CheckPlayerName(ctx context.Context,req *protos2.LWCheckPlayerName) (*protos2.WLCheckPlayerNameRet,error) {
	b := manager.PlayerNameManager.CheckForAddNewName(req.Name)
	exist := int32(0)
	if b {
		exist = 1
	}
	return &protos2.WLCheckPlayerNameRet{
		Name:      req.Name,
		IsExist:   exist,
	},nil

}

func (r *HallHandler) AddNewPlayer(ctx context.Context,req *protos2.LWAddNewPlayer) (*protos2.RpcResponse,error) {
	manager.PlayerNameManager.UpdateNewRoleName(req.Name,req.AccountId,req.PlayerGuId,int(req.Sexid),req.AppId)
	return &protos2.RpcResponse{RetCode: constants.SuccessRet},nil
}

func (r *HallHandler) CheckLoginNum(ctx context.Context,req *protos2.LWCheckLoginNum) (*protos2.WLCheckLoginNumRet,error)  {
	maxPlayerNum := pitaya.GetConfig().GetInt(constants2.WorldServerMaxpoolPlayerSize)
	//获取在游戏服务器上的人数
	reply := &protos2.PWOnGameServerPlayerNumRet{}
	err := pitaya.RPC(ctx,constants.WPOnGameServerPlayerNumRoute,reply,&protos2.WPOnGameServerPlayerNum{})
	if err != nil {
		logger.Log.Errorf("[HallHandler] CheckLoginNum failed,error:%s",err.Error())
		return nil,err
	}

	return &protos2.WLCheckLoginNumRet{
		PlayerId: req.PlayerId,
		Num:      int32(maxPlayerNum - int(reply.Num)),
	},nil
}

func (r *HallHandler) GetGSInfo(ctx context.Context,req *protos2.LWGetGSInfo) (*protos2.WLGetGSInfoRet,error) {
	playerData := req.PlayerData
	logger.Log.Infof("[GetGSInfo] Begin accountId=%d,playerId=%d",playerData.AccountId,playerData.PlayerId)

	//随机选择一台网关 让客户端登录
	servers,err := pitaya.GetServersByType(constants.SvTypeGame)
	if err != nil {
		logger.Log.Errorf("GetGSInfo failed,error=%s",err.Error())
		return nil,err
	}
	srvList := make([]*cluster.Server, 0)
	s := rand.NewSource(time.Now().Unix())
	rnd := rand.New(s)
	for _, v := range servers {
		srvList = append(srvList, v)
	}
	server := srvList[rnd.Intn(len(srvList))]
	ticket := uuid.New().String()
	playerInfo := manager.GameServerPlayerManager.GetPlayerInfoByAccount(playerData.AccountId,playerData.AppId)
	if playerInfo == nil {
		playerInfo = &model.PlayerInfo{
			FullData:   playerData,
			Ticket:     ticket,
			ServerId:   server.ID,
		}
		manager.GameServerPlayerManager.AddPlayerInfo(playerData.AccountId,playerData.AppId,playerInfo)
	}

	gsInfoRet := &protos2.WLGetGSInfoRet{
		Ticket:     ticket,
		ServerInfo:&protos2.MsgServerInfo{
			Id:    int32(1),
			Ip:   server.ID,
		},
	}
	return gsInfoRet,nil
}

