module github.com/tianjigames/fairy

go 1.14

require (
	cloud.google.com/go v0.81.0
	github.com/Chronokeeper/anyxml v0.0.0-20160530174208-54457d8e98c6 // indirect
	github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53 // indirect
	github.com/CloudyKit/jet v2.1.2+incompatible // indirect
	github.com/agrison/go-tablib v0.0.0-20160310143025-4930582c22ee // indirect
	github.com/agrison/mxj v0.0.0-20160310142625-1269f8afb3b4 // indirect
	github.com/bndr/gotabulate v1.1.2 // indirect
	github.com/clbanning/mxj v1.8.4 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gogo/protobuf v1.3.2
	github.com/google/uuid v1.2.0
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/spf13/viper v1.8.1
	github.com/streadway/handy v0.0.0-20200128134331-0f66f006fb2e
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tealeg/xlsx v1.0.5 // indirect
	github.com/topfreegames/pitaya v1.1.8
	github.com/urfave/cli v1.22.1
	github.com/xormplus/builder v0.0.0-20200331055651-240ff40009be // indirect
	github.com/xormplus/xorm v0.0.0-20210512135344-8123d584d5f5
	golang.org/x/text v0.3.5
	google.golang.org/grpc v1.38.0 // indirect    将这个版本降到v1.26.0
	gopkg.in/flosch/pongo2.v3 v3.0.0-20141028000813-5e81b817a0c4 // indirect
)

replace (
	github.com/topfreegames/pitaya => E:\learn\go_project\src\github.com\topfreegames\pitaya
	google.golang.org/grpc v1.38.0 => google.golang.org/grpc v1.26.0
)
