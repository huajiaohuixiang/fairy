package template

import "github.com/topfreegames/pitaya"

var EquipStarKey = &EquipStar{}

type EquipStar struct {
	pitaya.BaseTemplate
	Title string
	Id string
	StarLevel int
	SuccessRate int
	Fall2StarLevel int
	FallRate int
	MoneyCondition []int
	ItemCondition []int
	AttrIncRate1 int
	AttrIncRate2 int
	AttrIncRate3 int
	AttrIncRate4 int
	AttrIncRate5 int
	AttrIncRate6 int
	WinProb int
	MinimumLevel []int
}

func (p *EquipStar) NewTemplate() interface{} {
	return &EquipStar{
		MoneyCondition: make([]int,2),
		ItemCondition: make([]int,3),
		MinimumLevel: make([]int,2),
	}
}
