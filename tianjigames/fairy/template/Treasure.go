package template

import (
	"github.com/topfreegames/pitaya"
)

var TreasureKey = &Treasure{}

type Treasure struct {
	pitaya.BaseTemplate
	Title string
	Id string
	TreasureName string
	TreasureLevel int
	TreasureNextLevel int
	TreasureQuality int
	TreasureInfo string
	TreasureIcon string
	TreasureType int
	TreasureMutex []int
	TreasureExp int
	TreasureExpLimit int
	CanEquip int
	TreasureProperty1 []int
	TreasureProperty2 []int
	TreasureProperty3 []int
	TreasureProperty4 []int
	DecomposeCount int
}

func (p *Treasure) NewTemplate() interface{} {
	return &Treasure{
		TreasureMutex: make([]int,10),
		TreasureProperty1: make([]int,3),
		TreasureProperty2: make([]int,3),
		TreasureProperty3: make([]int,3),
		TreasureProperty4: make([]int,3),
	}
}
