package template

import "github.com/topfreegames/pitaya"

var ThreeYbDataKey = &ThreeYbData{}

type ThreeYbData struct {
	pitaya.BaseTemplate
	Title  string
	Id     string
	Ingot1 int
	Ingot2 int
}

func (p *ThreeYbData) NewTemplate() interface{} {
	return &ThreeYbData{}
}

