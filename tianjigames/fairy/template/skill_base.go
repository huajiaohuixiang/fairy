package template

import "github.com/topfreegames/pitaya"

var SkillBaseKey = &SkillBase{}

func (p *SkillBase) NewTemplate() interface{} {
	return &SkillBase{
		SkillLevel: make([]int,2),
		Buff1: make([]int,3),
		Buff2: make([]int,3),
		Buff3: make([]int,3),
		Buff4: make([]int,3),
		Buff5: make([]int,3),
		Effect1: make([]int,5),
		Effect2: make([]int,5),
		Effect3: make([]int,5),
		Effect4: make([]int,5),
		Effect5: make([]int,5),
		Effect6: make([]int,5),
		Effect7: make([]int,5),
		Effect8: make([]int,5),
		Effect9: make([]int,5),
		Effect10: make([]int,5),
		Effect11: make([]int,5),
		Effect12: make([]int,5),
		Effect13: make([]int,5),
		Effect14: make([]int,5),
		Effect15: make([]int,5),
		Effect16: make([]int,5),
		Effect17: make([]int,5),
		Effect18: make([]int,5),
		Effect19: make([]int,5),
		Effect20: make([]int,5),
	}
}

type SkillBase struct {
	pitaya.BaseTemplate
	Title string
	Id string
	MotionId int
	LogicId int
	Fontname string
	SkillName string
	DetailInfo string
	Index int
	Description string
	AniID int
	CareerId int
	SkillType int
	SkillSubType int
	SkillClassify int
	IsDragAble int
	Icon string
	Target int
	CastDistance int
	MPcost int
	RageCost int
	CD int
	CombatPower int
	Level int
	SkillLevel []int
	PreSkill int
	AftSkill int
	CostMoney int
	MoneyCount int
	CostSpirit int
	SpiritCount int
	CostItem int
	ItemCount int
	ItemPrice int
	Buff1 []int
	Buff2 []int
	Buff3 []int
	Buff4 []int
	Buff5 []int
	Effect1 []int
	Effect2 []int
	Effect3 []int
	Effect4 []int
	Effect5 []int
	Effect6 []int
	Effect7 []int
	Effect8 []int
	Effect9 []int
	Effect10 []int
	Effect11 []int
	Effect12 []int
	Effect13 []int
	Effect14 []int
	Effect15 []int
	Effect16 []int
	Effect17 []int
	Effect18 []int
	Effect19 []int
	Effect20 []int
	PileId int
	RageInc int
}
