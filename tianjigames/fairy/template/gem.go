package template

import "github.com/topfreegames/pitaya"

var GemKey = &Gem{}

type Gem struct {
	pitaya.BaseTemplate
	Title          string
	Id             string
	GemName        string
	GemType        int
	GemLevel       int
	GemPara1       []int
	GemPara2       []int
	GemPrice       int
	GemLevelUpCost []int
	GemUrl         int
	NextLevelGem   int
}

func (p *Gem) NewTemplate() interface{} {
	return &Gem{
		GemPara1:       make([]int,2),
		GemPara2:       make([]int,2),
		GemLevelUpCost: make([]int,2),
	}
}
