package template

import "github.com/topfreegames/pitaya"

var FabaoBaseKey = &FabaoBase{}

func (p *FabaoBase) NewTemplate() interface{} {
	return &FabaoBase{
		ItemCondition1: make([]int,3),
		ItemCondition2: make([]int,3),
		Attr1: make([]int,2),
		Attr2_0: make([]int,2),
		Attr2_1: make([]int,2),
		Attr3: make([]int,2),
		Attr4: make([]int,2),
		Attr5: make([]int,2),
		Attr6: make([]int,2),
		Attr7: make([]int,2),
		Attr8: make([]int,2),
	}
}

type FabaoBase struct {
	pitaya.BaseTemplate
	Title string
	Id string
	FabaoName string
	FabaoQuality int
	FabaoLevel int
	FabaoSubLvl int
	NextFabaoId int
	OpenMission int
	FabaoDescribe string
	TexturePath int
	IconName string
	ModelId int
	EffectId int
	NeedExp int
	ItemCondition1 []int
	ItemCondition2 []int
	AddExp int
	DoubleRate int
	Attr1 []int
	Attr2_0 []int
	Attr2_1 []int
	Attr3 []int
	Attr4 []int
	Attr5 []int
	Attr6 []int
	Attr7 []int
	Attr8 []int
}
