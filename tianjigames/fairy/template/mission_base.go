package template

import "github.com/topfreegames/pitaya"

var MissionBaseKey = &MissionBase{}

type MissionBase struct {
	pitaya.BaseTemplate
	Title string
	Id string
	Index int
	TaskName string
	Chapter int
	TaskReceiveDesc string
	TaskDesc string
	TaskDetail string
	TackingTips string
	TaskClassify int
	DailyTaskType int
	TaskType int
	LvLimit int
	StageLimit []int
	SortIndex int
	BeforeTaskID int
	AfterTaskID int
	WhetherRepeat int
	AutomaticReceive int
	ItemLimit []int
	ReceiveNPC int
	CompleteNPC int
	TaskTarget int
	TargetNum int
	TaskSecTarget []int
	TaskThirdTarget []int
	TaskFourTarget []int
	DialogTime []int
	DialogId []int
	BuffTime []int
	BuffId []int
	RewardRatio int
	RewardType1 []int
	RewardCount1 []int
	RewardType2 []int
	RewardCount2 []int
	RewardType3 []int
	RewardCount3 []int
	RewardType4 []int
	RewardCount4 []int
	RewardType5 []int
	RewardCount5 []int
	MultipleCyc string
	MultipleTime string
	TaskItemId []int
}

func (p *MissionBase) NewTemplate() interface{} {
	return &MissionBase{
		StageLimit: make([]int,2),
		ItemLimit: make([]int,2),
		TaskSecTarget: make([]int,3),
		TaskThirdTarget: make([]int,3),
		TaskFourTarget: make([]int,3),
		DialogTime: make([]int,3),
		DialogId: make([]int,3),
		BuffTime: make([]int,3),
		BuffId: make([]int,3),
		RewardType1: make([]int,4),
		RewardCount1: make([]int,4),
		RewardType2: make([]int,4),
		RewardCount2: make([]int,4),
		RewardType3: make([]int,4),
		RewardCount3: make([]int,4),
		RewardType4: make([]int,4),
		RewardCount4: make([]int,4),
		RewardType5: make([]int,4),
		RewardCount5: make([]int,4),
		TaskItemId: make([]int,10),
	}
}
