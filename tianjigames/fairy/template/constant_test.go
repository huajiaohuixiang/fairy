package template

import (
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/constants"
	"github.com/topfreegames/pitaya"
	"testing"
)

func TestConstant_NewTemplate(t *testing.T) {
	base.BaseInit()
	pitaya.RegisterTemplate(ConstantKey)
	pitaya.LoadAllTemplateWithPath("./txt")

	v := pitaya.GetTemplateById(ConstantKey,constants.Strength10VipLimitTip)
	assert.NotNil(t, v)
	rs,ok := v.(*Constant)
	assert.True(t, ok)
	assert.NotNil(t, rs)
	assert.EqualValues(t, constants.Strength10VipLimitTip,rs.Id)
}
