package template

import "github.com/topfreegames/pitaya"

var AttrFactorKey = &AttrFactor{}

func (p *AttrFactor) NewTemplate() interface{} {
	return &AttrFactor{}
}

type AttrFactor struct {
	pitaya.BaseTemplate
	Title string
	Id string
	ProId int
	Describe string
	Level int
	Experience int
	AttType int
	Constitution int
	ConToHp int
	ConToMana int
	StrengPower int
	StrToPattack int
	StrToPdefense int
	Spirit int
	SpiToMattack int
	SpiToMdefense int
	Agility int
	AgiToDodge int
	AgiToHit int
	Health int
	Mana int
	Pattack int
	Mattack int
	Pdefense int
	Mdefense int
	Hit int
	Miss int
	Critical int
	CritResist int
	CritIncreased int
	CritReduction int
	Through int
	Patty int
	CriticalHit int
	StrengthDefuse int
	DamageIncrease int
	DamageReduce int
	ExtraHit int
	ExtraMiss int
	ExtraCritical int
	ExtraCritResist int
	ExtraThrough int
	ExtraPatty int
	AttackIce int
	ResistIce int
	ReduceResistIce int
	AttackFire int	//attackFire
	ResistFire int	//resistFire
	ReduceResistFire int	//reduceResistFire
	AttackThunder int
	ResistThunder int
	ReduceResistThunder int
	AttackPoison int
	ResistPosion int
	ReduceResistPosion int
	AttackSpeed int
	AutoRecoveryAnger int
	AutoRecoveryHealth int
	AutoRecoveryMana int
}


