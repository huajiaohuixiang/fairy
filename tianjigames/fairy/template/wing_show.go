package template

import "github.com/topfreegames/pitaya"

var WingShowKey = &WingShow{}

type WingShow struct {
	pitaya.BaseTemplate
	Title string
	Id string
	WingName string
	ModelId int
	EffectPoint []string
	EffectId []int
	ActiveCondition int
	ActiveValue int
	ActiveTime int
	Attr1 []int
	Attr2 []int
	ObtainHint string
	IsShow int
}

func (p *WingShow) NewTemplate() interface{} {
	return &WingShow{
		EffectPoint: make([]string,4),
		EffectId: make([]int,4),
		Attr1: make([]int,2),
		Attr2: make([]int,2),
	}
}