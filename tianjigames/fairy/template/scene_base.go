package template

import "github.com/topfreegames/pitaya"

var SceneBaseKey = &SceneBase{}

func (p *SceneBase) NewTemplate() interface{} {
	return &SceneBase{
		PkModeGroup: make([]int,6),
		Size: make([]int,2),
		ResPoint: make([]int,3),
		RaderStartPos: make([]int,2),
		RadarSize: make([]int,2),
	}
}

type SceneBase struct {
	pitaya.BaseTemplate
	Title string
	Id string
	MapName string
	ServerId int
	ThreadId int
	SublineNum int
	SublinePlayer int
	Priority int
	StressRatio int
	RuleId int
	PkModeGroup []int
	Size []int
	SceneCount int
	SceneId string
	ExistType int
	CreateMode int
	ZoneSize int
	NpcSize int
	SceneMapData string
	SenceMusic string
	SceneMusicResID int
	ResPoint []int
	RadarImage string
	RadarImageResID int
	RaderStartPos []int
	RadarSize []int
	IfMap int
	CanTransfer int
	CampId int
	TransferCost int
	CanRide int
	CanKillPoint int
	AddEnemy int
	ReliveType int
	ReliveBuffId int
	CanEatDrug int
	CanTeamCall int
	CanMusic int
	Caustic string
	Perturbate int
	Ext1 string
	Ext2 string
}
