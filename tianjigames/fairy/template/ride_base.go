package template

import "github.com/topfreegames/pitaya"

var RideBaseKey = &RideBase{}

func (p *RideBase) NewTemplate() interface{} {
	return &RideBase{
		ModelId: make([]int,2),
		EffectPos: make([]string,4),
		EffectId: make([]int,4),
		SpecialPhyAttr1: make([]int,2),
		SpecialPhyAttr2: make([]int,2),
		SpecialMagicAttr1:make([]int,2),
		SpecialMagicAttr2:make([]int,2),
		CommonAttr1: make([]int,2),
		CommonAttr2: make([]int,2),
		CommonAttr3: make([]int,2),
		CommonAttr4: make([]int,2),
	}
}

type RideBase struct {
	pitaya.BaseTemplate
	Title string
	Id string
	RideName string
	NamePos float32
	RideType int
	RideLevel int
	RideQuality int
	RideIcon string
	NetxRideId int
	ModelId []int
	ScaleParam float32
	EffectPos []string
	EffectId []int
	SpeedRate int
	ActiveTime int
	SpecialPhyAttr1 []int
	SpecialPhyAttr2 []int
	SpecialMagicAttr1 []int
	SpecialMagicAttr2 []int
	CommonAttr1 []int
	CommonAttr2 []int
	CommonAttr3 []int
	CommonAttr4 []int
}
