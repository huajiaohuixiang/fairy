package template

import "github.com/topfreegames/pitaya"

var ConstantKey = &Constant{}

type Constant struct {
	pitaya.BaseTemplate
	Title string
	Id string
	Constant1 string
	Constant2 int
	Constant3 float32
	Constant4 float32
	Constant5 float32
	Constant6 float32
	Constant7 []int
}

func (p *Constant) NewTemplate() interface{} {
	return &Constant{
		Constant7: make([]int,10),
	}
}
