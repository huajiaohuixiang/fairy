package template

import (
	"github.com/topfreegames/pitaya"
	"regexp"
	"sort"
	"strings"
)

var StrFilterKey = &StrFilter{}

type (
	StrFilter struct {
		pitaya.BaseTemplate
		Title string
		Id string
		Description string
		Type int
		FilterReg *regexp.Regexp
	}

	StrFilterSlice []*StrFilter
)

func (p *StrFilterSlice) Len() int {
	return len(*p)
}

func (p *StrFilterSlice) Less(i, j int) bool{
	return (*p)[i].Id < (*p)[j].Id
}
func (p *StrFilterSlice) Swap(i, j int) {
	(*p)[i],(*p)[j] = (*p)[j],(*p)[i]
}


func (p *StrFilter) NewTemplate() interface{} {
	return &StrFilter{}
}

func (p *StrFilter) Init()  {
	list := pitaya.GetTemplates(StrFilterKey)
	if list == nil {
		return
	}

	slice := StrFilterSlice{}
	for _,v := range list {
		slice = append(slice,v.(*StrFilter))
	}

	sort.Sort(&slice)

	strs := make([]string,0)
	for _,v := range slice {
		strs = append(strs,v.Description)
	}

	str := strings.Join(strs,"|")
	p.FilterReg = regexp.MustCompile(str)
}




