package template

import "github.com/topfreegames/pitaya"

var FabaoSkillKey = &FabaoSkill{}

type FabaoSkill struct {
	pitaya.BaseTemplate
	Title string
	Id string
	Next int
	SkillOpenLevel int
	SkillBaseID int
	NeedMoneyNum int
	NeedItemNum []int
}

func (p *FabaoSkill) NewTemplate() interface{} {
	return &FabaoSkill{
		NeedItemNum: make([]int,2),
	}
}
