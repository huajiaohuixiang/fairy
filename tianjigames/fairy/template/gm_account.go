package template

import "github.com/topfreegames/pitaya"

var GMAccountKey = &GMAccount{}

func (p *GMAccount) NewTemplate() interface{} {
	return &GMAccount{
		IniItemID: make([]int,10),
		InitItemCout: make([]int,10),
	}
}

type GMAccount struct {
	pitaya.BaseTemplate
	Title string
	Id string
	AccountName string
	Password string
	AuthorityLevel int
	Level int
	IniItemID []int
	InitItemCout []int
}
