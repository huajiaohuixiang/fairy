package template

import "github.com/topfreegames/pitaya"

var EquiplayKey = &EquipPlay{}

type EquipPlay struct {
	pitaya.BaseTemplate
	Title string
	Id string
	StrengthExpRate int
	RefineMax int
	RefineProperty1 []int
	RefineProperty2 []int
	RefineProperty3 []int
	RefineProperty4 []int
	RefineProperty5 []int
	RefineProperty6 []int
}

func (p *EquipPlay) NewTemplate() interface{} {
	return &EquipPlay{
		RefineProperty1: make([]int,6),
		RefineProperty2: make([]int,6),
		RefineProperty3: make([]int,6),
		RefineProperty4: make([]int,6),
		RefineProperty5: make([]int,6),
		RefineProperty6: make([]int,6),
	}
}
