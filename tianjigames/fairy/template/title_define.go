package template

import "github.com/topfreegames/pitaya"

var TitleDefineKey = &TitleDefine{}

type TitleDefine struct {
	pitaya.BaseTemplate
	Title string
	Id string
	TitleType int
	TitleSort int
	TitleSubType []int
	Point int
	TimeLimit int
	TitleInfo string
	RecvPath string
	TitleName1 string
	TitleNameType1 []int
	TitlePicName1 string
	TitleName2 string
	TitleNameType2 []int
	TitlePicName2 string
	TitleName3 string
	TitleNameType3 []int
	TitlePicName3 string
	TitleName4 string
	TitleNameType4 []int
	TitlePicName4 string
	TitleName5 string
	TitleNameType5 []int
	TitlePicName5 string
	TitleName6 string
	TitleNameType6 []int
	TitlePicName6 string
	TitleName7 string
	TitleNameType7 []int
	TitlePicName7 string
	TitleName8 string
	TitleNameType8 []int
	TitlePicName8 string
	PositiveAttr1 []int
	PositiveAttr2 []int
	PassiveAttr1 []int
	PassiveAttr2 []int
}

func (p *TitleDefine) NewTemplate() interface{} {
	return &TitleDefine{
		TitleSubType: make([]int,2),
		TitleNameType1: make([]int,2),
		TitleNameType2: make([]int,2),
		TitleNameType3: make([]int,2),
		TitleNameType4: make([]int,2),
		TitleNameType5: make([]int,2),
		TitleNameType6: make([]int,2),
		TitleNameType7: make([]int,2),
		TitleNameType8: make([]int,2),
		PositiveAttr1: make([]int,2),
		PositiveAttr2: make([]int,2),
		PassiveAttr1: make([]int,2),
		PassiveAttr2: make([]int,2),
	}
}
