package template

import "github.com/topfreegames/pitaya"

var ItemKey = &Item{}

func (p *Item) NewTemplate() interface{} {
	return &Item{
		Fashion: make([]int,2),
	}
}

type Item struct {
	pitaya.BaseTemplate
	Title string
	Id string
	Remark string
	Name string
	Discribe string
	Quality int
	Bound int
	Level int
	Explain string
	Type int
	SubType int
	Profession int
	Composition int
	Icon string
	SoldItem int
	WingId int
	SoldItemCount int
	CoolGroup int
	Cooling int
	UseResult int
	TimingMode int
	Valid int
	IsInHouse int
	MaxOwnNum int
	IsQuickUse int
	IsBatchUse int
	State int
	Broadcast int
	Dropid int
	RideId int
	PetId int
	FaerieId int
	SoulId int
	Wingid int
	ComposeId int
	TitleId int
	SoulExp int
	TreasureId int
	MissionId int
	CardId int
	Fashion []int
	ProfessionLimit string
}
