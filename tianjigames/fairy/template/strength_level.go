package template

import "github.com/topfreegames/pitaya"

var StrengthLevelKey = &StrengthLevel{}

type StrengthLevel struct {
	pitaya.BaseTemplate
	Title string
	Id string
	StrengthLeve int `json:"strengthLevel"`
	BaseExp int
	TotalExp int
	NeedItem []int
	AddExp int
	AttrIncRate int
	CritRate1 int
	CritRate2 int
	CritRate3 int
	CritRate4 int
	NeedMoney int
}

func (p *StrengthLevel) NewTemplate() interface{} {
	return &StrengthLevel{
		NeedItem: make([]int,3),
	}
}