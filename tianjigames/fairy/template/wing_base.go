package template

import "github.com/topfreegames/pitaya"

var WingBaseKey = &WingBase{}

type WingBase struct {
	pitaya.BaseTemplate
	Title string
	Id string
	WingName string
	Icon string
	NeedExp int
	AddExp int
	RaiseCost []int
	DoubleRate int
	MagAttr1 []int
	PhyAttr1 []int
	Attr2 []int
	Attr3 []int
	Attr4 []int
}

func (p *WingBase) NewTemplate() interface{} {
	return &WingBase{
		MagAttr1: make([]int,2),
		PhyAttr1: make([]int,2),
		RaiseCost: make([]int,2),
		Attr2: make([]int,2),
		Attr3: make([]int,2),
		Attr4: make([]int,2),
	}
}

