package template

import "github.com/topfreegames/pitaya"

var EquipKey = &Equip{}

type Equip struct {
	pitaya.BaseTemplate
	Title               string
	Id                  string
	Remark              string
	Profession          int
	Postion             int
	WeaponResourceMode  int
	ClothesResourceMode int
	Resid               []int
	MeshResId           []int
	Property1           []int
	PropertyName1       string
	Property2           []int
	PropertyName2       string
	Property3           []int
	PropertyName3       string
	Property4           []int
	PropertyName4       string
	Property5           []int
	PropertyName5 string
	Property6 []int
	PropertyName6 string
	Property7 []int
	PropertyName7 string
	Property8 []int
	PropertyName8 string
	RandPropertyGenType int
	RandPropertyCount int
	RandProperty1 []int
	RandProperty2 []int
	RandProperty3 []int
	RandProperty4 []int
	RandProperty5 []int
	RandProperty6 []int
	EffectId int
}

func (p *Equip) NewTemplate() interface{} {
	return &Equip{
		Resid:         make([]int,2),
		MeshResId:     make([]int,2),
		Property1:     make([]int,3),
		Property2:     make([]int,3),
		Property3:     make([]int,3),
		Property4:     make([]int,3),
		Property5:     make([]int,3),
		Property6:     make([]int,3),
		Property7:     make([]int,3),
		Property8:     make([]int,3),
		RandProperty1: make([]int,4),
		RandProperty2: make([]int,4),
		RandProperty3: make([]int,4),
		RandProperty4: make([]int,4),
		RandProperty5: make([]int,4),
		RandProperty6: make([]int,4),
	}
}
