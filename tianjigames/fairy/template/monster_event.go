package template

import "github.com/topfreegames/pitaya"

var MonsterEventKey = &MonsterEvent{}

func (p *MonsterEvent) NewTemplate() interface{} {
	return &MonsterEvent{}
}

type MonsterEvent struct {
	pitaya.BaseTemplate
	Title string
	Id string
	Desc string
	CondType int
	CondParam int
	CondRate int
	ResultType1 int
	ResultParam1 int
	ResultType2 int
	ResultParam2 int
	ResultType3 int
	ResultParam3 int
	Count int
}
