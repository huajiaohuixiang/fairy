package template

import "github.com/topfreegames/pitaya"

var FashionKey = &Fashion{}

type Fashion struct {
	pitaya.BaseTemplate
	Title         string
	Id            string
	FashionType   int
	Name          string
	Quality       int
	MinLevel      int
	FashionDesc   string
	Icon          []string
	MaleModelId   []int
	FemaleModelId []int
	TextureId     []int
	UiBundleId    []int
	UiPosition    []float32
	Cost          []int
	JiankeAttr1   []int
	GongshouAttr1 []int
	QiangkeAttr1  []int
	FashiAttr1    []int
	JiankeAttr2   []int
	GongshouAttr2 []int
	QiangkeAttr2 []int
	FashiAttr2 []int
}

func (p *Fashion) NewTemplate() interface{} {
	return &Fashion{
		Icon:          make([]string,2),
		MaleModelId:   make([]int,4),
		FemaleModelId: make([]int,4),
		TextureId:     make([]int,2),
		UiPosition: make([]float32,6),
		UiBundleId:    make([]int,2),
		Cost:          make([]int,2),
		JiankeAttr1:   make([]int,3),
		GongshouAttr1: make([]int,3),
		QiangkeAttr1:  make([]int,3),
		FashiAttr1:    make([]int,3),
		JiankeAttr2:   make([]int,3),
		GongshouAttr2: make([]int,3),
		QiangkeAttr2:  make([]int,3),
		FashiAttr2: make([]int,3),
	}
}
