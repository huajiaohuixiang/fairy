package util

import "time"

var (
	DateUtil = newDataUtil()
)

const (
	MinuteMills = 1000*60
	HourMills   = MinuteMills*60
	DayMills    = HourMills *24
	WeekMills   = DayMills *7
)

type dateUtil struct {
	timeZoneOffset int64
}

func newDataUtil() *dateUtil {
	_,offset := time.Now().Zone()
	return &dateUtil{
		timeZoneOffset: int64(offset*1000),
	}
}

func (p *dateUtil) GetDayFirstMills(time int64) int64 {
	return time - (time + p.timeZoneOffset) %DayMills
}

