package util

import (
	"os"
)

var FileUtil = &fileUtil{}

type fileUtil struct {
	
}

/**
判断文件是否存在
 */
func (p *fileUtil) Exist(fileName string) (bool,error) {
	_,err := os.Stat(fileName)
	if err == nil {
		return true,nil
	}

	if os.IsNotExist(err) {
		return false,nil
	}

	return false,err
}
