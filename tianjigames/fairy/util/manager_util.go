package util

import (
	"github.com/go-redis/redis"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/logger"
	"github.com/topfreegames/pitaya/modules"
	"github.com/xormplus/xorm"
)

var ManagerUtil = &managerUtil{}

type managerUtil struct {

}

func (m *managerUtil) GetDbEnginer() (*xorm.Engine,error) {
	enginer,err := pitaya.GetModule("databaseStorage")
	if err != nil {
		logger.Log.Errorf("GetDbEnginer failed,error:%s",err.Error())
		return nil,err
	}

	return enginer.(*modules.DatabaseStorage).Enginer,nil
}

func (m *managerUtil) GetRedisClient() (*redis.Client,error) {
	redisStorage,err := pitaya.GetModule("redisStorage")
	if err != nil {
		logger.Log.Errorf("GetDbEnginer failed,error:%s",err.Error())
		return nil,err
	}
	return redisStorage.(*modules.RedisStorage).Client,nil
}
