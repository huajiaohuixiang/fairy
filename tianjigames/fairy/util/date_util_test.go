package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestDateUtil_GetDayFirstMills(t *testing.T) {
	now := time.Now().UnixNano()/1e6
	assert.EqualValues(t, 1629993600000,DateUtil.GetDayFirstMills(now))
}
