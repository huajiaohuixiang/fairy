package util

import (
	"github.com/tianjigames/fairy/template"
	"regexp"
)

var (
	StringFilter = &stringFilter{
		accountReg: regexp.MustCompile("^[A-Za-z0-9]+$"),
		nameReg: regexp.MustCompile("\\$|\\\\n|\\]|\\&|\\#|em| |　"),
	}
)

type stringFilter struct {
	accountReg *regexp.Regexp
	nameReg *regexp.Regexp
}

func (p *stringFilter) CheckAccount(account string) bool {
	return p.accountReg.MatchString(account)
}

func (p *stringFilter) CheckString(str string) (bool,error) {
	if template.StrFilterKey.FilterReg == nil {
		return false,nil
	}

	return template.StrFilterKey.FilterReg.MatchString(str),nil
}

