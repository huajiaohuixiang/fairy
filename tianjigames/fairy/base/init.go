package base

import (
	"flag"
	"github.com/spf13/viper"
	"github.com/topfreegames/pitaya"
	"github.com/topfreegames/pitaya/modules"
	"github.com/topfreegames/pitaya/serialize/protobuf"
)

func BaseInit()  {
	svType := flag.String("type", "connector", "the server type")
	isFrontend := flag.Bool("frontend", true, "if server is frontend")
	//rpcServerPort := flag.String("rpcsvport", "4222", "the port that grpc server will listen")

	flag.Parse()

	defer pitaya.Shutdown()

	pitaya.SetSerializer(protobuf.NewSerializer())
	confs := viper.New()
	//confs.Set("pitaya.cluster.rpc.server.grpc.port", *rpcServerPort)

	meta := map[string]string{
		//constants.GRPCHostKey: "127.0.0.1",
		//constants.GRPCPortKey: *rpcServerPort,
	}
	pitaya.Configure(*isFrontend, *svType, pitaya.Cluster, meta, confs)

	bs := modules.NewETCDBindingStorage(pitaya.GetServer(), pitaya.GetConfig())
	pitaya.RegisterModule(bs, "bindingsStorage")

	//注册数据库
	ds,err1 := modules.NewDatabaseStorage(pitaya.GetConfig())
	if err1 != nil {
		panic(err1)
	}
	pitaya.RegisterModule(ds,"databaseStorage")

	//注册redis
	rs := modules.NewRedisStorage(pitaya.GetConfig())
	pitaya.RegisterModule(rs,"redisStorage")

	bs.Init()
	ds.Init()
	rs.Init()
}
