// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: inner_message_wp_msg.proto

package protos

import (
	fmt "fmt"
	_ "github.com/gogo/protobuf/gogoproto"
	proto "github.com/gogo/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type WPOnGameServerPlayerNum struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WPOnGameServerPlayerNum) Reset()         { *m = WPOnGameServerPlayerNum{} }
func (m *WPOnGameServerPlayerNum) String() string { return proto.CompactTextString(m) }
func (*WPOnGameServerPlayerNum) ProtoMessage()    {}
func (*WPOnGameServerPlayerNum) Descriptor() ([]byte, []int) {
	return fileDescriptor_1092525362a2490d, []int{0}
}
func (m *WPOnGameServerPlayerNum) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WPOnGameServerPlayerNum.Unmarshal(m, b)
}
func (m *WPOnGameServerPlayerNum) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WPOnGameServerPlayerNum.Marshal(b, m, deterministic)
}
func (m *WPOnGameServerPlayerNum) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WPOnGameServerPlayerNum.Merge(m, src)
}
func (m *WPOnGameServerPlayerNum) XXX_Size() int {
	return xxx_messageInfo_WPOnGameServerPlayerNum.Size(m)
}
func (m *WPOnGameServerPlayerNum) XXX_DiscardUnknown() {
	xxx_messageInfo_WPOnGameServerPlayerNum.DiscardUnknown(m)
}

var xxx_messageInfo_WPOnGameServerPlayerNum proto.InternalMessageInfo

type PWOnGameServerPlayerNumRet struct {
	Num                  int32    `protobuf:"varint,1,opt,name=num" json:"num"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PWOnGameServerPlayerNumRet) Reset()         { *m = PWOnGameServerPlayerNumRet{} }
func (m *PWOnGameServerPlayerNumRet) String() string { return proto.CompactTextString(m) }
func (*PWOnGameServerPlayerNumRet) ProtoMessage()    {}
func (*PWOnGameServerPlayerNumRet) Descriptor() ([]byte, []int) {
	return fileDescriptor_1092525362a2490d, []int{1}
}
func (m *PWOnGameServerPlayerNumRet) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PWOnGameServerPlayerNumRet.Unmarshal(m, b)
}
func (m *PWOnGameServerPlayerNumRet) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PWOnGameServerPlayerNumRet.Marshal(b, m, deterministic)
}
func (m *PWOnGameServerPlayerNumRet) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PWOnGameServerPlayerNumRet.Merge(m, src)
}
func (m *PWOnGameServerPlayerNumRet) XXX_Size() int {
	return xxx_messageInfo_PWOnGameServerPlayerNumRet.Size(m)
}
func (m *PWOnGameServerPlayerNumRet) XXX_DiscardUnknown() {
	xxx_messageInfo_PWOnGameServerPlayerNumRet.DiscardUnknown(m)
}

var xxx_messageInfo_PWOnGameServerPlayerNumRet proto.InternalMessageInfo

func (m *PWOnGameServerPlayerNumRet) GetNum() int32 {
	if m != nil {
		return m.Num
	}
	return 0
}

func init() {
	proto.RegisterType((*WPOnGameServerPlayerNum)(nil), "protos.WPOnGameServerPlayerNum")
	proto.RegisterType((*PWOnGameServerPlayerNumRet)(nil), "protos.PWOnGameServerPlayerNumRet")
}

func init() { proto.RegisterFile("inner_message_wp_msg.proto", fileDescriptor_1092525362a2490d) }

var fileDescriptor_1092525362a2490d = []byte{
	// 154 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0xca, 0xcc, 0xcb, 0x4b,
	0x2d, 0x8a, 0xcf, 0x4d, 0x2d, 0x2e, 0x4e, 0x4c, 0x4f, 0x8d, 0x2f, 0x2f, 0x88, 0xcf, 0x2d, 0x4e,
	0xd7, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x03, 0x53, 0xc5, 0x52, 0xba, 0xe9, 0x99, 0x25,
	0x19, 0xa5, 0x49, 0x7a, 0xc9, 0xf9, 0xb9, 0xfa, 0xe9, 0xf9, 0xe9, 0xf9, 0xfa, 0x60, 0xf1, 0xa4,
	0xd2, 0x34, 0x30, 0x0f, 0xcc, 0x01, 0xb3, 0x20, 0xda, 0x94, 0x24, 0xb9, 0xc4, 0xc3, 0x03, 0xfc,
	0xf3, 0xdc, 0x13, 0x73, 0x53, 0x83, 0x53, 0x8b, 0xca, 0x52, 0x8b, 0x02, 0x72, 0x12, 0x2b, 0x53,
	0x8b, 0xfc, 0x4a, 0x73, 0x95, 0x4c, 0xb8, 0xa4, 0x02, 0xc2, 0xb1, 0x4a, 0x05, 0xa5, 0x96, 0x08,
	0x89, 0x71, 0x31, 0xe7, 0x95, 0xe6, 0x4a, 0x30, 0x2a, 0x30, 0x6a, 0xb0, 0x3a, 0xb1, 0x9c, 0xb8,
	0x27, 0xcf, 0x10, 0x04, 0x12, 0x70, 0xe2, 0x8c, 0x62, 0x87, 0x58, 0x59, 0x0c, 0x08, 0x00, 0x00,
	0xff, 0xff, 0x09, 0xde, 0x65, 0x92, 0xaf, 0x00, 0x00, 0x00,
}
