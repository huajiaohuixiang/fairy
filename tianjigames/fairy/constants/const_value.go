package constants

const (
	Per = 1000
	PlayerDefaultSpeedId = "init_speed"//角色初始速度

	//技能相关
	SkillTypeActive = 1 //主动技能
	SkillTypePassive = 0 //被动技能

	Strength10VipLimitTip = "strength_10_vip_level" //vip10的强化等级

	EquipRandAttrConst = "equip_rand_attr" //装备随机属性重复概率配置
	EquipWashConst = "equip_wash" //装配洗练伪随机配置

	RideMaxNumber = 30 //玩家所能拥有的最大坐骑数量

	//称号相关
	MaxTitleNum = 100 //最大称号数量
	TitlePositiveAttr = 2 //激活的称号加成的属性数量
	TitlePassitiveAttr = 2 //未激活的称号加成的属性数量
	TitleIdxAttrLen = 2 //称号加成属性数组的长度

	TreasureAttr = 4 //宝物对玩家基本属性影响的数量
	TreasureIdxAttrLen = 3 //宝物对玩家基本属性加成信息数组长度

	FashionIdxAttrLen = 3 //服装对玩家基本属性加成信息数组长度

	//提示类型
	MessageTypeNone = 0; // 配表类型
	MessageTypeFloatWordTips = 1; // 飘字提示
	MessageTypeConfirmTips = 2; // 确认提示
	MessageTypeInputTips = 3; // 输入提示
	MessageTypeEquipmentTips = 4; // 装备提示
	MessageTypeMailTips = 5; // 邮件提示
	MessageTypeChatTips = 6; // 聊天提示
	MessageTypeNoticeTips = 7; // 公告提示
	MessageTypeIconTips = 8; // 图标提示
	MessageTypeBigFloatWordTips = 10; // 世界级飘字提示

	//组队相关
	TeamMaxMember = 4 //组队最大队员数

	MaxObjScanListSize = 128 //单次扫描场景中某个矩形区间内角色的最大数量

	//任务相关常量
	MaxMissionNum = 4096
	MaxCharMissionNum = 10 //任务数据数组的长度
	MaxCharMissionFlagLen = (MaxMissionNum + 32)/32 //每一个任务完成标志长度
	MaxCharMissionDataNum = 512 //自定义任务数据的数组长度
	MaxCharMissionFlagDataNum = 10 //任务完成标志数据
	MaxMissionDataParamNum = 6 //单个任务参数的最大数量
	MaxMissionStringParamLen = 32 //字符串参数字节数
	McMissionMain = 1 //主线
	McMissionBranch = 2//支线
	McMissionDaily = 3 //日常
	McMissionWeekly = 4 //周循环任务
	McMissionActivity = 5 //活动任务
	McMissionDailyCamp = 6 //阵营日常
	McMissionCircleGang = 7 //帮会跑环
	McMissionDragon = 8 //pvp活动任务
	McMissionOther = 9 //其他
	McMissionChain //任务链模式

	//技能相关
	MaxSkillNum = 50 //角色所拥有的最大技能书
	MaxEquipativeSkillNum //角色可装备主动技能数量
	MaxFabaoSkillNum = 8 //法宝最大技能数

	//pk杀戮值相关
	PkHitBackCount = 10 //反击列表最大人数上限

	//翅膀相关
	MaxWingSize = 30 //最多拥有的翅膀数量
	WingDataVersion1 = 1
	WingExpSize = 4
	WingDataSize = 6
	ActiveMissionId = 1141

	//元宝商城相关
	LimitIngotItemCount = 100 //限购元宝数量
	//商店相关
	LimitShopItemCount = 200 //限购商品数量

	//活动相关
	MaxActivityNum = 60 //活动的最大数量
	MaxActivitySavaNum = MaxActivityNum + 5 //活动保存最大值

	//聊天相关
	ChannelNum = 8 //目前开放的频道数量

	//玩家战斗装填
	PlayerBattleStatePatrol = 1 //休闲状态
	PlayerBattleStateBattle =2 //战斗状态

	//玩家状态
	PlayerStateInvalid = -1 //无效状态
	PlayerStateSportOut = 2 //玩家处于观战状态
	PlayerStateNormal = 6 //玩家状态正常，在游戏中
	PlayerStateWaitDisconnect = 9 //玩家等待重新连接
	PlayerStateWaitEnterSceneOk = 10 //等待进入场景

	//结义金兰相关
	JieYiPlayerMaxCount = 4 //结义要求队员数

	//装备随机属性数量
	EquipRandAttrCount = 6

	StateTrue = 1 //将该状态设置为true
	StateFalse = 0 //将该状态设置为false

	//物品状态下标值
	ItemStateBody = 0 //物品是否可以穿在身上
	ItemStateBind = 1 //物品是否已经绑定
	ItemStateTime = 2 //物品有效期标志
	ItemStateStore = 3 //物品是否在仓库中

	//宝物寻仙相关
	TreasureStateLock = 0 //宝物状态下标 是否锁定
	TreasureBagNum = 100 //宝物背包格子数量
	TreasureBodyNum = 10 //宝物装备格子数量

	//背包相关
	BagMaxCellCount = 80 //最大格子数
	BagDefaultCellCount = 48 //默认格子数
	BagExpandPerTimes = 4 //每次扩充开放的背包格子数量

	//仓库相关
	StoreMaxCellCount = 80 //玩家仓库最大格子熟练g
	StoreDefaultCellCount = 48 //玩家默认开放仓库的格子数量
	StoreExpandPerTimes = 16 //每次扩容开放的格子数量

	//constant表常量id
	ParamKeyDefaultWeapon = "default_weapon" //初始武器
	FaBaoUnlockId = "fabao_unlock" //法宝解锁

	//法宝对属性战力值影响权重
	ParamKeyBellipotentPatk = "bellipotent_pattack"//物理攻击战力值影响权重
	ParamKeyBellipotentMatk = "bellipotent_mattack" //法术攻击战力值影响权重
	ParamKeyBellipotentPdef = "bellipotent_pdefense" //物理防御战力值影响权重
	ParamKeyBellipotentMdef = "bellipotent_mdefense" //法术防御战力值影响权重
	ParamKeyBellipotentHp = "bellipotent_hp" //生命值战力值影响权重
	ParamKeyBellipotentMp = "bellipotent_mana" //法力值战力值影响权重
	ParamKeyBellipotentHit = "bellipotent_hit" //命中值战力值影响权重
	ParamKeyBellipotentDod = "bellipotent_dodge" //闪避值战力值影响权重
	ParamKeyBellipotentCrit = "bellipotent_crit" //暴击值战力值影响权重
	ParamKeyBellipotentTen = "bellipotent_tenacity" //暴抗值战力值影响权重
	ParamKeyBellipotentParry = "bellipotent_parry" //格挡值战力值影响权重
	ParamKeyBellipotentSund = "bellipotent_sunderarmor" //破击值战力值影响权重
	ParamKeyBellipotentCritInc = "bellipotent_crit_increased" //暴击加深值战力值影响权重
	ParamKeyBellipotentCritRedu = "bellipotent_crit_reduction" //暴击减免值战力值影响权重
	ParamKeyBellipotentCritHit = "bellipotent_crit_hit" //会心一击值战力值影响权重
	ParamKeyBellipotentStrDef = "bellipotent_strength_defuse" //御劲化解值战力值影响权重
	ParamKeyBellipotentDamInc = "bellipotent_damage_increased" //伤害加深值战力值影响权重
	ParamKeyBellipotentDamRedu = "bellipotent_damage_reduction" //伤害减免值战力值影响权重
	ParamKeyBellipotentExtraHit = "bellipotent_extra_hit" //额外命中值战力值影响权重
	ParamKeyBellipotentExtraDod = "bellipotent_extra_dodge" //额外闪避值战力值影响权重
	ParamKeyBellipotentExtraCrit = "bellipotent_extra_crit" //额外暴击值战力值影响权重
	ParamKeyBellipotentExtraTen = "bellipotent_extra_tenacity" //额外暴抗值战力值影响权重
	ParamKeyBellipotentExtraParry = "bellipotent_extra_parry" //额外格挡值战力值影响权重
	ParamKeyBellipotentExtraSund = "bellipotent_extra_sund" //额外穿透值战力值影响权重
	ParamKeyBellipotentAtkIce = "bellipotent_attack_ice" //冰攻击值战力值影响权重
	ParamKeyBellipotentResIce = "bellipotent_resist_ice" //冰抗值战力值影响权重
	ParamKeyBellipotentReduResIce = "bellipotent_reduce_resist_ice" //冰减抗值战力值影响权重
	ParamKeyBellipotentAtkFire = "bellipotent_attack_fire" //火攻击值战力值影响权重
	ParamKeyBellipotentResFire = "bellipotent_resist_fire" //火抗值战力值影响权重
	ParamKeyBellipotentReduResFire = "bellipotent_reduce_resist_fire" //火减抗值战力值影响权重
	ParamKeyBellipotentAtkThunder = "bellipotent_attack_thunder" //雷攻击值战力值影响权重
	ParamKeyBellipotentResThunder = "bellipotent_resist_thunder" //雷抗值战力值影响权重
	ParamKeyBellipotentReduResThunder = "bellipotent_reduce_resist_thunder" //雷减抗值战力值影响权重
	ParamKeyBellipotentAtkPoison = "bellipotent_attack_poison" //雷攻击值战力值影响权重
	ParamKeyBellipotentResPoison = "bellipotent_resist_poison" //雷抗值战力值影响权重
	ParamKeyBellipotentReduResPoison = "bellipotent_reduce_resist_poison" //雷减抗值战力值影响权重

	//排行榜相关
	StandTypePower = 1//玩家战斗力
	StandTypeGang = 2//帮会战斗力
	StandTypeLevel = 3 //玩家等级
	StandTypeSmzt = 4//锁妖塔挑战层数
	StandTypeGem = 5//宝石战斗力
	StandTypeFabao = 6 //法宝战斗力
	StandTypeSoul = 7//元魂总评书
	StandTypeRide = 8//坐骑战斗力
	StandTypeCharm = 9//总魅力值
	StandTypeAchievement = 10 //成就值
	StandTypeExam = 11 //考试皇榜

	//效果相关
	MaxImpact = 14 //效果最大数

	//角色状态
	RoleStatusPosNormal = 0x1 //正常态
	RoleStatusPosSkill = 0x2

	//角色状态参数
	ObjStatusCombat = 0 //是否战斗
	ObjStatusDizz = 1 //眩晕计时
	ObjStatusRide = 2 //骑乘id
	ObjStatusSoul = 3 //变声id
	ObjStatusNum = 4

	//日志相关
	LogSubTypeMissTF = 0
	LogSubTypeMissBS = 1
	LogSubTypeMissCD = 2
	LogSubTypeMissGD = 3
	LogSubTypeMissWeek = 4

	//帮会活动相关
	GangActWefareWeek = 1 //每周福利
	GangActWefareSalary = 2 //帮会工资
	GangActWefareDay = 3 //每日福利


	//排行榜奖励
	RankReward = 1


)
