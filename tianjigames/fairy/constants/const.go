package constants

const (

	//服务器类型
	SvTypeConnector ="connector"
	SvTypeHall = "hall"
	SvTypeGame = "game"
	SvTypeWorld = "world"
	SvTypePlayer = "player"

	UIdKey = "userId"

	DaemonThread = "daemon_thread"
	LoginThread = "login_thread"
	IncomingThread = "incoming_thread"

	MaxRoleNum = 3//最多可以创建角色的数量
	PlayerPosBase = 100


)
