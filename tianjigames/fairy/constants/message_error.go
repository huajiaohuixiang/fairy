package constants



const (
	SuccessRet             = int32(0)      //登录成功
	RegisterReUserError    = int32(100002) //用户已经注册
	LoginSystemUnknowError = int32(100003) //系统未知错误
	LoginUsernameError     = int32(100004) //用户不存在
	LoginPasswordError     = int32(100005) //密码错误
	LoginParamsError       = int32(100008) //登录参数错误
	LoginUserNotActive     = int32(100009) //该账号尚未激活，需输入激活码
	LoginUserStateForbid   = int32(100014) //账号封停，不允许登录

	RoleNotRoundError = int32(100100) //角色不存在


	WorldKickUserSend = int32(100015)//已发送踢出消息，等待断线
	WorldKickUserOffline = int32(100016)//找不到人，已经不在线了

	RoleReNameError = int32(100104)//角色名已存在
	ContentInvalid = int32(100106) //包含非法字符
	AccountLengthError = int32(100107) //用户名长度非法
	NicknameLengthError = int32(100108)//昵称长度过段
	RoleCreateFailTooMany = int32(100114)//创建角色失败---超多最大可创建角色数
	RoleCreateFailIndex = int32(100115)//创建角色失败--角色索引参数不对
	NicknameContentError = int32(100212)//昵称包含屏蔽字

	InitGameAuthSuccess    = int32(0)      //成功
	InitGameFailError      = int32(300200) //初始化游戏失败
	WorldValidateFailError = int32(300201) //world 验证失败
	PlayereUnloginError    = int32(300202) //用户未登陆
	PlayerTicketError      = int32(300203) //用户ticket验证失败
	PlayerForceToLogin     = int32(300204) //强制跳转到登陆去吧
	PlayerPoolFull         = int32(300205) //玩家池满了
	InitGameWaitConnect    = int32(300206) //麻烦等一会再发送
	PlayerNoDataError      = int32(300207) //用户未登陆

	//通用提示
	RoleOffline = 2000 //角色已经掉线
	PlayerInOtherTeam	= 2004 //角色在其他队伍中
	OperOnlyByTeamLeader = 2009 //只有队长才能操作

	//背包装备相关
	ItemOptSucc = 0
	ItemIsNullError = 300600

	//帮会创建场景
	GangItemMaxError = 500107

	//采集相关
	ItemOwnMaxError = 300646
)

