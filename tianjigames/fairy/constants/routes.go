package constants

import "fmt"

var (
	CheckPlayerNameRoute       = fmt.Sprintf("%s.hall.checkplayername", SvTypeWorld)
	CheckPlayerNameRetRoute     = fmt.Sprintf("%s.hall.checkplayername_ret", SvTypeWorld)
	LWAddNewPlayerRoute         = fmt.Sprintf("%s.hall.addnewplayer", SvTypeWorld)
	GwDeleteTmpPlayerNameRoute  = fmt.Sprintf("%s.hall.deletetmpplayername", SvTypeWorld)
	LWGetGSInfoRetRoute         = fmt.Sprintf("%s.hall.getgsinfo_ret", SvTypeWorld)
	GWPlayerAuthRoute           = fmt.Sprintf("%s.game.playerauth", SvTypeWorld)
	LWGetGSInfoRoute            = fmt.Sprintf("%s.hall.getgsinfo", SvTypeWorld)
	LWCheckLoginNumRoute        = fmt.Sprintf("%s.hall.checkloginnum", SvTypeWorld)
	GWRetForceLogout            = fmt.Sprintf("%s.game.forcelogout",SvTypeWorld)
	GWCreateTeamRoute           = fmt.Sprintf("%s.team.create",SvTypeWorld)
	GWInviteJoinTeamRoute       = fmt.Sprintf("%s.team.invite",SvTypeWorld)
	GWAddOrUpdateStandDataRoute = fmt.Sprintf("%s.rank.addorupdatestanddata",SvTypeWorld)


	RoleCreateRetRoute         = fmt.Sprintf("%s.role.create_ret", SvTypeHall)
	BeginGameRetRoute          = fmt.Sprintf("%s.game.begingame_ret", SvTypeHall)
	PlayerLogoutRoute 			= fmt.Sprintf("%s.game.playerlogout",SvTypeHall)
	LCPushWaitLoginNumRetRoute = fmt.Sprintf("%s.login.waitloginnum_ret", SvTypeHall)
	PHOnSessionClosedRoute     = fmt.Sprintf("%s.gate.onsessionclosed", SvTypeHall)
	LCLoginQueueIndexRetRoute  = fmt.Sprintf("%s.login.loginqueueindex_ret", SvTypeHall)
	LoginRetRoute = fmt.Sprintf("%s.login.login_ret", SvTypeHall)

	LPPlayerOnLoginServerRoute = fmt.Sprintf("%s.player.playeronloginserver", SvTypePlayer)
	GatePOnSessionCreatedRoute = fmt.Sprintf("%s.player.onsessioncreated", SvTypePlayer)
	GatePOnSessionClosedRoute  = fmt.Sprintf("%s.player.onsessionclosed", SvTypePlayer)
	LPAccountBindSessionRoute  = fmt.Sprintf("%s.player.accountbindsession", SvTypePlayer)
	WPOnGameServerPlayerNumRoute  = fmt.Sprintf("%s.player.getongameserverplayernum", SvTypePlayer)

	CGInitGameRoute = fmt.Sprintf("%s.game.initgame",SvTypeGame)
	WGTeamResultRoute = fmt.Sprintf("%s.team.result",SvTypeGame)
	WGTeamDismissRoute = fmt.Sprintf("%s.team.dismiss",SvTypeGame)
	GCEnterTeamRetRoute = fmt.Sprintf("%s.team.enter",SvTypeGame)
	GCRespLeaveTeam = fmt.Sprintf("%s.team.leave",SvTypeGame)
	GCMyTeamRet = fmt.Sprintf("%s.team.myteam_ret",SvTypeGame)
	GCRespInviteList = fmt.Sprintf("%s.team.invitelist",SvTypeGame)
	GCOperTeamFollow = fmt.Sprintf("%s.team.follow",SvTypeGame)
	GCRefeshTeamInfo = fmt.Sprintf("%s.team.refreshteaminfo",SvTypeGame)
	GCCommonPrompt = fmt.Sprintf("%s.common.prompt",SvTypeGame)
	GCActiveFabaoRetRoute = fmt.Sprintf("%s.fabao.activefabaoinfo_ret",SvTypeGame)
	WGRankRewardRetRoute = fmt.Sprintf("%s.rank.rankreward",SvTypeGame)
)
