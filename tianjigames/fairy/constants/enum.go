package constants

type (
	GuidType int

	ItemType int
	MissionType int
	VirtualType int
	QualityType int
	EquipRandAttrGenType int //装备随机属性生产方式
)

const (
	_ GuidType = iota
	GuidTypePlayer
	GuidTypeItem
	GuidTypeMail
	GuidTypeGang
	GuidTypeAccount
	GuidTypeiden
	GuidTypeFabao
	GuidTypeoul
	GuidTypeAuction
	GuidTreasure
	GuidTypeBet
	GuidTypeRedPacket
)

const (
	//无效 / 虚拟物品 / 装备 / 宝石 / 合成材料 / 药品 / 可使用 / 普通 / 任务
	ItemTypeInvalid ItemType = iota
	ItemTypeVirtual
	ItemTypeEquip
	ItemTypeGem
	ItemTypeCompose
	ItemTypeDrugs
	ItemTypeUseAble
	ItemTypeNormal
	ItemTypeCard
	ItemTypeMisssion
)

const (
	//虚拟物品类型
	VirtualTypeInvalid VirtualType = iota
	VirtualTypeCoin //金币
	VirtualTypeIngot //元宝
	VirtualTypeBindIngot //绑定元宝
	VirtualTypeGMIngot //gm元宝
	VirtualTypeExp //经验
	VirtualTypeSKillPoint //技能点
	VirtualTypeStoryPoint //阅历点
	VirtualTypeEquipEssence //装备精华
	VirtualTypeVipExp //vip经验
	VirtualTypeGangContri //战队贡献
	VirtualTypeXianYuan //仙缘
	VirtualTypeGangWealth //帮会资金
	VirtualTypeArenaMoney //竞技场金钱
	VirtualTypePreStige //阵营威望
	VirtualTypeBetIntegral //下注积分
	VirtualTypeSoulEssence //灵魂精华
	VirtualTypeCardEssence //卡精华
	VirtualTypeMaxValue //最大值
)

const (
	//任务子类型
	MissionTypeInvalid MissionType = iota
	MissionTypeUnUseAble
	MissionTypeUseAble
	MissionTypeMultiMusic
	MissionTypeChaoDu
	MissionTypeReuseable
	MissionTypeMusic
)

const (
	QualityTypeInvalid QualityType = iota
	QualityTypeWhite
	QualityTypeBlue
	QualityTypePurple
	QualityTypeGold
	QualityTypeOrange
	QualityTypeMax
)

const (
	EquipRandAttrGenTypeInvalid EquipRandAttrGenType = iota //不随机
	EquipRandAttrGenTypeConfig //配置表
	EquipRandAttrGenTypeRand //程序随机生成
	EquipRandAttrGenTypeAuthen  //鉴定
)

type AttrFlag int

const (
	AttrStart AttrFlag = iota

	//一级属性
	AttrStr //力道
	AttrSpi //元气
	AttrCon //体质
	AttrAgi  //敏捷

	AttrAdSpace1 //广告招租位1
	AttrAdSpace2 //广告招租位2
	AttrAdSpace3 //广告招租位3
	AttrADSpace4 //广告招租位4

	AttrMaxHp //生命值
	AttrMaxMp //法力值
	AttrAttackPhy //物理攻击
	AttrAttackMagic //法术攻击
	AttrDefencePhy //物理防御
	AttrDefenceMagic //法术防御
	AttrHit //命中
	AttrMiss //闪避
	AttrCritical //暴击
	AttrCritResist //暴击抵抗
	AttrCritIncreased //暴击加深
	AttrCritReduction //暴击减免
	AttrThrough //穿透
	AttrPatty //格挡
	AttrCriticalHit //会心一击
	AttrStrengthDefuse //御劲化解
	AttrDamageIncreased //伤害加深
	AttrDamageReduction //伤害减免
	AttrExtraHit //额外命中xtra
	AttrExtraMiss //额外闪避
	AttrExtraCrtical //额外暴击
	AttrExtraCritResist //额外暴击抵抗
	AttrExtraThrough //额外穿透
	AttrExtraPatty //额外格挡

	AttrAdSpace5 //广告位招租5
	AttrAdSpace6 //广告位招租6
	AttrAdSpace7 //广告位招租7
	AttrAdSpace8 //广告位招租8
	AttrAdSpace9 //广告位招租9
	AttrAdSpace10 //广告位招租10

	//元素属性
	AttrAttackIce           //冰攻击
	AttrResistIce           //冰抗性
	AttrReduceResistIce     //冰减抗
	AttrAttackFire          //火攻击
	AttrResistFire          //火抗性
	AttrReduceResistFire    //火减抗
	AttrAttackThunder       //雷攻击
	AttrResistThunder       //雷抗性
	AttrReduceResistThunder //雷减抗
	AttrAttackPoison        //毒攻击
	AttrResistPoison        //毒抗性
	AttrReduceResistPoison  //毒减抗

	AttrAdSpace11 //广告位招租11
	AttrAdSpace12 //广告位招租12
	AttrAdSpace13 //广告位招租13
	AttrAdSpace14 //广告位招租14
	AttrAdSpace15 //广告位招租15
	AttrAdSpace116 //广告位招租16

	//特殊属性
	AttrMoveSpeed //移动速度
	AttrAttackSpeed //攻击速度
	AttrHpRegeneRate //生命值回复速度
	AttrMpRegeneRate //法力值回复速度
	AttrRage //怒气值

	AttrAdSpace17 //广告位招租17
	AttrAdSpace18 //广告位招租18
	AttrAdSpace19 //广告位招租19
	AttrAdSpace20 //广告位招租20

	//特殊辅助属性
	AttrAttackAllelem       //对所有的攻击类属性有加成效果
	AttrResistAllelem       //对所有的抗类属性有加成效果
	AttrReduceResistAllelem //对所有的减抗类属性有加成效果
	AttrAttackAll           //全攻击物理法术有加成效果
	AttrDefenceAll          //全防御有加成效果
	AttrMaxIndex            //属性最大值
)

func GetAttrFlagByValue(value int) *AttrFlag {
	if value < int(AttrStart) || value > int(AttrMaxIndex) {
		return nil
	}

	rt := AttrFlag(value)
	return &rt
}

type LogType int

const (
	_ LogType = iota -1
	LogTypeInv
	LogTypeMission
	LogTypeTongTian
	LogTypeCopyScene
	LogTypeArena
	LogTypeDuel
	LogTypeFabao
	LogTypeRide
	LogTypeWing
	LogTypeEquip
	LogTypeSoul
	LogTypeSkill
	LogTypeGem
	LogTypeShop
	LogTypeMail
	LogTypeAction
	LogTypeBag
	LogTypeStore
	LogTypePlayerAction
	LogTypeTitle
	LogTypeTreasure
	LogTypeGang
	LogTypeOnlineGift
	LogTypeStand
	LogTypeSocial
	LogTypeRecharge
	LogTypeLiveNess
	LogTypeResourceFind
	LogTypeActivity
	LogTypeCard
	LogTypeSevenDayAct
	LogTypeWeekCard
	LogTypeMonthCard
	LogTypeRebate
	LogTypeFashion
	LogTypeLoginReward
	LogTypeWorldBoss
	LogTypeTimecut
	LogTypeBagItem
	LogTypeMoneyMake
)

