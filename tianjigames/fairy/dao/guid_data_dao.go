package dao

import (
	"sync"
)
var (
	GuidDataDao     *guidDataDao
	guidDataDaoOnce sync.Once
)

type guidDataDao struct {
	BaseDao
}

func NewGuidDataDao() *guidDataDao {
	guidDataDaoOnce.Do(func() {
		GuidDataDao = &guidDataDao{}
	})
	return GuidDataDao
}
