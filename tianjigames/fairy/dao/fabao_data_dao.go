package dao

import (
	"sync"
)

var (
	FabaoDataDao     *fabaoDataDao
	fabaoDataDaoOnce sync.Once
)

type fabaoDataDao struct {
	BaseDao
}

func NewFabaoDataDao() *fabaoDataDao {
	fabaoDataDaoOnce.Do(func() {
		FabaoDataDao = &fabaoDataDao{}
	})
	return FabaoDataDao
}

