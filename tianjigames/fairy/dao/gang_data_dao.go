package dao

import (
	"sync"
)

var (
	GangDataDao *gangDataDao
	gangDataDaoOnce sync.Once
)

type gangDataDao struct {
	BaseDao
}

func NewGangDataDao() *gangDataDao {
	gangDataDaoOnce.Do(func() {
		GangDataDao = &gangDataDao{}
	})
	return GangDataDao
}
