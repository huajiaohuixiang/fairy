package dao

import (
	"github.com/tianjigames/fairy/pojo"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	WeddingInvitationDataDao *weddingInvitationDataDao
	weddingInvitationDataDaoOnce sync.Once
)

type weddingInvitationDataDao struct {
	BaseDao
}

func NewWeddingInvitationDataDao() *weddingInvitationDataDao {
	weddingInvitationDataDaoOnce.Do(func() {
		WeddingInvitationDataDao = &weddingInvitationDataDao{}
	})
	return WeddingInvitationDataDao
}

func (p *weddingInvitationDataDao) GetListByDb(guid int64) ([]*pojo.WeddingInvitationData,error) {
	rs := make([]*pojo.WeddingInvitationData,0)
	err := p.enginer.Where("playerguid=?",guid).Find(&rs)
	if err != nil {
		logger.Log.Errorf("[WeddingInvitationDataDao] GetListByDb failed,error:%s",err.Error())
		return nil,err
	}

	if len(rs) == 0 {
		return nil,nil
	}

	return rs,nil
}


