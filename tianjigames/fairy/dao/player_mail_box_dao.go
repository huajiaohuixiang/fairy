package dao

import (
	"github.com/tianjigames/fairy/pojo"
	"github.com/topfreegames/pitaya/logger"
	"sync"
)

var (
	PlayerMailBoxDao *playerMailBoxDao
	playerMailBoxDaoOnce sync.Once
)

type playerMailBoxDao struct {
	BaseDao
}

func NewPlayerMailBoxDao() *playerMailBoxDao {
	playerMailBoxDaoOnce.Do(func() {
		PlayerMailBoxDao = &playerMailBoxDao{}
	})
	return PlayerMailBoxDao
}

func (p *playerMailBoxDao) GetPlayerMailBoxByPlayerGuid(playerGuid int64) (*pojo.PlayerMailBox,error) {
	playerMailBox := &pojo.PlayerMailBox{}
	r,err := p.enginer.Where("playerGuId=?",playerGuid).Get(playerMailBox)
	if err != nil {
		logger.Log.Errorf("GetPlayerMailBoxByPlayerGuid failed,error:%s",err)
		return nil,err
	}

	if !r {
		return nil,nil
	}

	return playerMailBox,nil
}