package dao

import (
	"sync"
)

var (
	GlobalDataDao *globalDataDao
	globalDataDaoOnce sync.Once
)

type globalDataDao struct {
	BaseDao
}

func NewGlobalDataDao() *globalDataDao {
	globalDataDaoOnce.Do(func() {
		GlobalDataDao = &globalDataDao{}
	})
	return GlobalDataDao
}



