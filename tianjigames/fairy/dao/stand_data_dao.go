package dao

import (
	"sync"
)

var (
	StandDataDao *standDataDao
	standDataDaoOnce sync.Once
)

type standDataDao struct {
	BaseDao
}

func NewStandDataDao() *standDataDao {
	standDataDaoOnce.Do(func() {
		StandDataDao = &standDataDao{}
	})
	return StandDataDao
}




