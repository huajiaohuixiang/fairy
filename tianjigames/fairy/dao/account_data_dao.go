package dao

import (
	"sync"
)

var (
	AccountDataDao *accountDataDao
	accountDataDaoOnce sync.Once
)

type accountDataDao struct {
	BaseDao
}

func NewAccountDataDao() *accountDataDao {
	accountDataDaoOnce.Do(func() {
		AccountDataDao = &accountDataDao{}
	})
	return AccountDataDao
}


