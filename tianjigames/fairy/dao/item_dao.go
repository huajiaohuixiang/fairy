package dao

import (
	"sync"
)

var (
	ItemDao *itemDao
	itemDaoOnce sync.Once
)

type itemDao struct {
	BaseDao
}

func NewItemDao() *itemDao {
	itemDaoOnce.Do(func() {
		ItemDao = &itemDao{}
	})
	return ItemDao
}

