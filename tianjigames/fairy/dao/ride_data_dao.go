package dao

import "sync"

var (
	RideDataDao *rideDataDao
	rideDataDaoOnce sync.Once
)

type rideDataDao struct {
	BaseDao
}

func NewRideDataDao() *rideDataDao {
	rideDataDaoOnce.Do(func() {
		RideDataDao = &rideDataDao{}
	})
	return RideDataDao
}
