package dao

import "sync"

var (
	GangMemberDataDao *gangMemberDataDao
	gangMemberDataDaoOnce sync.Once
)

type gangMemberDataDao struct {
	BaseDao
}

func NewGangMemberDataDao() *gangMemberDataDao {
	gangMemberDataDaoOnce.Do(func() {
		GangMemberDataDao = &gangMemberDataDao{}
	})
	return GangMemberDataDao
}