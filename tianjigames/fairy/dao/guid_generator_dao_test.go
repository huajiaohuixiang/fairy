package dao

import (
	"github.com/stretchr/testify/assert"
	"github.com/tianjigames/fairy/base"
	"github.com/tianjigames/fairy/pojo"
	"testing"
)

func TestGuidGeneratorDao_GenGuid(t *testing.T) {
	base.BaseInit()
	NewGuidGenertorDao().Init()
	NewGuidDataDao().Init()
	v,err := GuidDataDao.Get(&pojo.GuidData{
		Id: 1,
	})
	assert.Nil(t, err)
	assert.NotNil(t, v)
	rs,ok := v.(*pojo.GuidData)
	assert.True(t, ok)
	assert.EqualValues(t, 1,rs.Id)

	v1,err := GuidGeneratorDao.GenGuid(1)
	assert.Nil(t, err)
	assert.NotEqualValues(t, 0,v1)

	v2,err := GuidDataDao.Get(&pojo.GuidData{
		Id: 1,
	})
	assert.Nil(t, err)
	assert.NotNil(t, v2)
	rs2,ok := v2.(*pojo.GuidData)
	assert.True(t, ok)
	assert.EqualValues(t, rs.Low+1,rs2.Low)
}
