package dao

import (
	"sync"
)

var (
	TreasureDao *treasureDao
	treasureDaoOnce sync.Once
)

type treasureDao struct {
	BaseDao
}

/**
实例化treasureDao
 */
func NewTreasureDao() *treasureDao {
	treasureDaoOnce.Do(func() {
		TreasureDao = &treasureDao{}
	})
	return TreasureDao
}

