package dao

import "sync"

var (
	ExecuteMailDao *executeMailDao
	executeMailDaoOnce sync.Once
)

type executeMailDao struct {
	BaseDao
}

func NewExecuteMailDao() *executeMailDao {
	executeMailDaoOnce.Do(func() {
		ExecuteMailDao = &executeMailDao{}
	})
	return ExecuteMailDao
}
