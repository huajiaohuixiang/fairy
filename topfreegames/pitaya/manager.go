package pitaya


import "github.com/topfreegames/pitaya/component"

var managerComp = make([]mgrComp,0)

type mgrComp struct {
	comp component.Component
	opts []component.Option
}

func RegisterManger(c component.Component, options ...component.Option) {
	managerComp = append(managerComp, mgrComp{c, options})
}

func startupManagers() {
	// component initialize hooks
	for _, c := range managerComp {
		c.comp.Init()
	}

	// component after initialize hooks
	for _, c := range managerComp {
		c.comp.AfterInit()
	}
}

func shutdownManagers() {
	// reverse call `BeforeShutdown` hooks
	length := len(managerComp)
	for i := length - 1; i >= 0; i-- {
		managerComp[i].comp.BeforeShutdown()
	}

	// reverse call `Shutdown` hooks
	for i := length - 1; i >= 0; i-- {
		managerComp[i].comp.Shutdown()
	}
}