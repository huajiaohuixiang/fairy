package constants

const (
	LoginServerWorldId = "pitaya.game.loginserver.worldid"
	LoignServerActiveCodeOpen = "pitaya.game.loginserver.activecode_open"
	LoginServerDeleteRoleKeepTime = "pitaya.game.loginserver.delete_role_keep_time"
	LoginServerMaxOnlinePlayerNum = "pitaya.game.loginserver.max_online_player_num"

	WorldServerMaxpoolPlayerSize   = "pitaya.game.worldserver.max_pool_player_size"
	LoginServerTestLoginChannelId  = "pitaya.game.loginserver.test_loginchannel_id"
	LoginServerLocalLoginChannelId = "pitaya.game.loginserver.local_loginchannel_id"
)
